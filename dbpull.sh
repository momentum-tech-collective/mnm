#!/bin/bash

export PGPORT=5462
export PGHOST=localhost
export PGUSER=postgres
export PGPASSWORD=postgres

DB=postgres

dropdb $DB
heroku pg:pull DATABASE_URL $DB --app mcm-prod
