/** @jsx jsx */

import { jsx } from 'theme-ui'
import { configure, addDecorator } from '@storybook/react'
import { MemoryRouter } from 'react-router'
import Theme from '../src/data/theme'
import GraphQLProvider from '../src/data/graphql'

addDecorator(children => (
  <Theme>
    <GraphQLProvider>
      <MemoryRouter>
        <div
          sx={{
            p: 4,
            position: 'relative',
            boxSizing: 'border-box',
            backgroundColor: '#FFCFCF',
            width: '100vw',
            height: '100vh'
          }}
        >
          <main
            sx={{
              position: 'absolute',
              left: 0,
              top: 0,
              backgroundColor: 'white',
              width: ['100%', '500px'],
              maxWidth: ['100%', '50%'],
              height: '100%',
              overflow: 'hidden'
            }}
          >
            {children()}
          </main>
        </div>
      </MemoryRouter>
    </GraphQLProvider>
  </Theme>
))

// automatically import all files ending in *.stories.tsx
configure(require.context('../src', true, /\.stories\.tsx$/), module)
