module.exports = {
  client: {
    service: {
      name: 'localhost',
      url: process.env.GRAPHQL_ENDPOINT
    },
    skipSSLValidation: true,
    tagName: 'gql',
    includes: ['src/**/*.{ts,tsx,js,jsx,graphql}']
  }
}
