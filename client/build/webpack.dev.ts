import merge from 'webpack-merge'
import webpack from 'webpack'
import SpeedMeasurePlugin from 'speed-measure-webpack-plugin'
import * as common from './webpack.common'
import { project } from './utils'

export const config = merge.smart(common.config, {
  mode: 'development',
  devtool: 'eval-source-map',
  output: {
    publicPath: '/',
  },
  devServer: {
    hot: true,
    headers: { 'Access-Control-Allow-Origin': '*' },
    contentBase: project.root('static'),
    host: '0.0.0.0',
    port: 3000,
    disableHostCheck: true,
    compress: true,
    historyApiFallback: true
  }
} as webpack.Configuration)

export default new SpeedMeasurePlugin().wrap(config)
