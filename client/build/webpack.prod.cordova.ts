import merge from 'webpack-merge'
import webpack from 'webpack'
import SpeedMeasurePlugin from 'speed-measure-webpack-plugin'
import { BundleAnalyzerPlugin } from 'webpack-bundle-analyzer'
import { CleanWebpackPlugin } from 'clean-webpack-plugin'
import HtmlWebpackPlugin from 'html-webpack-plugin'
import * as prod from './webpack.prod'
import { project } from './utils'

export const config = merge.smart(prod.config, {
    entry: {
      main: project.src('index.cordova.tsx')
    },
  output: {
    filename: 'bundle.js',
    publicPath: './',
    path: project.root('../cordova/www'),
  },
  plugins: [
    new HtmlWebpackPlugin({
        inject: true,
        template: project.src('static/index.cordova.ejs'),
        minify: {
            removeComments: true,
            collapseWhitespace: true,
            removeRedundantAttributes: true,
            useShortDoctype: true,
            removeEmptyAttributes: true,
            removeStyleLinkTypeAttributes: true,
            keepClosingSlash: true,
            minifyJS: true,
            minifyCSS: true,
            minifyURLs: true
        }
    }),
    new webpack.optimize.LimitChunkCountPlugin({
      maxChunks: 1,
    }),
    new webpack.EnvironmentPlugin({
        CORDOVA_APP: true
    })
  ],
} as webpack.Configuration)

export default new SpeedMeasurePlugin().wrap(
  merge.smart(config, {
    plugins: [
      new CleanWebpackPlugin()
    ]
  })
)
