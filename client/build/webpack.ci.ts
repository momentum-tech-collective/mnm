import merge from 'webpack-merge'
import webpack from 'webpack'
import SpeedMeasurePlugin from 'speed-measure-webpack-plugin'
import * as prod from './webpack.prod'

export const config = merge.smart(prod.config, {} as webpack.Configuration)

// export default new SpeedMeasurePlugin().wrap(config)

export default config
