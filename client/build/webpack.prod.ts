import merge from 'webpack-merge'
import webpack from 'webpack'
import SpeedMeasurePlugin from 'speed-measure-webpack-plugin'
import { BundleAnalyzerPlugin } from 'webpack-bundle-analyzer'
import TerserPlugin from 'terser-webpack-plugin'
import SentryWebpackPlugin from '@sentry/webpack-plugin'
import { CleanWebpackPlugin } from 'clean-webpack-plugin'
import { compact } from 'lodash'
import * as common from './webpack.common'
import { project, gitCommit } from './utils'

const METADATA = `// version: ${process.env.NODE_ENV}-${JSON.stringify(
  gitCommit
)}, time: ${new Date().toString()}, hash:[hash], chunkhash:[chunkhash], name:[name], filebase:[filebase], query:[query], file:[file]`

const deploying = process.env.NODE_ENV === 'production'

export const config = merge.smart(common.config, {
  devtool: 'source-map',
  mode: 'production',
  plugins: [
    new webpack.BannerPlugin(METADATA)
  ],
  optimization: {
    minimizer: [
      new TerserPlugin({
        test: /\.js(\?.*)?$/i,
        sourceMap: true,
        terserOptions: {
          output: {
            preamble: METADATA
          }
        }
      })
    ]
  }
} as webpack.Configuration)

export default new SpeedMeasurePlugin().wrap(
  merge.smart(config, {
    plugins: compact([
      !deploying && new BundleAnalyzerPlugin({
        analyzerPort: 3001,
        analyzerHost: 'localhost'
      }),
      new CleanWebpackPlugin()
    ])
  })
)
