import 'dotenv/config'

import webpack from 'webpack'
import HtmlWebpackPlugin from 'html-webpack-plugin'
import { project, gitCommit } from './utils'
import { REACT_APP_ROOT } from '../src/symbols'

console.log(`
#############################################################
##                                                         ##
##     running webpack with NODE_ENV = ${process.env.NODE_ENV}   ️            ##
##     git commit = ${gitCommit}
##                                                         ##
#############################################################`)

const env = {
  PUBLIC_URL: process.env.PUBLIC_URL,
  NODE_ENV: process.env.NODE_ENV,
  API_HOST: process.env.API_HOST,
  GRAPHQL_ENDPOINT: process.env.GRAPHQL_ENDPOINT,
  CCTLD: process.env.CCTLD,
  MAPBOX_USERNAME: process.env.MAPBOX_USERNAME,
  MAPBOX_ACCESS_TOKEN: process.env.MAPBOX_ACCESS_TOKEN,
  MAPBOX_STYLE_ID: process.env.MAPBOX_STYLE_ID,
  GOOGLE_ANALYTICS_ID: process.env.GOOGLE_ANALYTICS_ID,
  SENTRY_DSN: process.env.SENTRY_DSN,
  SENTRY_ORG: process.env.SENTRY_ORG,
  SENTRY_PROJECT: process.env.SENTRY_PROJECT,
  FACEBOOK_TRACKING_ID: process.env.FACEBOOK_TRACKING_ID,
  CRISP_WEBSITE_ID: process.env.CRISP_WEBSITE_ID,
  FACBOOK_PIXEL_MATCHING: process.env.FACBOOK_PIXEL_MATCHING,
  FEATURE_FLAGS: process.env.FEATURE_FLAGS,
  REACT_APP_ROOT,
  LAST_GIT_COMMIT: gitCommit
}

export const config = {
  entry: {
    main: project.src('index.web.tsx')
  },
  output: {
    publicPath: '/static/',
    filename: '[name].[hash].js',
    path: project.root('dist'),
    sourceMapFilename: '[file].map'
  },
  plugins: [
    new webpack.ContextReplacementPlugin(/moment[/\\]locale$/, /en-gb/),
    new webpack.DefinePlugin({
      'process.env': {
        // Post whatever is in .env to compile-time
        ...Object.entries(env).reduce((dict, [key, value]) => {
          dict[key] = JSON.stringify(value)
          return dict
        }, {})
      }
    }),
    new HtmlWebpackPlugin({
      inject: true,
      template: project.src('static/index.ejs'),
      minify: {
        removeComments: true,
        collapseWhitespace: true,
        removeRedundantAttributes: true,
        useShortDoctype: true,
        removeEmptyAttributes: true,
        removeStyleLinkTypeAttributes: true,
        keepClosingSlash: true,
        minifyJS: true,
        minifyCSS: true,
        minifyURLs: true
      }
    })
  ],
  resolve: {
    symlinks: false,
    extensions: ['.ts', '.tsx', '.mjs', '.js', '.jsx', '.json', '.css'],
    alias: {
      'react-dom': '@hot-loader/react-dom',
      'lodash-es': 'lodash'
    },
    modules: ['node_modules', project.src()]
  },
  module: {
    rules: [
      {
        // Include ts, tsx, js, and jsx files.
        test: /\.(ts|js)x?$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              cacheDirectory: true
            }
          }
        ]
      },
      {
        test: /\.(css|scss|sass|less)$/,
        use: [
          {
            loader: 'style-loader'
          },
          {
            loader: 'css-loader'
          }
        ]
      },
      {
        test: /\.(png|jpg|jpeg|gif|woff|woff2|eot|ttf|svg)$/,
        loader: 'url-loader?limit=100000000'
      }
    ]
  }
} as webpack.Configuration

export default config
