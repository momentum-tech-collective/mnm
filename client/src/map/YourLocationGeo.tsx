/** @jsx jsx */
import { jsx } from 'theme-ui'
import * as React from 'react'
import { Marker } from 'react-mapbox-gl'
import { useSpring, config, animated } from 'react-spring'
import { useGeoContext } from 'map/MapHelperContext'
import { convertPostcodeToCoordinates } from 'data/geo'

/** Map marker for location specified by postcode */
export const MapPostcodeLocation: React.FC<{ postcode: string }> = ({
  postcode
}) => {
  const [coordinates, setCoordinates] = React.useState<any>()

  React.useEffect(() => {
    const loadPostcodeGeo = async () => {
      if (postcode) {
        setCoordinates(await convertPostcodeToCoordinates(postcode))
      }
    }

    loadPostcodeGeo()
  }, [postcode])

  return (
    <React.Fragment>
      {coordinates && (
        <YourLocationGeo postcode={postcode} coordinates={coordinates} />
      )}
    </React.Fragment>
  )
}

/** Map marker for location specified by postcode & coordinates */
const YourLocationGeo: React.FC<{
  postcode: string
  coordinates: [number, number]
}> = React.memo(({ postcode, coordinates }) => {
  const geoContext = useGeoContext()
  const spring = useSpring({
    config: config.wobbly,
    from: { transform: 'perspective(10px) translate3d(0, 0, 0px)' },
    to: async next => {
      while (true) {
        await next({ transform: 'perspective(10px) translate3d(0, 0, 2px)' })
        await next({ transform: 'perspective(10px) translate3d(0, 0, -1px)' })
      }
    }
  })

  return (
    <div className={geoContext.className}>
      <Marker coordinates={coordinates}>
        <animated.div style={{ position: 'relative', ...spring }}>
          <div
            sx={{
              position: 'absolute',
              top: '50%',
              left: '50%',
              transform: 'translate3d(-50%, -50%, 0)',
              borderRadius: 100,
              bg: 'blue.5',
              width: 15,
              height: 15,
              border: '3px solid white',
              boxShadow: '0px 3px 5px rgba(0,0,0,0.5)'
            }}
          />
        </animated.div>
        <div sx={{ mt: 1 }}>
          <div
            sx={{
              boxShadow: '0px 3px 5px rgba(0,0,0,0.5)',
              fontWeight: 900,
              bg: 'white',
              color: 'blue.7',
              p: 1,
              display: 'inline',
              borderRadius: 5
            }}
          >
            {postcode}
          </div>
        </div>
      </Marker>
    </div>
  )
})

export default YourLocationGeo
