import { isMobile } from 'react-device-detect'
import { FlyToInterpolator } from 'react-map-gl'
import mapboxgl from 'mapbox-gl'
import { IMapHelpers } from './MapHelperContext'
import {
  featuresToBounds,
  CONSTITUENCY_MAP_SOURCE,
  CONSTITUENCY_ID_PROPERTY
} from '../helpers/geo'
import { theme } from '../data/theme'

export const zoomToLocations = (
  mapHelper: IMapHelpers,
  features: GeoJSONFeature[],
  zoom = 13,
  pitch?: number
) => {
  if (
    features.length == 1 &&
    features[0].geometry.type.toLowerCase() === 'point'
  ) {
    const map = mapHelper.getMapObject() as mapboxgl.Map
    map.flyTo({
      speed: 1.5,
      center: features[0].geometry.coordinates,
      zoom,
      pitch: typeof pitch !== 'undefined' ? pitch : 45
    })
  } else {
    const map = mapHelper.getMapObject() as mapboxgl.Map
    mapHelper.updateViewportBounds(featuresToBounds(features), { padding: 100 })
    map.easeTo({
      pitch: typeof pitch !== 'undefined' ? pitch : 0
    })
  }
}

export const getConstituencyFeature = (map: mapboxgl.Map, id: string) => {
  return getConstituencyFeatures(map).find(
    d => d.properties![CONSTITUENCY_ID_PROPERTY] === id
  )
}

export const getConstituencyFeatures = (map: mapboxgl.Map) =>
  map.querySourceFeatures(CONSTITUENCY_MAP_SOURCE.sourceId, {
    sourceLayer: CONSTITUENCY_MAP_SOURCE.sourceLayer
  })
