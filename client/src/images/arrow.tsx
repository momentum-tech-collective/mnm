import React from 'react'

export enum ArrowDirection {
  LEFT = 0,
  UP = 90,
  RIGHT = 180,
  DOWN = -90
}

export const Arrow: ViewElement<{ direction: ArrowDirection }> = ({
  direction,
  ...props
}) => (
  <svg
    sx={{ transform: `rotate(${direction})` }}
    height={22}
    viewBox="0 0 12 22"
    fill="none"
    {...props}
  >
    <path d="M11 1L2 11.1887L11 21" stroke="currentColor" strokeWidth="2" />
  </svg>
)
