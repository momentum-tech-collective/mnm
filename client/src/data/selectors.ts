// TODO: Generate typescript interface for GraphQL schema
export const upcomingEventsFilter = (e: any) =>
  new Date(e.properties.startTime).getTime() >= Date.now()
