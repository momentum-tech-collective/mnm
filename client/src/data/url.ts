import { parse } from 'querystring'

export const getRequiredQueryParam = (search: string, key: string) => {
  const qp = parse(search.substr(1))[key]
  const param = Array.isArray(qp) ? qp[0] : qp

  if (typeof param === 'undefined') {
    throw Error('Missing query param')
  }

  return param
}
