/** @jsx jsx */
import { jsx } from 'theme-ui'
import * as React from 'react'
import gql from 'graphql-tag'
import EventCard from 'event/EventCard'
import { useMutation, useQuery } from '@apollo/react-hooks'
import {
  Panel,
  PanelHeader,
  PanelCard,
  ImagePanelHeaderCard,
  LoadingArea
} from 'components/Elements'
import { Button } from '@material-ui/core'
import { useAnalytics } from 'data/analytics'
import { usePushRoute } from 'components/Navigation'
import { Event, Landing } from 'routes'
import { useHistory } from 'react-router'
import { EventForm, CreateEventFormFields } from 'event/EventForm'
import { parse } from 'query-string'
import { CurrentUser_currentUser } from 'data/__graphql__/CurrentUser'
import { EventCategory } from 'data/event'
import { useCurrentUser } from '../data/hooks'
import {
  CreateEvent,
  CreateEventVariables,
  CreateEvent_createEvent_event
} from './__graphql__/CreateEvent'
import {
  TravelToTargetEvents,
  TravelToTargetEventsVariables
} from './__graphql__/TravelToTargetEvents'
import { useCategories, flattenSubcategories } from './queries'

const TRAVEL_TO_EVENT_DATA = gql`
  query TravelToTargetEvents($constituencyId: String!) {
    constituency(id: $constituencyId) {
      id
      name
    }
  }
`

const CREATE_EVENT_MUTATION = gql`
  ${EventCard.fragment}

  mutation CreateEvent(
    $name: String!
    $description: String!
    $startTime: Int!
    $constituency: String
    $address: String!
    $postcode: String!
    $contactNumber: String
    $contactEmail: String
    $whatsappLink: String
    $targetEventId: String
    $level: String!
    $catOrSubcatId: String!
    $otherCategoryDescription: String
  ) {
    createEvent(
      name: $name
      description: $description
      startTime: $startTime
      constituency: $constituency
      address: $address
      postcode: $postcode
      targetEvent: $targetEventId
      whatsappLink: $whatsappLink
      level: $level
      catOrSubcatId: $catOrSubcatId
      contactNumber: $contactNumber
      contactEmail: $contactEmail
      otherCategoryDescription: $otherCategoryDescription
    ) {
      event {
        id
        ...EventCard
      }
      currentUser {
        managedEvents {
          id
        }
      }
    }
  }
`

type State = 'create' | 'completed'

const defaultInitialValues: CreateEventFormFields = {
  name: '',
  description: '',
  constituency: '',
  address: '',
  contactEmail: '',
  contactNumber: '',
  postcode: '',
  subcategory: null,
  startTime: null,
  isContactable: false
}

export const CreateEventContent: React.FC<{
  initialState?: State
  initialValues?: Partial<CreateEventFormFields>
  user?: CurrentUser_currentUser
  fixedType?: boolean
}> = ({
  initialState = 'create',
  initialValues: initialValuesOverrides = {},
  user,
  fixedType
}) => {
  const initialValues = {
    ...defaultInitialValues,
    ...initialValuesOverrides
  }
  const { data } = useCategories()
  const { categories = [] } = data || {}
  const subcategories = flattenSubcategories(categories)
  const navigate = usePushRoute([Event, Landing], { replace: true })
  const history = useHistory()
  const [event, setEvent] = React.useState<CreateEvent_createEvent_event>()

  const [state, setState] = React.useState(initialState)
  const [mutate] = useMutation<CreateEvent, CreateEventVariables>(
    CREATE_EVENT_MUTATION
  )
  const analytics = useAnalytics()

  React.useEffect(() => {
    analytics.logEvent('create-event-start', {
      value: initialValues.category
    })
  }, [])

  React.useEffect(() => {
    if (event) {
      setState('completed')
    }
  }, [event])

  if (user) {
    const { phone, email } = user
    initialValues.contactNumber = phone || ''
    initialValues.contactEmail = email
  }

  return (
    <React.Fragment>
      {state === 'create' && (
        <Panel
          key={state}
          sx={{
            gridTemplateRows: 'repeat(3, min-content) 1fr'
          }}
        >
          <PanelHeader
            breadCrumbs={[
              { to: '/', text: 'Home' },
              { to: '/create-event', text: 'Add an Event' }
            ]}
          />
          <PanelCard>
            Hosting your own event? Add the details of your event below 👇
          </PanelCard>

          <EventForm
            initialValues={initialValues}
            subcategories={subcategories}
            fixedType={fixedType}
            onSubmit={async variables => {
              const res = await mutate({
                variables: {
                  ...variables,
                  startTime: new Date(variables.startTime).getTime() / 1000,
                  address: variables.address,
                  level: variables.subcategory.level,
                  catOrSubcatId: variables.subcategory.id
                }
              })

              if (res.errors) {
                throw res.errors[0]
              }

              setEvent(res.data && res.data.createEvent!.event)
              analytics.logEvent('create-event-complete')
            }}
          />
        </Panel>
      )}

      {state === 'completed' && (
        <Panel
          sx={{
            gridTemplateRows: 'repeat(2, min-content) 1fr'
          }}
        >
          <PanelCard
            sx={{
              variant: 'body',
              mt: [null, '180px'],
              overflow: 'scroll'
            }}
          >
            <h1
              sx={{
                fontWeight: '600',
                fontSize: '60px',
                color: 'primary'
              }}
            >
              Thank you!
            </h1>
            <p sx={{ fontSize: 6, fontFamily: 'heading', fontWeight: '600' }}>
              Organising is hard work - but without it, our movement wouldn't
              exist.
              <br />
              <br />
              Solidarity ✊
            </p>

            <p sx={{ fontSize: 6, fontFamily: 'heading', fontWeight: '600' }}>
              We’ll give your event a quick review to check that it’s legit and
              then add it to the map. We’ll be in touch with any questions or to
              offer you help and let you know when it’s live.
            </p>

            <p>
              We’re busy building out the rest of this app and it’s not
              currently possible to edit your event on here. If you need to
              change anything, contact us using the information found in the
              "About" panel below.
            </p>
          </PanelCard>

          <PanelCard
            sx={{
              variant: 'rows',
              alignItems: 'flex-start',
              '> *:not(:last-child)': { mb: 2 }
            }}
          >
            <Button
              onClick={
                event &&
                (() => {
                  navigate(`/event/${event.id}`)
                })
              }
              color="primary"
              variant="contained"
            >
              My Event →
            </Button>

            <Button
              onClick={
                history.length > 0
                  ? () => history.goBack()
                  : () => {
                      navigate('/')
                    }
              }
              sx={{ mr: 2 }}
              color="primary"
            >
              ← Back to map
            </Button>
          </PanelCard>
        </Panel>
      )}
    </React.Fragment>
  )
}

const CreateEventScreen = () => {
  const history = useHistory()

  const queryParams = parse(history.location.search.substr(1)) as Record<
    string,
    string
  >
  const constituencyOpts = useQuery<
    TravelToTargetEvents,
    TravelToTargetEventsVariables
  >(TRAVEL_TO_EVENT_DATA, {
    skip: queryParams.type !== EventCategory.TRAVELTOGETHER,
    variables: {
      constituencyId: String(queryParams.constituency)
    }
  })
  const user = useCurrentUser()

  if (constituencyOpts.loading || user.loading) {
    return <LoadingArea />
  }

  const initialValues = { ...defaultInitialValues, ...queryParams }

  if (queryParams.type) {
    initialValues.category = queryParams.type
    initialValues.subcategory = undefined
    initialValues.constituency = queryParams.constituency
    initialValues.targetEventId = queryParams.event
  }

  return (
    <CreateEventContent
      initialValues={initialValues}
      user={(user && user.user) || undefined}
      fixedType={Boolean(queryParams.type)}
      constituencyName={
        constituencyOpts.data &&
        constituencyOpts.data.constituency &&
        constituencyOpts.data.constituency.name
      }
    />
  )
}

export default CreateEventScreen
