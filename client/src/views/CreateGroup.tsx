// /** @jsx jsx */
// import { jsx } from 'theme-ui'
// import * as React from 'react'
// import gql from 'graphql-tag'
// import { Formik, Field, FieldProps } from 'formik'
// import GroupCard from 'Group/GroupCard'
// import * as yup from 'yup'
// import { useMutation } from '@apollo/react-hooks'
// import AddressInput from 'components/AddressInput'
// import ConstituencyInput from 'components/ConstituencyInput'
// import { useAuthenticatedCallback } from 'data/hooks'
// import { get } from 'lodash'
// import { categories, campaigningCategories } from 'data/Group'
// import { DateCalendar } from 'components/DateField'
// import {
//   Panel,
//   PanelHeader,
//   FormWrapper,
//   ProgressButton,
//   PanelCard,
//   ImagePanelHeaderCard,
//   LoadingArea
// } from '../components/Elements';
// import { Button } from '@material-ui/core'
// import { useAnalytics } from 'data/analytics'
// import { usePushRoute } from 'components/Navigation'
// import { Group, Landing } from 'routes'
// import { useHistory } from 'react-router'
// import { GroupForm, CreateGroupFormFields } from 'Group/GroupForm'
// import {
//   Input,
//   LargeRadioOption,
//   RadioOptionGroup,
//   FormSection,
//   FormError,
//   FORM_GENERAL_ERROR
// } from '../components/Form'
// import { useCurrentUser } from '../data/hooks'
// import {
//   CreateGroup,
//   CreateGroupVariables,
//   CreateGroup_createGroup,
//   CreateGroup_createGroup_Group
// } from './__graphql__/CreateGroup'

// const CREATE_Group_MUTATION = gql`
//   mutation CreateGroup(
//     $name: String!
//     $description: String
//     $joinLink: String!
//     $platform: String!
//     $groupCount: Int
//     $constituencies: [String]!
//     $tags: [String]!
//   ) {
//     createGroup(
//       name: $name
//       description: $description
//       joinLink: $joinLink
//       platform: $platform
//       groupCount: $groupCount
//       constituencies: $constituencies
//       tags: $tags
//     ) {
//       communicationGroup {
//         id
//       }
//     }
//   }
// `

// type State = 'create' | 'completed'

// const CreateGroupScreen: React.FC<{ initialState?: State }> = ({
//   initialState = 'create'
// }) => {
//   const navigate = usePushRoute([Landing], { replace: true })
//   const history = useHistory()
//   const [Group, setGroup] = React.useState<CreateGroup_createGroup_Group>()

//   const [state, setState] = React.useState(initialState)
//   const [mutate] = useMutation<CreateGroup, CreateGroupVariables>(
//     CREATE_Group_MUTATION
//   )
//   const user = useCurrentUser()
//   const analytics = useAnalytics()

//   React.useEffect(() => {
//     analytics.logEvent('create-group-start')
//   }, [])

//   React.useEffect(() => {
//     if (Group) {
//       setState('completed')
//     }
//   }, [Group])

//   const initialValues: CreateGroupFormFields = {
//     name: null,
//     description: null,
//     joinLink: null,
//     platform: 'WhatsappChat',
//     groupCount: null,
//     constituencies: [],
//     tags: [],
//   }

//   return (
//     <Panel key={state}>
//       <PanelHeader>Add a chat group</PanelHeader>

//       {state === 'create' &&
//         (user.loading ? (
//           <LoadingArea />
//         ) : (
//           <React.Fragment>
//             <PanelCard>
//               Know a good social media group others should join? Add it to My Campaign Map below 👇
//             </PanelCard>

//             <GroupForm
//               initialValues={initialValues}
//               onSubmit={async variables => {
//                 const res = await mutate({
//                   variables
//                 })

//                 if (res.errors) {
//                   throw res.errors[0]
//                 }

//                 setGroup(res.data && res.data.createGroup!.Group)
//                 analytics.logEvent('create-group-complete')
//               }}
//             />
//           </React.Fragment>
//         ))}

//       {state === 'completed' && (
//         <React.Fragment>
//           <ImagePanelHeaderCard image={require('../images/corbyn-stairs.gif')}>
//             Thank You!!! 🙏
//           </ImagePanelHeaderCard>

//           <PanelCard sx={{ variant: 'body' }}>
//             <p>
//               Adding groups to the platform helps others get networked and get involved.{' '}
//               <b>
//                 You’re organising the movement. 💪
//               </b>
//             </p>

//             <p>
//               We’ll give this group a quick review to check that it’s legit and
//               then add it to the map. We’ll be in touch with any questions or to
//               offer you help and let you know when it’s live.
//             </p>

//             <p>
//               We’re busy building out the rest of this app and it’s not
//               currently possible to edit this group on here. If you need to
//               change anything, contact us using the help icon on the bottom
//               right of the screen.
//             </p>
//           </PanelCard>

//           <PanelCard
//             sx={{
//               variant: 'rows',
//               alignItems: 'flex-start',
//               '> *:not(:last-child)': { mb: 2 }
//             }}
//           >
//             <Button
//               onClick={
//                 history.length > 0
//                   ? () => history.goBack()
//                   : () => {
//                       navigate('/')
//                     }
//               }
//               sx={{ mr: 2 }}
//               color="primary"
//             >
//               ← Back to map
//             </Button>
//           </PanelCard>
//         </React.Fragment>
//       )}
//     </Panel>
//   )
// }

// export default CreateGroupScreen
