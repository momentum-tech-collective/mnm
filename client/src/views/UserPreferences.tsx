/** @jsx jsx */
import { jsx } from 'theme-ui'
import gql from 'graphql-tag'
import * as React from 'react'
import { RouteComponentProps } from 'react-router'
import PostcodeSearch from 'components/PostcodeSearch'
import { PanelCard, Panel, PanelSectionTitle } from 'components/Elements'
import { usePushRoute } from 'components/Navigation'
import { setPostcode } from 'data/sync'
import { signout } from 'auth/authHelpers'
import { updateContactable } from 'helpers/user'
import { Button, Switch, FormControlLabel } from '@material-ui/core'
import { useQuery } from '@apollo/react-hooks'
import { Landing } from 'routes'
import { Link } from 'react-router-dom'
import { useAuthenticatedCallback, useAuth } from '../data/hooks'
import {
  UserPreferencesPage,
  UserPreferencesPage_user
} from './__graphql__/UserPreferencesPage'

export const USER_PREFERENCE_QUERY = gql`
  query UserPreferencesPage {
    user: currentUser {
      id
      email
      firstName
      lastName
      isContactable
      postcode
    }
  }
`

const UserPreferences: React.FC<RouteComponentProps> = () => {
  const { loggedIn } = useAuth()
  const { data } = useQuery<UserPreferencesPage>(USER_PREFERENCE_QUERY, {
    skip: !loggedIn
  })

  return <PreferencesPrompt {...data} />
}

export const PreferencesPrompt: React.FC<Partial<UserPreferencesPage>> = ({
  user
}) => {
  return (
    <React.Fragment>
      <Panel>
        <PanelCard
          sx={{
            variant: 'rows',
            justifyContent: 'flex-end',
            pb: 4,
            height: 300,
            backgroundSize: 'cover',
            backgroundImage: `url("${require('../images/crowd.jpg')}")`,
            color: 'white',
            maxHeight: '20vh',
            minHeight: 100
          }}
        >
          <img
            sx={{ alignSelf: 'flex-start' }}
            src={require('../images/logo.svg')}
          />
        </PanelCard>
        {user ? <UserPreferencesSwitches user={user} /> : <PreferencesSignIn />}
      </Panel>
    </React.Fragment>
  )
}

const UserPreferencesSwitches: React.FC<{ user: UserPreferencesPage_user }> = ({
  user
}) => {
  return (
    <React.Fragment>
      <PanelSectionTitle sx={{ pt: 4 }}>Preferences</PanelSectionTitle>

      <PanelCard>
        <p sx={{ mb: 3, fontSize: 4 }}>
          Hi {user.firstName}, you can update your postcode and control the
          emails you get from us using the form below.
        </p>
      </PanelCard>

      <PanelSectionTitle sx={{ pt: 4 }}>Email preferences</PanelSectionTitle>
      <PanelCard>
        <FormControlLabel
          control={
            <Switch
              checked={user.isContactable}
              onChange={updateContactable()}
              value="checkedA"
              inputProps={{ 'aria-label': 'secondary checkbox' }}
            />
          }
          labelPlacement="start"
          label="Email updates"
        />{' '}
        {user.isContactable ? 'ON' : 'OFF'}
      </PanelCard>

      <PanelSectionTitle sx={{ pt: 4 }}>Update Postcode</PanelSectionTitle>
      <PanelCard>
        <PostcodeSearch
          initialValue={user.postcode}
          label={'Update postcode'}
          onSubmit={async postcode => {
            setPostcode(postcode)
          }}
          inputType={'update'}
        />
      </PanelCard>

      <PanelCard>
        <Link to="/">
          <Button variant="outlined" color="default">
            Back to events
          </Button>
        </Link>
        <span sx={{ mr: 2 }} />

        <Button
          variant="outlined"
          color="default"
          onMouseDown={() => signout()}
        >
          Sign out
        </Button>
      </PanelCard>
    </React.Fragment>
  )
}

const PreferencesSignIn: React.FC = () => {
  const [authenticate, authModal] = useAuthenticatedCallback()

  return (
    <React.Fragment>
      {authModal}

      <PanelCard bg="contrast.1" sep="mid">
        <p>Sign in to manage your preferences</p>
      </PanelCard>

      <PanelCard>
        <Button
          color="primary"
          onClick={() => authenticate({ mode: 'signin' })}
        >
          Sign in
        </Button>

        <span sx={{ mr: 2 }} />

        <Button
          variant="contained"
          color="primary"
          onClick={() => authenticate({ mode: 'register' })}
        >
          Register
        </Button>
      </PanelCard>
    </React.Fragment>
  )
}

export default UserPreferences
