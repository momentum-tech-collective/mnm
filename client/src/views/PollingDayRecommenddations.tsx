/** @jsx jsx */
import { jsx } from 'theme-ui'
import * as React from 'react'
import { RouteComponentProps } from 'react-router'
import { parse } from 'querystring'
import { useSavePostcode } from 'data/hooks'
import {
  LoadingArea,
  Panel,
  PanelHeader,
  PanelCardTitle,
  PanelCard,
  LinkButtonExternal
} from 'components/Elements'
import { PushLink, usePushRoute } from 'components/Navigation'
import { Constituency } from 'routes'
import { Select, FormControl, InputLabel, MenuItem } from '@material-ui/core'
import { usePollingDayRecommendations } from './queries'
import { ConstituencyPollingDayAttendance } from '../../__graphql__/globalTypes'
import { useAnalytics } from 'data/analytics'
import { convertPostcodeToConstituency } from 'data/geo'
import Helmet from 'react-helmet'

export const PollingDayRecommendations: React.FC<RouteComponentProps<any>> = ({
  location,
  match: {
    params: { postcode }
  }
}) => {
  const analytics = useAnalytics()
  useSavePostcode(postcode)

  const navigateConstituency = usePushRoute(Constituency)
  const { loading, data } = usePollingDayRecommendations(postcode)
  const { constituencies = [] } = data || {}

  const priorityConstituencies = constituencies.filter(
    c =>
      c.constituency.pollingDayAttendance < ConstituencyPollingDayAttendance.A_3
  )
  const fullConstituencies = constituencies.filter(
    c =>
      c.constituency.pollingDayAttendance ===
      ConstituencyPollingDayAttendance.A_3
  )

  React.useEffect(() => {
    const logRecommendations = async () => {
      const source = await convertPostcodeToConstituency(postcode)
      for (const e of priorityConstituencies) {
        analytics.logEvent('recommend-high-pri', {
          category: e.constituency.name,
          value: source
        })
      }
      for (const e of constituencies) {
        analytics.logEvent('recommend-constituencies', {
          category: e.constituency.name,
          value: source
        })
      }
      for (const e of fullConstituencies) {
        analytics.logEvent('recommend-low-pri', {
          category: e.constituency.name,
          value: source
        })
      }
    }
    logRecommendations()
  }, [priorityConstituencies, fullConstituencies])

  if (loading) {
    return (
      <React.Fragment>
        <Helmet>
          <title>{postcode}: Where you're needed - My Campaign Map</title>
        </Helmet>
        <Panel>
          <LoadingArea />
        </Panel>
      </React.Fragment>
    )
  }

  return (
    <React.Fragment>
      <Helmet>
        <title>{postcode}: Where you're needed - My Campaign Map</title>
      </Helmet>

      <Panel>
        <PanelHeader
          breadCrumbs={[
            { to: '/', text: 'Home' },
            {
              to: `/recommendations/${postcode}`,
              text: 'Search Results'
            }
          ]}
        />

        <PanelCard sep>
          <PanelCardTitle sx={{ fontSize: 6, color: 'black', py: 2 }}>
            🚨 These are the marginals that need you most today:
          </PanelCardTitle>

          <p sx={{ mt: 0, mb: 2 }}>
            We're distributing support based on your location and how it's going
            in marginals. <b>When we coordinate, we win.</b>
          </p>

          {priorityConstituencies.map(({ id, constituency, distance }) => (
            <PushLink
              route={Constituency}
              to={{ pathname: `/constituency/${id}` }}
              sx={{
                variant: 'columns',
                textDecoration: 'none',
                color: 'inherit',
                bg:
                  constituency.pollingDayAttendance === 'A_1'
                    ? 'red.0'
                    : 'white',
                p: 3,
                py: 2,
                my: 2,
                boxShadow: '0px 0px 6px rgba(0, 0, 0, 0.1)',
                borderRadius: 6,
                flexDirection: 'column'
              }}
            >
              <div key={id} sx={{ display: 'flex', alignItems: 'center' }}>
                <PanelCardTitle sx={{ color: '#000000', flex: 1, m: 0 }}>
                  <span sx={{ fontSize: [4, 5], flex: 1 }}>
                    {constituency.name}
                  </span>
                </PanelCardTitle>

                <span sx={{ color: '#A5A5A5', fontWeight: 300 }}>
                  {distance.mi === 0 ? (
                    'Home'
                  ) : (
                    <span>{distance.mi.toFixed(2)} mi</span>
                  )}
                </span>

                <span sx={{ ml: 2, fontSize: 5 }}>&rarr;</span>
              </div>
              {constituency.pollingDayAttendance === 'A_1' && (
                <div sx={{ fontSize: 3, color: 'red.8', pt: 1 }}>
                  Right now this constituency needs the most support
                </div>
              )}
            </PushLink>
          ))}

          {fullConstituencies.length > 0 && (
            <React.Fragment>
              <PanelCardTitle sx={{ fontSize: 5, color: 'black', mt: 4 }}>
                Other marginals nearby:
              </PanelCardTitle>

              <div sx={{ fontWeight: 300, fontSize: 3, my: 2, mb: 2 }}>
                These places have a lot of support already. You’ll make a bigger
                impact in one up there 👆.
              </div>

              <div>
                <FormControl fullWidth variant="outlined">
                  <InputLabel>Choose constitency</InputLabel>
                  <Select placeholder="Choose constitency" fullWidth>
                    {fullConstituencies.map(constit => (
                      <MenuItem
                        value={constit.id}
                        onMouseDown={() => {
                          navigateConstituency({
                            pathname: `/constituency/${constit.id}`
                          })
                        }}
                      >
                        {constit.constituency.name}
                      </MenuItem>
                    ))}
                  </Select>
                </FormControl>
              </div>
            </React.Fragment>
          )}
        </PanelCard>

        <PanelCard sx={{ lineHeight: '1.2em', bg: 'white' }}>
          <PanelCardTitle sx={{ mb: 3, fontSize: 5 }}>
            Genuinely can't make it out tonight?
          </PanelCardTitle>

          <LinkButtonExternal
            variant="outlined"
            color="primary"
            size="large"
            href="https://www.facebook.com/events/3018812678146822/?event_time_id=3032342703460486"
            sx={{ width: '100%', fontWeight: 900, bg: 'white' }}
          >
            ‘Twelve Marginals of Christmas’ phonebank! 👉 📲
          </LinkButtonExternal>

          <p sx={{ fontWeight: 500 }}>
            Join our virtual phonebank on Facebook and call Labour voters in key
            marginals.
          </p>

          <ul>
            <li sx={{ pl: 2, mb: 1 }}>
              You'll need a mobile or landline and a laptop, desktop or tablet
              computer
            </li>
            <li sx={{ pl: 2, mb: 1 }}>
              You need to be a Labour member to use the Dialogue system
            </li>
            <li sx={{ pl: 2, mb: 1 }}>
              If you haven’t logged in before, you’ll need your Labour member
              number
            </li>
          </ul>

          <LinkButtonExternal
            variant="outlined"
            color="primary"
            size="large"
            href="https://docs.google.com/document/d/1R4olixCWAW9OoH8ZoRQsJ0yftExVzbMAHd4--XzVOek/edit?usp=sharing"
            sx={{ width: '100%', fontWeight: 900, bg: 'white' }}
          >
            Other ways to campaign from your phone 👉 📲
          </LinkButtonExternal>
        </PanelCard>
      </Panel>
    </React.Fragment>
  )
}
