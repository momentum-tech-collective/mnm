import { RouteComponentProps } from 'react-router'
import React from 'react'
import ConstituencyDashboardView from 'components/ConstituencyDashboard'
import { parse } from 'querystring'
import { usePostcode } from 'data/hooks'
import { useConstituencyScreenData } from './queries'

const Constituency: React.FC<RouteComponentProps<{ id: string }>> = ({
  match: {
    params: { id }
  },
  location: { search }
}) => {
  const savedPostcode = usePostcode()
  const postcode = parse(search.substr(1)).start || savedPostcode
  const { data, loading } = useConstituencyScreenData(
    id,
    String(postcode || undefined)
  )

  return (
    <ConstituencyDashboardView
      loading={loading}
      postcode={postcode}
      {...data}
      {...(data && data.constituencyTravelToEvents)}
    />
  )
}

export default Constituency
