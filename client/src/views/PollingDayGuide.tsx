/** @jsx jsx */
import { jsx } from 'theme-ui'
import * as React from 'react'
import {
  PanelSectionTitle,
  PanelCard,
  Panel,
  PanelHeader
} from '../components/Elements'

const checkBoxSx = {
  display: 'flex',
  flexDirection: 'row',
  fontSize: '1.2rem',
  fontFamily: 'heading',
  '& + &': {
    marginTop: 15
  },
  '::before': {
    display: 'block',
    flexShrink: 0,
    flexGrow: 0,
    content: "'✅'",
    paddingRight: 20,
    paddingBottom: 20,
    fontSize: '1.4rem'
  }
}

const PollingDayGuide: React.FC = () => {
  return (
    <React.Fragment>
      <Panel>
        <PanelHeader
          breadCrumbs={[
            { to: '/', text: 'Home' },
            { to: '/pollingdayguide', text: 'Polling day guide' }
          ]}
        />

        <PanelSectionTitle sx={{ pt: 4, pb: 1, background: 'white' }}>
          What to expect on Polling Day
        </PanelSectionTitle>
        <PanelCard sx={{ background: 'white' }}>
          <div sx={checkBoxSx}>
            <p sx={{ margin: 0 }}>
              Show up at a Campaign Centre as early as you can. They’re usually
              open from 6am–10pm.
            </p>
          </div>
          <div sx={checkBoxSx}>
            <p sx={{ margin: 0 }}>
              The most important hours in many areas are from <b>6pm–10pm</b> as
              this is the key time for speaking to voters.
            </p>
          </div>
          <div sx={checkBoxSx}>
            <p sx={{ margin: 0 }}>
              <b>Bring a car</b> if you have one. This is particularly important
              in rural or suburban marginals.
            </p>
          </div>
          <div sx={checkBoxSx}>
            <p sx={{ margin: 0 }}>
              <b>Be ready to run a board</b> if you need to. If you have run a
              board before , then your skills will be extra useful.
            </p>
          </div>
          <div sx={checkBoxSx}>
            <p sx={{ margin: 0 }}>
              <b>Bring warm clothes.</b> Make sure you have plenty of layers,
              warm clothes and waterproofs.
            </p>
          </div>
          <div sx={checkBoxSx}>
            <p sx={{ margin: 0 }}>
              <b>Bring snacks.</b> Especially in more rural constituencies where
              it’s hard to get food late at night – bring a packed lunch and
              plenty of snacks with you.
            </p>
          </div>
        </PanelCard>

        <PanelSectionTitle sx={{ pt: 4, pb: 0 }}>
          How to make it count
        </PanelSectionTitle>
        <PanelCard>
          <p>
            Canvassing is different on polling day. The focus is on speaking to
            identified Labour supporters and making sure they get out and vote.
          </p>
          <p>
            This transition from persuasive canvassing to ‘getting out the vote’
            will depend on the data for the area you’re campaigning in and the
            number of volunteers on the ground.
          </p>
          <p>
            The strategy will be carefully decided by the local group to
            maximise the chances of the local Labour candidate cross the winning
            line.
          </p>
          <div sx={{ fontSize: '1.3rem', fontWeight: 900 }}>
            Board running. 📋
          </div>
          <p>
            Just like with usual canvasses, doors will be split up into boards,
            with one person (the board runner) managing that information.
          </p>
          <p>
            Today, instead of recording who is planning to vote for Labour, the
            ‘board runner’ will record if they have voted yet or in some cases
            ask when they are planning to vote.
          </p>
          <div sx={{ fontSize: '1.3rem', fontWeight: 900 }}>
            Paint the town red. 🌹
          </div>
          <p>
            We want every street, train and bus to be painted red on Thursday.
            Put up flyers and stickers everywhere you go and wear red.
          </p>
          <div sx={{ fontSize: '1.3rem', fontWeight: 900 }}>
            Go flyering, if the office is too busy. 💌
          </div>
          <p>
            Despite our best effort to distribute volunteers well, not everyone
            will have told us their polling day plans, so there will be some
            places where more people turned up than are planned for.
          </p>
          <p>
            If this happens, it’s a good idea to head out to do flyering. Again
            it’s important to pick areas where you’re likely to find Labour
            supporters - busy shopping areas are ideal.
          </p>
          <div sx={{ fontSize: '1.3rem', fontWeight: 900 }}>Be visible. 👋</div>
          <p>
            Polling day is the most important time for Labour to be visible to
            the world.
          </p>
          <p>
            So make sure you wear red, and post photos and videos on social
            media with the hashtag #LabourDoorstep so that we can reach millions
            of voters both online and offline!
          </p>
          <div sx={{ fontSize: '1.3rem', fontWeight: 900 }}>
            Be supportive. 😌
          </div>
          <p>
            It’s really important that we look after each other on polling day.
            Many people will have been working tirelessly around the clock for
            weeks, giving it everything they’ve got.
          </p>
          <p>
            Local organisers will be juggling lots and, along with key
            activists, will have been up since 5am for the first leaflet drop.
            Please be kind, helpful and understanding.
          </p>
        </PanelCard>
      </Panel>
    </React.Fragment>
  )
}

export default PollingDayGuide
