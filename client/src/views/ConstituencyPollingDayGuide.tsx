import { RouteComponentProps } from 'react-router'
import React from 'react'
import ConstituencyPollingDayGuide from 'components/ConstituencyPollingDayGuide'
import { parse } from 'querystring'
import { usePostcode } from 'data/hooks'
import { useConstituencyPollingDayScreenData } from './queries'

const ConstituencyPollingDayView: React.FC<
  RouteComponentProps<{ id: string }>
> = ({
  match: {
    params: { id }
  },
  location: { search }
}) => {
  const savedPostcode = usePostcode()
  const postcode = String(parse(search.substr(1)).start || savedPostcode)
  const { data, loading } = useConstituencyPollingDayScreenData(
    id,
    String(postcode || undefined)
  )

  return (
    <ConstituencyPollingDayGuide
      loading={loading}
      postcode={postcode}
      {...data}
    />
  )
}

export default ConstituencyPollingDayView
