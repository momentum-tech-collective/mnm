/** @jsx jsx */
import { jsx } from 'theme-ui'
import * as React from 'react'
import { RouteComponentProps } from 'react-router'
import EventDetail from 'event/EventDetail'
import { useAuthenticatedCallback, useCurrentUser } from 'data/hooks'
import { commitToEvent } from 'data/sync'
import { useEvent } from './queries'

const EventScreen: React.FC<RouteComponentProps<{ id: string }>> = ({
  history,
  match: {
    params: { id }
  }
}) => {
  const { data } = useEvent(id)
  const { user } = useCurrentUser()
  const [callbackAfterAuthentication, authModal] = useAuthenticatedCallback()

  const indicateRSVP = () => {
    callbackAfterAuthentication({
      mode: 'register',
      onAuthenticated: async () => {
        await commitToEvent(id)
        history.push(`/commitment/${id}`)
      }
    })
  }

  return (
    <React.Fragment>
      <EventDetail
        id={id}
        onRsvp={indicateRSVP}
        currentUser={user || undefined}
        {...(data && data.event)}
      />
      {authModal}
    </React.Fragment>
  )
}

export default EventScreen
