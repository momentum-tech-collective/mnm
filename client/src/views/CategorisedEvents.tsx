/** @jsx jsx */
import { jsx } from 'theme-ui'
import * as React from 'react'
import { RouteComponentProps } from 'react-router'
import { useSavePostcode } from 'data/hooks'
import { useCategorisedEvents } from './queries'
import AllCategorisedEvents from '../components/AllCategorisedEvents'

const CategorisedEvents: React.FC<RouteComponentProps<any>> = ({
  match: {
    params: { category, date }
  }
}) => {
  const { loading, data } = useCategorisedEvents(category, date)
  const { events = [] } = data || {}

  return (
    <AllCategorisedEvents
      category={category}
      date={date}
      events={events}
      loading={loading}
    />
  )
}

export default CategorisedEvents
