/** @jsx jsx */
import { jsx } from 'theme-ui'
import * as React from 'react'
import PostcodeAllEvents from 'components/PostcodeAllEvents'
import { RouteComponentProps } from 'react-router'
import { useSavePostcode } from 'data/hooks'
import { useAllEvents } from './queries'

const AllEvents: React.FC<RouteComponentProps<any>> = ({
  match: {
    params: { postcode, date, radius }
  }
}) => {
  useSavePostcode(postcode)
  const { loading, data } = useAllEvents(postcode, date, radius)

  const { events = [] } = data || {}

  return (
    <PostcodeAllEvents
      postcode={postcode}
      date={date}
      events={events}
      loading={loading}
      radius={radius}
    />
  )
}

export default AllEvents
