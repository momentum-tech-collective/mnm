import { storiesOf } from '@storybook/react'
import React from 'react'
import { times } from 'lodash'
import { someEvent, someUser } from 'helpers/fakes'
import { LandingPrompt } from './Landing'
import { CreateEventContent } from './CreateEvent'
import { Commitment } from './Commitment'
import { PreferencesPrompt } from './UserPreferences'

storiesOf('views/CreateEventScreen', module)
  .add('default', () => <CreateEventContent mode="genericEvent" />)
  .add('created', () => (
    <CreateEventContent mode="genericEvent" initialState="completed" />
  ))
  .add('travel to constituency', () => (
    <CreateEventContent
      mode="travelToConstituency"
      initialValues={{ category: 'TravelTogether' }}
      constituencyEvents={times(5, someEvent)}
    />
  ))
  .add('travel to event', () => (
    <CreateEventContent
      mode="travelToEvent"
      initialValues={{ category: 'TravelTogether' }}
      targetEvent="someEvent"
    />
  ))

storiesOf('views/Landing', module)
  .add('logged out', () => <LandingPrompt />)
  .add('logged in', () => (
    <LandingPrompt
      user={{
        ...someUser(),
        upcomingCommitments: times(3, someEvent),
        managedEvents: times(3, someEvent),
        isContactable: true
      }}
    />
  ))
  .add('logged in', () => (
    <LandingPrompt
      user={{
        ...someUser(),
        upcomingCommitments: times(3, someEvent),
        managedEvents: times(3, someEvent),
        isContactable: true
      }}
    />
  ))

storiesOf('views/UserPreferences', module)
  .add('logged out', () => <PreferencesPrompt />)
  .add('logged in', () => (
    <PreferencesPrompt
      user={{
        ...someUser(),
        upcomingCommitments: times(3, someEvent),
        managedEvents: times(3, someEvent),
        isContactable: true,
        postcode: 'SW24JE'
      }}
    />
  ))

const commitmentBaseProps = {
  open: true,
  event: someEvent(),
  currentUser: someUser()
}

storiesOf('views/Commitment', module)
  .add('first time', () => <Commitment first {...commitmentBaseProps} />)
  .add('first time in constituency', () => (
    <Commitment firstTimeInConstituency {...commitmentBaseProps} />
  ))
  .add('busy bee', () => <Commitment busy {...commitmentBaseProps} />)
