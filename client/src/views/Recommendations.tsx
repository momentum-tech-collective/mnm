/** @jsx jsx */
import { jsx } from 'theme-ui'
import * as React from 'react'
import PostcodeRecommendations, {
  ActivistMode
} from 'components/PostcodeRecommendations'
import { RouteComponentProps } from 'react-router'
import { parse } from 'querystring'
import { useSavePostcode } from 'data/hooks'
import {
  usePostcodeRecommendations,
  useCategories,
  flattenSubcategories
} from './queries'

const getSubcategories = () => {
  const { data } = useCategories() || {}
  const { categories = [] } = data || {}
  return flattenSubcategories(categories)
}

const Recommendations: React.FC<RouteComponentProps<any>> = ({
  location,
  match: {
    params: { postcode }
  },
  history
}) => {
  useSavePostcode(postcode)
  const subcategories = getSubcategories()
  const { loading, data } = usePostcodeRecommendations(postcode)
  const { events = [], groups = [] } = data || {}
  const { mode } = parse(location.search.substr(1))

  return (
    <PostcodeRecommendations
      postcode={postcode}
      loading={loading}
      events={events}
      groups={groups}
      showEventList={false}
      showGroupList={true}
      subcategories={subcategories}
      setMode={mode =>
        history.replace({ ...location, search: `?mode=${mode}` })
      }
      mode={
        mode === ActivistMode.Effectiveness
          ? ActivistMode.Effectiveness
          : ActivistMode.Convenience
      }
    />
  )
}

export default Recommendations
