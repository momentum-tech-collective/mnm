/** @jsx jsx */

import { jsx } from 'theme-ui'

import gql from 'graphql-tag'
import { Livestreams, Livestreams_livestreams } from './__graphql__/Livestreams'

import * as React from 'react'
import * as Elements from 'components/Elements'

import { Button } from '@material-ui/core'
import ArrowForwardIcon from '@material-ui/icons/ArrowForward'

import { useQuery } from '@apollo/react-hooks'
import { RouteComponentProps } from 'react-router'

import { DateTimeValue } from '../components/DateField'
import { DateTimeFormat } from 'helpers/date'
import * as DateFns from 'date-fns'

import './css/LivestreamPage.css'

export const LIVESTREAMS_QUERY = gql`
  fragment Livestream on LivestreamType {
    id
    name
    link
    description
    organisation
    startTime
    endTime
  }

  query Livestreams {
    livestreams {
      ...Livestream
    }
  }
`

const isLive = (livestream?: Livestreams_livestreams) => {
  if (!livestream) {
    return false
  }
  let startDate = DateFns.parseISO(livestream.startTime)
  let endDate = DateFns.parseISO(livestream.endTime)
  return DateFns.isWithinInterval(new Date(), {
    start: startDate,
    end: endDate
  })
}

const LivestreamPage: React.FC<RouteComponentProps> = () => {
  const result = useQuery<Livestreams>(LIVESTREAMS_QUERY)
  return <LivestreamView {...result.data} />
}

export const LivestreamView: React.FC<Partial<Livestreams>> = ({
  livestreams
}) => {
  return (
    // { /* pseudoPanel */ }
    <div
      sx={{
        // Needs to be important or overridden by theme styles.
        borderRadius: '0 !important',
        width: '100vw',
        maxWidth: '100vw !important',
        padding: '0px',
        display: 'flex',
        flexDirection: 'column',
        height: '100%',
        overflowY: 'auto',
        overflowX: 'hidden',
        WebkitOverflowScrolling: 'touch',
        userSelect: 'none'
      }}
    >
      <div
        sx={{
          height: '100%',
          overflowY: 'auto',
          overflowX: 'auto',
          WebkitOverflowScrolling: 'touch',
          userSelect: 'none',
          maxWidth: '100%',
          display: 'grid',
          gridTemplateColumns: '1fr auto 1fr',
          justifyContent: 'center',
          gridAutoRows: 'min-content'
        }}
      >
        {/* header */}
        <Elements.PanelCard
          sx={{
            gridColumn: '1/4',
            display: 'flex',
            justifyContent: 'center',
            padding: '15px 10px',
            backgroundSize: 'cover',
            backgroundColor: '#3f3f3f',
            color: 'white'
          }}
        >
          <div sx={{ display: 'flex', flexDirection: 'column' }}>
            <Elements.PanelCardTitle
              sx={{
                color: 'white',
                variant: 'heading',
                alignItems: 'center',
                mb: 2,
                fontSize: 7,
                maxWidth: '500px'
              }}
            >
              Join a livestream
            </Elements.PanelCardTitle>
            <span sx={{ fontSize: 5, maxWidth: '500px' }}>
              Organisers and leaders from across the movement are hosting
              trainings and discussion to help build the movement. Sign up
              below.
            </span>
          </div>
        </Elements.PanelCard>

        {livestreams ? (
          livestreams.map((livestream, index) => (
            <Elements.PanelCard
              sep
              key={livestream!!.id}
              sx={{
                gridColumn: '2',
                bg: 'white',
                borderBottom: '1px solid grey',
                borderBottomColor: 'gray.2',
                display: 'grid',
                gridTemplateColumns: '2fr 1fr',
                gridTemplateRows: 'repeat(4, auto)',
                alignItems: 'center',
                justifySelf: 'center',
                padding: '15px 0px',
                maxWidth: '500px',
                width: 'calc(100% - 25px)',
                height: 'auto'
              }}
              className={index + 1 === livestreams.length ? 'padBottom' : ''}
            >
              {/* title */}
              <Elements.PanelCardTitle
                sx={{
                  color: 'text',
                  mb: 2,
                  fontSize: 5,
                  // rowStart, colStart, RowEnd, ColENd
                  gridArea: isLive(livestream) ? '1/1/2/2' : '1/1/2/3'
                }}
              >
                <span sx={{ fontSize: [6] }}>{livestream!!.name}</span>
              </Elements.PanelCardTitle>

              {/* liveButton */}
              {isLive(livestream) ? (
                <div
                  sx={{
                    justifySelf: 'flex-end',
                    alignSelf: 'flex-start'
                  }}
                >
                  <Button
                    variant="contained"
                    color="secondary"
                    disableRipple={true}
                    size="small"
                    sx={{
                      ml: 1,
                      borderWidth: '2px !important',
                      fontWeight: '600 !important',
                      cursor: 'default !important',
                      boxShadow: 'none !important'
                    }}
                  >
                    Live
                  </Button>
                </div>
              ) : (
                <React.Fragment></React.Fragment>
              )}

              {/* description */}
              <p
                sx={{
                  fontFamily: 'heading',
                  gridArea: '2/1/3/3',
                  fontWeight: '500'
                }}
              >
                {livestream!!.description}
              </p>

              {/* organisation */
              livestream!!.organisation && (
                <p
                  sx={{
                    fontFamily: 'heading',
                    fontWeight: '700',
                    fontSize: [4],
                    color: 'primary',
                    gridArea: '3/1/4/3',
                    mt: 0
                  }}
                >
                  by {livestream!!.organisation}
                </p>
              )}

              {/* dateTime + btn row*/}
              <div
                sx={{
                  gridArea: '4/1/5/3',
                  display: 'flex',
                  justifyContent: 'space-between'
                }}
              >
                {/* dateTimes */}
                <div
                  sx={{
                    display: 'flex',
                    flexDirection: 'column'
                  }}
                >
                  <DateTimeValue
                    relative
                    value={livestream!!.startTime}
                    format={DateTimeFormat.DAY_LONG}
                    sx={{
                      fontWeight: '700',
                      fontFamily: 'heading',
                      fontSize: [4],
                      color: 'gray.5'
                    }}
                  />
                  <div>
                    <DateTimeValue
                      value={livestream!!.startTime}
                      format={DateTimeFormat.TIME}
                      lower={true}
                      sx={{
                        fontWeight: '700',
                        fontFamily: 'heading',
                        fontSize: [4]
                      }}
                    />
                    <span> to </span>
                    <DateTimeValue
                      value={livestream!!.endTime}
                      format={DateTimeFormat.TIME}
                      lower={true}
                      sx={{
                        fontWeight: '700',
                        fontFamily: 'heading',
                        fontSize: [4]
                      }}
                    />
                  </div>
                </div>

                {/* btn */}
                <div>
                  <Button
                    variant="outlined"
                    color="primary"
                    href={livestream!!.link}
                    size="large"
                    sx={{
                      ml: 1,
                      borderWidth: '2px !important',
                      fontWeight: '600 !important',
                      gridArea: '4/2/5/3'
                    }}
                  >
                    {isLive(livestream) ? 'Join' : 'Sign up'}
                    <ArrowForwardIcon />
                  </Button>
                </div>
              </div>
            </Elements.PanelCard>
          ))
        ) : (
          <div>Loading</div>
        )}
        <Elements.PanelCard sx={{ gridColumn: '2', borderBottom: 'none' }}>
          <br />
        </Elements.PanelCard>
      </div>
    </div>
  )
}

export default LivestreamPage
