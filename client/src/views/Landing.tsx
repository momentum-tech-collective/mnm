/** @jsx jsx */
import { jsx } from 'theme-ui'
import gql from 'graphql-tag'
import * as React from 'react'
import { RouteComponentProps } from 'react-router'
import PostcodeSearch from 'components/PostcodeSearch'
import { AddGroupButton } from 'components/CommunicationGroupRecommendationsList'
import {
  recommendationType,
  RecommendationType
} from 'components/PostcodeRecommendations'
import { PanelCard, Panel } from 'components/Elements'
import { Recommendations, CreateEvent } from 'routes'
import { usePushRoute } from 'components/Navigation'
import { useQuery } from '@apollo/react-hooks'
import { LandingPage } from './__graphql__/LandingPage'
import { useAuth } from '../data/hooks'
import {
  EventCategoryFragment,
  EventSubcategoryFragment
} from '../event/EventFragments'

import { LinkButton } from '../components/Elements'
import { Add } from '@material-ui/icons'

export const LANDING_PAGE_QUERY = gql`
  ${EventCategoryFragment}
  ${EventSubcategoryFragment}

  fragment UserEvent on EventType {
    id
    properties {
      startTime
      address
      postcode
      name
      description
      ...EventCategoryFragment
      ...EventSubcategoryFragment
      isFull
      amAttending
      constituency {
        id
        name
      }
    }
  }

  query LandingPage {
    user: currentUser {
      id
      email
      firstName
      lastName
      upcomingCommitments {
        ...UserEvent
      }
      managedEvents {
        ...UserEvent
      }
    }
  }
`

const Landing: React.FC<RouteComponentProps> = () => {
  const { loggedIn } = useAuth()
  const { data } = useQuery<LandingPage>(LANDING_PAGE_QUERY, {
    skip: !loggedIn
  })

  return <LandingPrompt {...data} />
}

export const LandingPrompt: React.FC<Partial<LandingPage>> = ({ user }) => {
  const navigateRecommendations = usePushRoute(Recommendations)

  return (
    <React.Fragment>
      <Panel
        sx={{
          gridTemplateRows: 'repeat(3, min-content) 1fr'
        }}
      >
        <PanelCard
          sx={{
            variant: 'rows',
            justifyContent: 'flex-end',
            pb: 4,
            height: 350,
            backgroundSize: 'cover',
            backgroundImage: `url("${require('../images/housing_header.png')}")`,
            color: 'white',
            maxHeight: '30vh',
            minHeight: 100
          }}
        >
          <img
            src={`${require('../images/logos/logo_1x.png')}`}
            sx={{
              px: 1,
              width: 'max-content',
              objectFit: 'contain'
            }}
          />
        </PanelCard>

        <PanelCard
          sx={{
            px: 4,
            mt: [null, 5]
          }}
        >
          <p
            sx={{
              mt: 0,
              mb: 3,
              fontSize: 6,
              fontFamily: 'heading',
              fontWeight: '600'
            }}
          >
            Organise events, plot them on the map, and build power at the
            grassroots.
          </p>
          <p
            sx={{
              mt: 0,
              mb: 3,
              fontSize: 5,
              fontFamily: 'heading',
              fontWeight: '600'
            }}
          >
            By knocking on doors, by blocking evictions, by getting rooted in
            your community, you can help turn the tide. The retreat is over. Now
            it’s time to build the movement.
          </p>

          <PostcodeSearch
            onSubmit={async postcode => {
              navigateRecommendations(`/recommendations/${postcode}`)
            }}
          />
        </PanelCard>
        <PanelCard
          sx={{
            padding: '10px 30px'
          }}
        >
          {recommendationType === RecommendationType.Groups ? (
            <AddGroupButton text="Add your group to the map" />
          ) : (
            <LinkButton
              route={CreateEvent}
              to={`/create-event`}
              startIcon={<Add />}
              color="primary"
            >
              Add your event to the map 🗓
            </LinkButton>
          )}
        </PanelCard>
      </Panel>
    </React.Fragment>
  )
}

export default Landing
