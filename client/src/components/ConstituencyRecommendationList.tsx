/** @jsx jsx */

import { jsx } from 'theme-ui'
import React from 'react'
import gql from 'graphql-tag'
import { Button } from '@material-ui/core'
import { Constituency } from 'routes'
import { ConstituencyRecommendation } from './__graphql__/ConstituencyRecommendation'
import { PanelCardTitle, CalloutCard, LinkButton } from './Elements'

export const ConstituencyRecommendationsList: ComponentWithFragment<{
  recommendations: ConstituencyRecommendation[]
  loading?: boolean
  start: string
}> = ({ recommendations, start }) => {
  if (recommendations.length === 0) {
    return null
  }

  return (
    <div
      sx={{
        backgroundImage: `url("${require('../images/crowd.jpg')}")`,
        backgroundSize: 'cover',
        borderBottom: 'strongThick'
      }}
    >
      <CalloutCard sx={{ fontSize: [2, 3] }}>
        {recommendations.map(({ id, constituency }) => (
          <div sx={{ my: 2 }} key={id}>
            <div>
              <PanelCardTitle
                sx={{
                  color: 'black',
                  variant: 'columns',
                  alignItems: 'center',
                  mb: 2
                }}
              >
                <span sx={{ fontSize: [4, 5], flex: 1 }}>
                  {constituency.name}
                </span>
              </PanelCardTitle>
            </div>

            <LinkButton
              to={{
                pathname: `/constituency/${constituency.id}`,
                search: `?start=${start}`
              }}
              route={Constituency}
              color="primary"
              key={id}
            >
              I’ll campaign here →
            </LinkButton>
          </div>
        ))}
      </CalloutCard>
    </div>
  )
}

ConstituencyRecommendationsList.fragment = gql`
  fragment ConstituencyRecommendation on ConstituencyRecommendationType {
    id
    constituency {
      id
      name
      volunteerNeedBand
    }
  }
`
