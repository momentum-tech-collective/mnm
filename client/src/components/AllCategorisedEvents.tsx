/** @jsx jsx */

import { jsx } from 'theme-ui'
import * as React from 'react'
import { Recommendations } from 'routes'
import { Panel, PanelHeader, PanelCard } from './Elements'
import { EventRecommendationsList } from '../event/EventRecommendationList'
import { usePushRoute } from './Navigation'
import PostcodeSearch from './PostcodeSearch'
import { CategorisedEventsView_events } from '../views/__graphql__/CategorisedEventsView'

const AllCategorisedEvents: ComponentWithFragment<{
  events: CategorisedEventsView_events[]
  category: string
  date: string
  loading?: boolean
}> = ({ events, category, date, loading }) => {
  const navigateRecommendations = usePushRoute(Recommendations)

  const prettyCategory = `${category.slice(0, 1).toUpperCase()}${category
    .slice(1)
    .toLowerCase()}`

  return (
    <React.Fragment>
      <Panel sx={{ bg: '#F8F8F8' }}>
        <PanelHeader
          breadCrumbs={[
            { to: '/', text: 'Home' },
            { to: '/', text: `${prettyCategory} Events` }
          ]}
        />
        <PanelCard>
          <p>
            This page shows{' '}
            <b>
              all <span sx={{ textTransform: 'capitalize' }}>{category}</span>{' '}
              events
            </b>{' '}
            happening in the next few days. If you want to focus on marginals
            and areas in urgent need of help, search your postcode:
          </p>
          <PostcodeSearch
            onSubmit={postcode =>
              navigateRecommendations(`/recommendations/${postcode}/`)
            }
          />
        </PanelCard>
        <EventRecommendationsList
          recommendations={events}
          loading={loading}
          eventsPerDay={200}
        />
      </Panel>
    </React.Fragment>
  )
}

export default AllCategorisedEvents
