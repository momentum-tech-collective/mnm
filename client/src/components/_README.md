Components are stateless and context-unaware; they take input only through props.

As much as possible, separate out the component from the composition and the view.
