import * as React from 'react'
import gql from 'graphql-tag'
import { AutocompleteField } from './AutocompleteField'
import AutocompleteFromQuery from 'components/AutocompleteFromQuery'

const QUERY = gql`
  query Constituencies {
    constituencies {
      value: id
      label: name
    }
  }
`

const ConstituencyInput: React.FC<
  Omit<GetComponentProps<typeof AutocompleteField>, 'suggestions'>
> = props => (
  <AutocompleteFromQuery
    query={QUERY}
    getList={d => d.constituencies}
    {...props}
  />
)

export default React.memo(ConstituencyInput)
