import { storiesOf } from '@storybook/react'
import { action } from '@storybook/addon-actions'
import { someEvent } from 'helpers/fakes'
import { times } from 'lodash'
import React from 'react'
import { TargetEventList } from './TargetEventPicker'

storiesOf('components/TargetEventPicker', module).add('default', () => (
  <TargetEventList
    label="Choose an event"
    value={someEvent().id}
    onChange={action('change')}
    options={times(10, someEvent)}
  />
))
