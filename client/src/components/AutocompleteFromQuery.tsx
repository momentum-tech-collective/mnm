import * as React from 'react'
import { get, take, compact } from 'lodash'
import gql from 'graphql-tag'
import { useQuery } from '@apollo/react-hooks'
import { AutocompleteField } from './AutocompleteField'

const AutocompleteFromQuery: React.FC<
  Omit<GetComponentProps<typeof AutocompleteField>, 'suggestions'> & {
    query: any
    getList: any
  }
> = ({ query, getList, ...props }) => {
  const res = useQuery(query)

  const list: any[] = React.useMemo(
    () => (res.data ? compact(getList(res.data)) : []),
    [res, getList]
  )

  return (
    <AutocompleteField
      suggestions={list}
      disabled={!!res.loading || !!res.error}
      {...props}
    />
  )
}

export default React.memo(AutocompleteFromQuery)
