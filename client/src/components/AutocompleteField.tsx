import React, { Ref } from 'react'
import deburr from 'lodash/deburr'
import Downshift, { ControllerStateAndHelpers } from 'downshift'
import { makeStyles } from '@material-ui/core/styles'
import TextField, { TextFieldProps } from '@material-ui/core/TextField'
import Paper from '@material-ui/core/Paper'
import MenuItem from '@material-ui/core/MenuItem'

interface Suggestion {
  label: string
  value: string
}

function renderInput(
  inputProps: Partial<TextFieldProps> & {
    ref?: Ref<HTMLInputElement>
  }
) {
  const { InputProps, ref, ...other } = inputProps

  return (
    <TextField
      variant="outlined"
      InputProps={{
        inputRef: ref,
        ...InputProps
      }}
      {...(other as any)}
    />
  )
}

function renderSuggestion(suggestionProps: {
  suggestion: Suggestion
  index: number
  itemProps: {}
  highlightedIndex: number | null
  selectedItem: string
}) {
  const {
    suggestion,
    index,
    itemProps,
    highlightedIndex,
    selectedItem
  } = suggestionProps
  const isHighlighted = highlightedIndex === index
  const isSelected = (selectedItem || '').indexOf(suggestion.label) > -1

  return (
    <MenuItem
      {...itemProps}
      key={suggestion.label}
      selected={isHighlighted}
      component="div"
      style={{
        fontWeight: isSelected ? 500 : 400
      }}
    >
      {suggestion.label}
    </MenuItem>
  )
}

function getSuggestions(
  value: string = '',
  suggestions: Suggestion[],
  { showEmpty = false } = {}
) {
  const inputValue = deburr(value.trim()).toLowerCase()
  const inputLength = inputValue.length
  let count = 0

  return inputLength === 0 && !showEmpty
    ? []
    : suggestions.filter(suggestion => {
        const keep =
          count < 5 &&
          suggestion.label.slice(0, inputLength).toLowerCase() === inputValue

        if (keep) {
          count += 1
        }

        return keep
      })
}

const useStyles = makeStyles(theme => ({
  container: {
    position: 'relative'
  },
  paper: {
    position: 'absolute',
    zIndex: 10,
    marginTop: theme.spacing(1),
    left: 0,
    right: 0
  },
  chip: {
    margin: theme.spacing(0.5, 0.25)
  },
  inputRoot: {
    flexWrap: 'wrap'
  },
  inputInput: {
    width: 'auto',
    flexGrow: 1
  },
  divider: {
    height: theme.spacing(2)
  }
}))

export const AutocompleteField: ViewElement<
  {
    suggestions: Suggestion[]
    value: string
    onChange: (x: string) => void
    onSearchChange?: (x: string) => void
    loading?: boolean
  },
  TextFieldProps
> = ({
  suggestions,
  value,
  onChange,
  placeholder,
  onSearchChange,
  ...props
}) => {
  const classes = useStyles()
  const suggestion = suggestions.find(s => s.value === value)

  return (
    <Downshift
      onChange={item => onChange(item.value)}
      onInputValueChange={onSearchChange}
      selectedItem={suggestion && suggestion.label}
    >
      {({
        getInputProps,
        getItemProps,
        getLabelProps,
        getMenuProps,
        highlightedIndex,
        inputValue,
        isOpen,
        selectedItem
      }: ControllerStateAndHelpers<Suggestion>) => {
        const { onBlur, onFocus, ...inputProps } = getInputProps({
          placeholder
        })

        return (
          <div className={classes.container}>
            {renderInput({
              fullWidth: true,
              classes,
              InputLabelProps: getLabelProps(),
              InputProps: { onBlur, onFocus },
              inputProps,
              value: inputValue,
              ...props
            })}

            <div {...getMenuProps()}>
              {isOpen ? (
                <Paper className={classes.paper} square>
                  {getSuggestions(inputValue || '', suggestions).map(
                    (suggestion, index) =>
                      renderSuggestion({
                        suggestion,
                        index,
                        itemProps: getItemProps({ item: suggestion }),
                        highlightedIndex,
                        selectedItem
                      })
                  )}
                </Paper>
              ) : null}
            </div>
          </div>
        )
      }}
    </Downshift>
  )
}
