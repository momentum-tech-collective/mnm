/** @jsx jsx */

import { jsx } from 'theme-ui'
import { Radio } from '@material-ui/core'
import gql from 'graphql-tag'
import { useQuery } from '@apollo/react-hooks'
import { Field, FieldProps } from 'formik'
import { sortBy } from 'lodash'
import { EventCategory } from 'data/event'
import { LoadingArea } from './Elements'
import { RadioOptionGroup } from './Form'
import { TargetEventListItem } from './__graphql__/TargetEventListItem'
import { DateTimeValue, DateTimeFormat } from './DateField'
import {
  TargetEventPickerQuery,
  TargetEventPickerQueryVariables
} from './__graphql__/TargetEventPickerQuery'
import { EventCategoryFragment } from '../event/EventFragments'

export const TargetEventList: ComponentWithFragment<{
  label: string
  value?: string
  options: TargetEventListItem[]
  onChange: (id: string) => void
}> = ({ value, options, onChange, ...props }) => {
  return (
    <RadioOptionGroup {...props}>
      {sortBy(options, x => x.properties!.startTime).map(
        ({ id, properties }) => (
          <div
            key={id}
            onClick={() => onChange(id)}
            sx={{
              variant: 'columns',
              borderBottom: 'light',
              cursor: 'pointer',
              alignItems: 'center',
              p: 3,
              ...(value === id && { color: 'primary' })
            }}
          >
            <div sx={{ flex: 1 }}>
              <div sx={{ fontSize: [3, 4] }}>{properties!.name}</div>
              <div sx={{ fontSize: [2], mt: 1 }}>
                <DateTimeValue
                  sx={{ fontWeight: 'bold', mr: 1 }}
                  value={properties!.startTime}
                  format={DateTimeFormat.TIME}
                  lower
                />
                <DateTimeValue
                  relative
                  value={properties!.startTime}
                  format={DateTimeFormat.DAY_LONG}
                />
              </div>
              <div sx={{ fontSize: [2] }}>{properties!.address}</div>
            </div>

            <Radio size="medium" checked={value === id} />
          </div>
        )
      )}
    </RadioOptionGroup>
  )
}

TargetEventList.fragment = gql`
  fragment TargetEventListItem on EventType {
    id
    properties {
      name
      startTime
      address
    }
  }
`

const TARGET_EVENT_PICKER_QUERY = gql`
  ${TargetEventList.fragment}
  ${EventCategoryFragment}

  query TargetEventPickerQuery($constituencyId: String!) {
    constituency(id: $constituencyId) {
      id
      upcomingEvents {
        ...TargetEventListItem
        properties {
          ...EventCategoryFragment
        }
      }
    }
  }
`

export const TargetEventPicker: ViewElement<{
  constituencyId?: string
  label: string
  onChange: (x: string) => void
  value?: string
}> = ({ name, constituencyId, ...props }) => {
  const { data, loading, error } = useQuery<
    TargetEventPickerQuery,
    TargetEventPickerQueryVariables
  >(TARGET_EVENT_PICKER_QUERY, {
    variables: { constituencyId: constituencyId! },
    skip: !constituencyId
  })

  if (loading) {
    return <LoadingArea>Loading events…</LoadingArea>
  }

  if (error) {
    throw error
  }

  if (!data) {
    return null
  }

  return (
    <TargetEventList
      options={data!.constituency!.upcomingEvents.filter(
        e => e.properties!.category === EventCategory.CAMPAIGNING
      )}
      {...props}
    />
  )
}

export const FormTargetEventPicker: ViewElement<{
  name: string
  constituencyId?: string
  label: string
}> = ({ name, ...props }) => (
  <Field name={name}>
    {({ field, form }: FieldProps) => (
      <TargetEventPicker
        {...props}
        value={field.value}
        onChange={x => form.setFieldValue(name, x)}
      />
    )}
  </Field>
)
