import { InputAdornment, IconButton } from '@material-ui/core'
import { Formik } from 'formik'
import { DoubleArrow } from '@material-ui/icons'
import { useAuth, useAuthenticatedCallback, useLocalStorage } from 'data/hooks'
import { useApolloClient } from '@apollo/react-hooks'
import React from 'react'
import { PanelCard, PanelCardTitle, FormWrapper, CalloutCard } from './Elements'
import { Input } from './Form'
import { DATA_CAPTURE } from './PostcodeRecommendations'

export const DataCapturePanel: ViewElement<{
  postcode: string
}> = ({ postcode }) => {
  const auth = useAuth()
  const [authenticate, authModal] = useAuthenticatedCallback()
  const apollo = useApolloClient()
  const [emailSubmitted, setEmailSubmitted] = useLocalStorage('email-submitted')
  if (auth.loggedIn || emailSubmitted) {
    return (
      <React.Fragment>
        <div sx={{ mb: 4 }} />
        {authModal}
      </React.Fragment>
    )
  }
  return (
    <CalloutCard>
      {authModal}

      <PanelCardTitle
        sx={{ fontSize: [4, 6], color: 'primary', alignSelf: 'stretch' }}
      >
        Events straight to your inbox
      </PanelCardTitle>

      <p
        sx={{
          m: 0,
          mb: 2,
          variant: 'description',
          color: 'black'
        }}
      >
        We need your email address to help you get involved in the campaign and
        make the biggest impact. Enter your email now 👇
      </p>

      <Formik
        onSubmit={({ email }) => {
          apollo.mutate({
            mutation: DATA_CAPTURE,
            variables: { postcode, email }
          })
          authenticate({
            mode: 'register',
            initialValues: {
              postcode,
              email
            }
          })
          setEmailSubmitted('true')
        }}
      >
        {({ handleSubmit }) => (
          <FormWrapper sx={{ width: '100%' }} onSubmit={handleSubmit}>
            <Input
              fullWidth
              name="email"
              InputProps={{
                endAdornment: (
                  <InputAdornment position="end">
                    <IconButton type="submit">
                      <DoubleArrow />
                    </IconButton>
                  </InputAdornment>
                )
              }}
            />
          </FormWrapper>
        )}
      </Formik>

      <div sx={{ variant: 'caption', fontSize: 2, mt: 3 }}>
        By entering your email, you are consenting to receive communications
        about Momentum's campaigns, campaigns we support, and how you can
        support and be part of them. For more information please see our{' '}
        <a href="https://peoplesmomentum.com/privacy-policy" target="_blank">
          privacy policy
        </a>
      </div>
    </CalloutCard>
  )
}
