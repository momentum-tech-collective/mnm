/** @jsx jsx */
import { jsx } from 'theme-ui'
import * as React from 'react'
import { CircularProgress } from '@material-ui/core'

const DefaultLoading: React.FC = () => (
  <div
    sx={{
      width: '100%',
      height: '100%',
      top: 0,
      left: 0,
      position: 'absolute',
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'center'
    }}
  >
    <CircularProgress color="primary" size={32} />
  </div>
)
