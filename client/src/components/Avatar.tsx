/** @jsx jsx */
import { jsx } from 'theme-ui'
import md5 from 'md5'

const Avatar: React.FC<{
    email: string,
    size?: number
}> = ({ email, size = 100 }) => <img
    src={`https://www.gravatar.com/avatar/${md5(email)}?d=monsterid&s=${size}`}
    sx={{ borderRadius: '100%', width: size }}
/>

export default Avatar