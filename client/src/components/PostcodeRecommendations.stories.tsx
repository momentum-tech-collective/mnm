import React from 'react'
import { storiesOf } from '@storybook/react'
import { someConstituencyRecommendation, someEvent } from 'helpers/fakes'
import { times } from 'lodash'
import { action } from '@storybook/addon-actions'
import PostcodeRecommendations, {
  ActivistMode
} from './PostcodeRecommendations'

const baseProps = {
  postcode: 'AB1 2XY',
  setMode: action('setMode'),
  events: times(10, someEvent),
  constituencies: times(2, someConstituencyRecommendation)
}

storiesOf('components/PostcodeRecommendations', module)
  .add('Loading', () => <PostcodeRecommendations loading {...baseProps} />)
  .add('Convenience Mode', () => (
    <PostcodeRecommendations {...baseProps} mode={ActivistMode.Convenience} />
  ))
  .add('Convenience Mode (in marginal)', () => (
    <PostcodeRecommendations {...baseProps} mode={ActivistMode.Convenience} />
  ))
  .add('Convenience Mode (travel together group exists)', () => (
    <PostcodeRecommendations {...baseProps} mode={ActivistMode.Convenience} />
  ))
  .add('Convenience Mode (whatsapp group)', () => (
    <PostcodeRecommendations
      {...baseProps}
      mode={ActivistMode.Convenience}
      eventRecommendations={baseProps.eventRecommendations.map(er => ({
        ...er,
        properties: { ...er.properties, whatsappLink: 'http://whats.app' }
      }))}
    />
  ))
  .add('Effectiveness Mode', () => (
    <PostcodeRecommendations {...baseProps} mode={ActivistMode.Effectiveness} />
  ))
