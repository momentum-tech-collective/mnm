/** @jsx jsx */
import { jsx } from 'theme-ui'
import * as React from 'react'
import { validatePostcode } from 'data/validators'
import { getDeviceLocation } from 'data/device'
import {
  convertCoordinatesToPostcode,
  convertLocationToCoordinates
} from 'data/geo'
import {
  TextField,
  InputAdornment,
  IconButton,
  Button,
  makeStyles,
  createStyles,
  Paper,
  InputBase,
  Divider,
  Theme,
  CircularProgress
} from '@material-ui/core'
import { useField } from 'react-jeff'
import { TextFieldProps } from '@material-ui/core/TextField'
import { MyLocation, Search, Send } from '@material-ui/icons'
import Postcode from 'postcode'
import { FormWrapper } from './Elements'

type PostcodeSearchType = 'search' | 'update'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      padding: '2px 4px',
      display: 'flex',
      alignItems: 'center',
      width: 400
    },
    input: {
      marginLeft: theme.spacing(1),
      flex: 1
    },
    iconButton: {
      margin: 2,
      minWidth: 0,
      flexShrink: 0
    },
    divider: {
      height: 28
    }
  })
)

const PostcodeSearch: ViewElement<
  {
    initialValue?: string
    label?: string
    onSubmit: (postcode: string) => void
    inputType?: PostcodeSearchType
  },
  TextFieldProps
> = ({
  onSubmit,
  label = 'Your Location or Postcode',
  initialValue = '',
  inputType = 'search'
}) => {
  const classes = useStyles()

  const input = useField({
    defaultValue: initialValue || ''
  })
  const [loading, setLoading] = React.useState<boolean>(false)

  const handleSubmit = React.useCallback(async () => {
    let normalized
    if (validatePostcode(input.value.trim())) {
      normalized = new Postcode(input.value.trim()).normalise()
    } else {
      const locationCoords = await convertLocationToCoordinates(
        input.value.trim()
      )
      const formattedCoords = {
        longitude: locationCoords[0],
        latitude: locationCoords[1]
      }
      const postcode = await convertCoordinatesToPostcode(formattedCoords)
      normalized = new Postcode(postcode).normalise()
    }

    if (normalized) {
      onSubmit(normalized)
    }
  }, [input.valid, input.dirty, input.value, onSubmit])

  const geolocate = React.useCallback(() => {
    ;(async () => {
      setLoading(true)
      try {
        const coords = await getDeviceLocation()
        const postcode = await convertCoordinatesToPostcode(coords)
        input.setValue(postcode)
        onSubmit(postcode)
      } catch (e) {
        setLoading(false)
      } finally {
        setLoading(false)
      }
    })()
  }, [input.setValue])

  return (
    <FormWrapper
      sx={{
        variant: 'columns',
        justifyContent: 'center',
        alignItems: 'center',
        position: 'relative'
      }}
      onSubmit={handleSubmit}
    >
      <Paper className={classes.root} sx={{ width: '100% !important' }}>
        <InputBase
          {...input.props}
          onChange={e => input.setValue(e.currentTarget.value)}
          className={classes.input}
          placeholder={label}
          inputProps={{ 'aria-label': label }}
        />
        <IconButton
          type="submit"
          className={classes.iconButton}
          disabled={(!input.dirty && !initialValue) || !input.valid}
          aria-label="search"
        >
          {inputType == 'search' ? <Search /> : <Send />}
        </IconButton>

        <Divider className={classes.divider} orientation="vertical" />

        <IconButton
          className={classes.iconButton}
          edge="end"
          onClick={geolocate}
          disabled={loading}
          aria-label="use my location"
        >
          {loading ? <CircularProgress size={16} /> : <MyLocation />}
        </IconButton>
      </Paper>
    </FormWrapper>
  )
}

export default PostcodeSearch
