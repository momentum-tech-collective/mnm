import * as React from 'react'

import styled from '@emotion/styled'

const Ellipsis: React.FC<
  React.DetailedHTMLProps<
    React.HTMLAttributes<HTMLSpanElement>,
    HTMLSpanElement
  >
> = styled(({ className, ...props }) => <span {...props} />)`
  & {
    padding-right: 1em;
  }

  &:after {
    overflow: hidden;
    position: absolute;
    display: inline-block;
    vertical-align: bottom;
    animation: ellipsis steps(4, end) 900ms infinite;
    content: '\\2026'; /* ascii code for the ellipsis character */
    width: 0em;
  }

  @keyframes ellipsis {
    to {
      width: 1em;
    }
  }
`

export default Ellipsis
