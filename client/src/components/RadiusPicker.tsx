/** @jsx jsx */
import React from 'react'
import { jsx } from 'theme-ui'
import MenuItem from '@material-ui/core/MenuItem'
import FormControl from '@material-ui/core/FormControl'
import Select from '@material-ui/core/Select'
import { makeStyles } from '@material-ui/core'
import { theme } from 'data/theme'
import { PanelCard, LinkButton, PanelCardTitle } from './Elements'

const useMenuStyle = makeStyles({
  root: {
    backgroundColor: theme.colors.contrast[0]
  }
})

export const RadiusPicker: ViewElement<
  GetComponentProps<typeof PanelCard> & {
    updateRadius: (radius: number) => any
    radius: number
  }
> = ({ updateRadius, radius, ...props }) => {
  const menuStyle = useMenuStyle()
  const handleChange = (event: React.ChangeEvent<{ value: unknown }>) => {
    updateRadius(event.target.value as number)
  }

  const radiusOptions = [40, 50, 75, 100]

  return (
    <PanelCard {...props}>
      <PanelCardTitle>Search radius</PanelCardTitle>
      <FormControl>
        <Select
          classes={menuStyle}
          id="search-radius-select"
          value={radius}
          onChange={handleChange}
        >
          {radiusOptions.map(option => (
            <MenuItem value={option}>{option}km</MenuItem>
          ))}
        </Select>
      </FormControl>
    </PanelCard>
  )
}
