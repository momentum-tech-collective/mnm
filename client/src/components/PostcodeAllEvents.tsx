/** @jsx jsx */

import { jsx } from 'theme-ui'
import * as React from 'react'
import { isSameDay, addDays, format } from 'date-fns'
import { Field, useField } from 'react-jeff'
import { range, take, uniqBy, flatMap } from 'lodash'
import { CreateEvent, Recommendations, AllEvents } from 'routes'
import {
  Tabs,
  Tab,
  TextField,
  createStyles,
  makeStyles,
  Theme,
  useTheme,
  useMediaQuery,
  Fab
} from '@material-ui/core'
import { Form, FormikProps } from 'formik'
import { Add } from '@material-ui/icons'
import gql from 'graphql-tag'
import { theme } from 'data/theme'
import { useAnalytics } from 'data/analytics'

import {
  RecommendationView,
  RecommendationView_events
} from 'views/__graphql__/RecommendationView'
import { useFeatureFlag } from 'data/hooks'
import {
  Panel,
  LinkButton,
  PanelHeader,
  PanelCard,
  ToggleGroup,
  ToggleButton
} from './Elements'
import { EventRecommendationsList } from '../event/EventRecommendationList'
import { usePushRoute } from './Navigation'
import { AddButton } from './AddButton'
import { RadiusPicker } from './RadiusPicker'
import { PushLink } from './Navigation'

const PostcodeAllEvents: ComponentWithFragment<{
  events: RecommendationView_events[]
  postcode: string
  date: string
  loading?: boolean
  radius: number
}> = ({ events, postcode, date, loading, radius }) => {
  const navigate = usePushRoute([AllEvents], { replace: true })
  const updateRadius = (newRadius: number) => {
    navigate(`/allevents/${date}/${postcode}/${newRadius}`)
  }

  const parsedDate = new Date(date)
  let options = {
    weekday: 'long',
    month: 'long',
    day: 'numeric'
  }
  const dateString = parsedDate.toLocaleDateString('en-UK', options) // ` ${parsedDate.getDay()} ${parsedDate.getFul }`

  return (
    <React.Fragment>
      <Panel sx={{ bg: '#F8F8F8' }}>
        <PanelHeader
          breadCrumbs={[
            { to: '/', text: 'Home' },
            {
              to: `/allevents/${date}/${postcode}/${radius}`,
              text: `Events near ${postcode}`
            }
          ]}
        />
        <PanelCard>
          <p>
            This page shows <i>all</i> events that are happening on
            {` ${dateString}`}. If you want to focus on marginals and areas in
            urgent need of help, see{' '}
            <PushLink
              to={`/recommendations/${postcode}`}
              route={Recommendations}
            >
              recommendations
            </PushLink>
            {'.'}
          </p>
        </PanelCard>
        <RadiusPicker updateRadius={updateRadius} radius={radius} />

        <EventRecommendationsList
          recommendations={events}
          loading={loading}
          eventsPerDay={200}
        />
      </Panel>
    </React.Fragment>
  )
}

export default PostcodeAllEvents
