import { FC, ReactElement, useRef, cloneElement } from 'react'

export const LockElementWidth: FC<{ children: ReactElement<any> }> = ({
  children
}) => {
  const ref = (refEl: HTMLElement) => {
    if (refEl) {
      refEl.style.width = refEl.clientWidth + 'px'
    }
  }

  return cloneElement(children, { ref })
}
