/** @jsx jsx */
import { jsx } from 'theme-ui'
import * as React from 'react'

const ErrorMessage: React.FC<{ message?: any }> = ({ message }) => (
  <div sx={{ variant: 'card', m: 3 }}>{message}. Try refreshing.</div>
)

export default ErrorMessage
