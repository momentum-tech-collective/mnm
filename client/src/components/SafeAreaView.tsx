import styled from '@emotion/styled'

/** Adds padding to the top of the view to push out away from status bar on ios */
export const SafeAreaView = styled.div`
  padding-top: constant(safe-area-inset-top); /* iOS 11.0 */
  padding-top: env(safe-area-inset-top); /* iOS 11.2 */
`
