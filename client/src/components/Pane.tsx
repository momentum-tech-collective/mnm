/** @jsx jsx */
import { jsx } from 'theme-ui'
import * as React from 'react'
import { SpringUp } from 'components/SpringUp'
import { isMobile } from 'react-device-detect'
import styled from '@emotion/styled'
import { useSpring, animated, config } from 'react-spring'
import { useGesture } from 'react-use-gesture'
import { clamp } from 'lodash'
import { theme } from '../data/theme'

const Pane: React.FC<{
  fullscreen?: boolean
  scrollable?: boolean
  offset?: number
  threshold?: number
}> = ({
  children,
  fullscreen = false,
  scrollable = true,
  offset = window.innerHeight - theme.paneHeight,
  threshold = offset / 2
}) => {
  const [isFullscreen, setIsFullscreen] = React.useState(fullscreen)

  const yScroll = React.useRef(0)
  const domRef = React.useRef<HTMLDivElement>()

  const [scrollProps, set] = useSpring<{ y: number }>(() => ({
    y: fullscreen ? -offset : 0,
    config: config.stiff
  }))

  const settleOnY = (y: number, immediate = false) => {
    set({ y, immediate } as any)
    // To be able to callback to the y value,
    // on the assumption that this is the only function that affects it.
    // TODO: Think of a better way, that pulls it from the event ref?
    yScroll.current = y
  }

  const goFullScreen = () => {
    setIsFullscreen(true)
  }

  const goPulledDown = () => {
    setIsFullscreen(false)
  }

  // TODO: Derive as much as possible from the DOM and from State, not refs
  React.useEffect(() => {
    if (isFullscreen) {
      settleOnY(-offset)
      setIsFullscreen(true)
    } else {
      // then revert the thing back to the offset - i.e. y = 0 and fullscreen off
      settleOnY(0)
      domRef.current.scrollTop = 0
    }
  }, [isFullscreen, goFullScreen, goPulledDown, settleOnY])

  const swipeProps: any = useGesture({
    // passive: true,
    // @ts-ignore
    onDrag: ({
      last,
      cancel,
      down: mouseDown,
      delta: [, delta],
      velocities: [, velocity]
    }) => {
      if (!scrollable) {
        return
      }

      const direction = delta > 0 ? 'down' : 'up'

      if (!isFullscreen) {
        const nextY = delta
        const quickSwipeUp = direction === 'up' && Math.abs(velocity) > 1.5
        const nearlyFullscreen = nextY < 0 && nextY <= -threshold

        if ((direction === 'up' && nearlyFullscreen) || quickSwipeUp) {
          cancel && cancel()
          return goFullScreen()
        }

        if (!mouseDown) {
          // If scrolling down (the wrong direction) then
          // spring the Pane back to its original position when the mouse is released
          return settleOnY(0)
        }

        // Else just directly control the translateY property by dragging
        set({ y: nextY, immediate: true } as any)
      }
    },
    onWheel: ({ last, velocity, delta: [, delta], cancel }) => {
      if (!scrollable) return

      const nextY = yScroll.current + delta
      const nextActualY = clamp(nextY, 0, domRef.current.scrollHeight)
      yScroll.current = nextActualY

      const direction = delta > 0 ? 'down' : 'up'

      if (isFullscreen) {
        // If scrolled to the top
        // and scroll intent is at the threshold
        const nearlyScrolledUp = -nextY > threshold
        // and still scrolling
        const scrollingAboveTop = nextY <= 0
        // or scroll velocity is hard
        // const quickScrollUp = direction === "up" && Math.abs(velocity) > 1.5;

        // if at the top and still scrolling, make it respond as though dragging
        if (!last && direction === 'up' && scrollingAboveTop) {
          set({ y: -offset - delta } as any)
        }

        // then revert the thing back to the offset - i.e. y = 0 and fullscreen off
        if (direction === 'up' && nearlyScrolledUp) {
          cancel && cancel()
          return goPulledDown()
        }

        // else spring the thing back to its fullscreen position
        if (last && nextY < 0) {
          cancel && cancel()
          yScroll.current = 0
          return set({ y: -offset, immediate: false } as any)
        }
      } else {
        const nearlyScrolledUp = delta > threshold
        const quickScrollUp = direction === 'down' && Math.abs(velocity) > 1.5

        if (!last && direction === 'down') {
          set({ y: -delta })
        }

        if ((direction === 'down' && nearlyScrolledUp) || quickScrollUp) {
          cancel && cancel()
          return goFullScreen()
        }

        if (last) {
          yScroll.current = 0
          return set({ y: 0, immediate: false } as any)
        }
      }
    }
  })

  return (
    <React.Fragment>
      {/* This is the initial spacing */}
      <div sx={{ height: offset, pointerEvents: 'none' }} />
      <SpringUp
        from={
          fullscreen && !scrollable
            ? {}
            : { transform: `translate3d(0, 30px, 0)`, opacity: 0.5 }
        }
      >
        <NoScrollbar
          ref={domRef}
          {...swipeProps()}
          style={{ transform: scrollProps.y.interpolate(transY) }}
          sx={{
            variant: 'card',
            overflowY: isFullscreen ? 'scroll' : 'hidden',
            height: isFullscreen ? '100vh' : 'auto',
            minHeight: '100vh',
            borderRadius: isFullscreen ? 0 : undefined,
            boxShadow:
              ' 0px 1px 2px 2px rgba(0, 0, 0, 0.03), 0px 15px 20px 6px rgba(0, 0, 0, 0.08)',
            pt: isFullscreen ? 5 : 0 // for the header
          }}
        >
          {scrollable &&
            (isFullscreen ? (
              <div
                sx={{
                  mb: 2,
                  p: 3,
                  backgroundColor: 'gray.1',
                  borderRadius: 5,
                  width: '100%',
                  textAlign: 'center'
                }}
                onMouseDown={goPulledDown}
                onClick={goPulledDown}
              >
                Collapse ↓
              </div>
            ) : (
              <div sx={{ textAlign: 'center', my: 2 }}>
                <div
                  sx={{
                    width: 50,
                    borderRadius: 20,
                    height: 5,
                    backgroundColor: 'gray.3',
                    display: 'inline-block'
                  }}
                />
              </div>
            ))}

          {children}
        </NoScrollbar>
      </SpringUp>
    </React.Fragment>
  )
}

const transY = y => `translateY(${y}px)`

export const NoScrollbar = styled(animated.div)`
  scrollbar-width: none; /* Firefox */
  -ms-overflow-style: none; /* IE 10+ */

  &::-webkit-scrollbar {
    width: 0px;
    background: transparent; /* make scrollbar transparent */
  }
`
// /

export default Pane
