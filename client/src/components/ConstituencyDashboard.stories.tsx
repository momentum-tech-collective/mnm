/** @jsx jsx */

import * as React from 'react'
import { jsx } from 'theme-ui'
import { storiesOf } from '@storybook/react'
import {
  someConstituency,
  someConstituencyRecommendation,
  someEvent
} from 'helpers/fakes'
import { times } from 'lodash'
import ConstituencyDashboardView from './ConstituencyDashboard'

storiesOf('components/ConstituencyDashboard', module)
  .add('loading', () => <ConstituencyDashboardView loading />)
  .add('default', () => (
    <ConstituencyDashboardView
      constituency={{
        ...someConstituency(),
        upcomingEvents: times(12, someEvent)
      }}
    />
  ))
  .add('with asks', () => (
    <ConstituencyDashboardView
      travelToEvents={times(4, someEvent)}
      postcode="AB13XY"
      travelToWanted={[]}
      constituency={{
        ...someConstituency(),
        upcomingEvents: times(12, someEvent)
      }}
    />
  ))
