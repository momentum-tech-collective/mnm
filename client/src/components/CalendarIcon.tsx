/** @jsx jsx */

import * as React from 'react'
import { jsx } from 'theme-ui'
import { DateTimeValue, DateTimeFormat } from './DateField'

const CalendarIcon: React.FC<React.SVGAttributes<{}> & { date: Date }> = ({
  date,
  ...props
}) => (
  <svg sx={{ overflow: 'visible' }} viewBox="0 0 52 49" fill="none" {...props}>
    <rect
      x={0.603}
      y={0.603}
      width={50.794}
      height={47.794}
      rx={6.748}
      fill="#FF4848"
      stroke="#000"
      strokeWidth={0.794}
    />
    <rect x={1} y={1} width={50} height={27} rx={6.512} fill="#fff" />
    <text
      x={26}
      y={23}
      sx={{ fill: '#000', textAnchor: 'middle', fontWeight: 600, fontSize: 5 }}
    >
      <DateTimeValue value={date} format={DateTimeFormat.DAY} />
    </text>
    <text
      x={26}
      y={43}
      sx={{ fill: '#FFF', textAnchor: 'middle', fontWeight: 500, fontSize: 3 }}
    >
      <DateTimeValue value={date} format={DateTimeFormat.MONTH_SHORT} />
    </text>
  </svg>
)

export default CalendarIcon
