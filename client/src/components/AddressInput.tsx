import * as React from 'react'
import { debounce, get } from 'lodash'
import { Autocomplete, geoAutocomplete } from 'data/geo'
import { AutocompleteField } from './AutocompleteField'

type Address = { address: string }

const AddressInput: ViewElement<
  { value?: Address; onChange: (address: Address) => void },
  GetComponentProps<typeof AutocompleteField>
> = ({ onChange, value, ...props }) => {
  const [addressOptions, setAddressOptions] = React.useState<Autocomplete[]>([])
  const [searching, setSearching] = React.useState(false)

  const updateAutocomplete = debounce(async (str: string) => {
    setSearching(true)
    const features = await geoAutocomplete(str)
    setAddressOptions(features)
    setSearching(false)
  }, 650)

  return (
    <AutocompleteField
      {...props}
      value={(value && value.address) || ''}
      onChange={address => onChange({ address })}
      suggestions={addressOptions.map(f => ({
        label: f.address,
        value: f.address
      }))}
      onSearchChange={query => {
        query && query.length > 5 && updateAutocomplete(query)
      }}
      loading={searching}
    />
  )
}

export default React.memo(AddressInput)
