/**
 * App-level abstractions
 */

type AnalyticsContext = { [property: string]: string }

interface Analytics {
  initialiseCookies: () => void
  setUserId: (id: string) => void
  logView: (path: string, context?: AnalyticsContext) => void
  logEvent: (
    event: string,
    context?: { category?: string; value?: string; metric?: number }
  ) => void
  setContext: (context: AnalyticsContext) => void
  logError: (error: Error) => void
}
