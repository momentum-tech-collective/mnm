import * as React from 'react'

declare global {
  type ComponentWithFragment<Frag = any, OtherProps = {}> = React.FC<
    Frag & OtherProps & { className?: string }
  > & {
    fragment?: any
    fragments?: any
  }

  type ViewElement<Props = {}, Attrs = React.HTMLAttributes<{}>> = React.FC<
    Omit<Attrs, keyof Props> & Props
  >

  type ViewElementRenderer<
    Props = {},
    Children = {},
    Attrs = React.HTMLAttributes<{}>
  > = <T extends Children>(
    props: Omit<Attrs, keyof Props> &
      Props & { values: T[]; children?: (value: T) => React.ReactNode }
  ) => React.ReactElement
}
