interface Coordinates {
  // @ts-ignore
  latitude: number
  // @ts-ignore
  longitude: number
}

interface Event {
  id: string
  properties: {
    title: string
    description: string
    startTime: number
    endTime: number
    constituency: {
      name: string
    }
  }
}

type GeoJSONFeature = {
  geometry: { coordinates: [number, number] }
}

// // Mapbox

namespace Mapbox {
  export interface AutoCompleteResults {
    type: 'FeatureCollection'
    features: AutoCompleteFeature[]
  }

  export interface AutoCompleteFeature {
    id: string
    type: string
    place_type: string[]
    relevance: number
    properties: Properties
    text: string
    place_name: string
    center: number[]
    geometry: Geometry
    address?: string
    context: Context[]
    matching_text?: string
    matching_place_name?: string
    bbox?: number[]
  }

  interface Context {
    id: string
    text: string
    wikidata?: null | string
    short_code?: ShortCode
  }

  enum ShortCode {
    GB = 'gb',
    GBEng = 'GB-ENG'
  }

  interface Geometry {
    type: string
    coordinates: number[]
  }

  interface Properties {
    accuracy?: string
    wikidata?: string
  }
}

// / Google

namespace Google {
  export interface AutoCompleteResults {
    predictions: Prediction[]
    status: string
  }

  export interface Prediction {
    description: string
    id: string
    matched_substrings: MatchedSubstring[]
    place_id: string
    reference: string
    structured_formatting: StructuredFormatting
    terms: Term[]
    types: Type[]
  }

  export interface MatchedSubstring {
    length: number
    offset: number
  }

  export interface StructuredFormatting {
    main_text: string
    main_text_matched_substrings: MatchedSubstring[]
    secondary_text: string
  }

  export interface Term {
    offset: number
    value: string
  }

  export enum Type {
    Geocode = 'geocode',
    Route = 'route',
    StreetAddress = 'street_address'
  }

  //

  export interface GeocodeResults {
    results: Result[]
    status: string
  }

  export interface Result {
    address_components: AddressComponent[]
    formatted_address: string
    geometry: Geometry
    place_id: string
    plus_code: PlusCode
    types: string[]
  }

  export interface AddressComponent {
    long_name: string
    short_name: string
    types: string[]
  }

  export interface Geometry {
    location: Location
    location_type: string
    viewport: Viewport
  }

  export interface Location {
    lat: number
    lng: number
  }

  export interface Viewport {
    northeast: Location
    southwest: Location
  }

  export interface PlusCode {
    compound_code: string
    global_code: string
  }
}
