import React from 'react'
import { storiesOf } from '@storybook/react'

import { action } from '@storybook/addon-actions'
import AuthModal from './AuthModal'
import {
  PasswordResetForm,
  PasswordResetFormContents
} from './PasswordResetForm'

const authModalBaseProps = {
  open: true,
  onAuthentication: action('authenticated'),
  setMode: action('setMode')
}

storiesOf('auth/SignIn', module).add('default', () => (
  <AuthModal {...authModalBaseProps} mode="signin" />
))

storiesOf('auth/Register', module).add('default', () => (
  <AuthModal {...authModalBaseProps} mode="register" />
))

storiesOf('auth/PasswordReset', module)
  .add('default', () => <AuthModal {...authModalBaseProps} mode="reset" />)
  .add('completed', () => (
    <AuthModal {...authModalBaseProps} mode="reset">
      <PasswordResetFormContents completed />
    </AuthModal>
  ))
