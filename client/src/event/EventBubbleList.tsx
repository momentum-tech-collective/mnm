/** @jsx jsx */
import { jsx } from 'theme-ui'
import * as React from 'react'
import { take } from 'lodash'
import { groupByDay, DateTimeFormat } from '../helpers/date'
import { DateTimeValue } from '../components/DateField'
import EventBubble from './EventBubble'

const EventList: React.FC<{
  events: Array<{
    id: string
    properties: {
      startTime: string
    }
  }>
}> = ({ events, ...props }) => (
  <div sx={{ variant: 'grid' }} {...props}>
    {take(
      groupByDay(events, 'Europe/London', x => x.properties!.startTime),
      4
    ).map(({ day, values }) => (
      <div key={day} sx={{ mr: 4, mt: 2 }} {...props}>
        <h4 sx={{ variant: 'subheading', color: 'primary', mb: 1 }}>
          <DateTimeValue
            relative
            value={day}
            format={DateTimeFormat.WEEKDAY_S}
          />

          <span sx={{ ml: 2, fontWeight: 300 }}>
            <DateTimeValue value={day} format={DateTimeFormat.DATE_S} />
          </span>
        </h4>

        <div sx={{ mb: -1 }}>
          {values.map(e => (
            <EventBubble
              key={e.id}
              sx={{ mb: 1 }}
              id={e.id}
              startTime={e.properties!.startTime}
            />
          ))}
        </div>
      </div>
    ))}
  </div>
)

export default EventList
