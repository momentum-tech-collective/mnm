/** @jsx jsx */
import { jsx } from 'theme-ui'
import * as React from 'react'
import { Formik, Field, FieldProps } from 'formik'
import * as yup from 'yup'
import { DateCalendar } from 'components/DateField'
import { FormWrapper, ProgressButton } from 'components/Elements'
import { useAuthenticatedCallback } from 'data/hooks'
import {
  Input,
  LargeRadioOption,
  RadioOptionGroup,
  FormSection,
  FormError,
  FORM_GENERAL_ERROR
} from '../components/Form'
import { CatOrSubcat } from '../views/queries'

export interface CreateEventFormFields {
  name: string
  description: string
  startTime: string | null
  constituency?: string
  postcode: string
  address: string
  targetEventId?: string
  subcategory: CatOrSubcat
  whatsappLink?: string
  contactNumber: string
  contactEmail: string
  isContactable: boolean
}

const SEEMS_SHORT = 'Seems a bit short...'
const TOO_LONG = 'Try not to blow up the website; shorten the text.'

export const EventForm: React.FC<{
  initialValues: CreateEventFormFields
  subcategories: CatOrSubcat[]
  onSubmit: (field: CreateEventFormFields) => void
  fixedType?: boolean
}> = ({ initialValues, subcategories, onSubmit, fixedType }) => {
  const [authModal] = useAuthenticatedCallback()

  const validationSchema = React.useMemo(
    () =>
      yup.object<CreateEventFormFields>().shape({
        name: yup
          .string()
          .label('Event title')
          .required()
          .min(6, SEEMS_SHORT)
          .max(80, 'Shorten that title!'),
        address: yup
          .string()
          .required()
          .label('Street address'),
        description: yup
          .string()
          .label('Description')
          .required(
            "Let activists know what you'll do and what will happen after."
          )
          .max(10000, TOO_LONG),
        whatsappLink: yup
          .string()
          .label('WhatsApp Group')
          .url('Paste the full WhatsApp join link here'),
        subcategory: yup
          .string()
          .label('Category')
          .required(),
        startTime: yup
          .string()
          .label('Start time and date')
          .required('Tell us when it’s happening!'),
        contactEmail: yup
          .string()
          .email()
          .label('Contact email')
          .required('Let us know how we can email to confirm any details'),
        contactNumber: yup
          .string()
          .label('Contact number')
          .required('Let us know how we can call to confirm any details')
      }),
    []
  )

  return (
    <React.Fragment>
      {authModal}

      <Formik<CreateEventFormFields>
        initialValues={{
          ...initialValues,
          subcategory: subcategories[0]
        }}
        validationSchema={validationSchema}
        onSubmit={async (variables, { setSubmitting, setFieldError }) => {
          setSubmitting(true)
          try {
            await onSubmit(variables)
            setSubmitting(false)
          } catch (e) {
            setFieldError(FORM_GENERAL_ERROR, e.toString())
          } finally {
            setSubmitting(false)
          }
        }}
      >
        {({ values, handleSubmit, setFieldValue, isSubmitting, isValid }) => {
          return (
            <FormWrapper onSubmit={handleSubmit}>
              <FormSection label="Describe the day">
                <Input
                  name="name"
                  fullWidth
                  label="Event Name"
                  helperText="What's happening?"
                  autoComplete="off"
                />

                {!fixedType && (
                  <React.Fragment>
                    <RadioOptionGroup label="What will people do together?">
                      {subcategories
                        .filter(catOrSubcat => catOrSubcat.allowUserCreate)
                        .map(catOrSubcat => (
                          <LargeRadioOption
                            key={catOrSubcat.id}
                            name="category"
                            label={`${catOrSubcat.emoji} ${catOrSubcat.name}`}
                            value={name}
                            description={catOrSubcat.description}
                            selected={
                              values.subcategory &&
                              catOrSubcat.id === values.subcategory.id
                            }
                            onClick={() => {
                              setFieldValue('subcategory', catOrSubcat)
                            }}
                          />
                        ))}
                    </RadioOptionGroup>
                  </React.Fragment>
                )}

                <Input
                  name="description"
                  label="Description"
                  helperText="Tell activists about the event and what people will be doing"
                  multiline
                  required
                  fullWidth
                  rows="8"
                />
              </FormSection>

              <FormSection label="Practical bits">
                <Input
                  name="address"
                  fullWidth
                  label="Street address"
                  helperText="Where are you meeting?"
                  required
                />
                <Input
                  name="postcode"
                  label="Postcode"
                  required
                  helperText="Used to show the point on the map where people are meeting"
                  fullWidth
                />
                <Field
                  name="startTime"
                  fullWidth
                  render={({ field }: FieldProps) => (
                    <DateCalendar
                      {...field}
                      label="Start Time"
                      helperText="When is it happening?"
                      required
                      fullWidth
                      onChange={value => setFieldValue('startTime', value)}
                    />
                  )}
                />

                <Input
                  name="whatsappLink"
                  label="WhatsApp Group"
                  helperText="Post the join link to a relevant WhatsApp group in here so people can get to know each other!"
                  fullWidth
                />
                <Input
                  required
                  name="contactEmail"
                  label="Email"
                  helperText="Provide your email so that we can contact you with any questions we have about the event"
                  fullWidth
                />
                <Input
                  required
                  name="contactNumber"
                  label="Phone Number"
                  helperText="Provide your number so that we can contact you with any questions we have about the event"
                  fullWidth
                />
              </FormSection>

              <FormSection>
                <FormError sx={{ mb: 2 }} />

                <ProgressButton
                  variant="contained"
                  type="submit"
                  size="large"
                  color="primary"
                  loading={isSubmitting}
                  disabled={!isValid}
                >
                  Create Event
                </ProgressButton>
              </FormSection>
            </FormWrapper>
          )
        }}
      </Formik>
    </React.Fragment>
  )
}
