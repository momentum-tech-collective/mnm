import React from 'react'
import { storiesOf } from '@storybook/react'
import { action } from '@storybook/addon-actions'
import { someEvent, someUser } from 'helpers/fakes'
import EventDetailView from './EventDetail'

const baseProps = {
  onRsvp: action('rsvp'),
  ...someEvent()
}

storiesOf('components/EventCard', module)
  .add('default', () => <EventDetailView {...baseProps} />)
  .add('loading', () => (
    <EventDetailView {...baseProps} loading properties={null} />
  ))
  .add('attending', () => (
    <EventDetailView
      {...baseProps}
      properties={{
        ...baseProps.properties,
        amAttending: true
      }}
    />
  ))
  .add('managing (pending)', () => (
    <EventDetailView
      {...baseProps}
      currentUser={{
        ...someUser(),
        id: 'me'
      }}
      properties={{
        ...baseProps.properties,
        published: false,
        manager: {
          id: 'me'
        }
      }}
    />
  ))
  .add('managing (published)', () => (
    <EventDetailView
      {...baseProps}
      currentUser={{
        ...someUser(),
        id: 'me'
      }}
      properties={{
        ...baseProps.properties,
        published: true,
        manager: {
          id: 'me'
        }
      }}
    />
  ))
  .add('managing (editing)', () => (
    <EventDetailView
      {...baseProps}
      editing
      currentUser={{
        ...someUser(),
        id: 'me'
      }}
      properties={{
        ...baseProps.properties,
        published: true,
        manager: {
          id: 'me'
        }
      }}
    />
  ))
  .add('whatsapp group', () => (
    <EventDetailView
      {...baseProps}
      currentUser={{
        ...someUser(),
        id: 'me'
      }}
      properties={{
        ...baseProps.properties,
        published: true,
        whatsappLink: 'http://whatsapp',
        manager: {
          id: 'me'
        }
      }}
    />
  ))
