import React from 'react'
import { storiesOf } from '@storybook/react'
import { someEvent } from 'helpers/fakes'
import EventCard from './EventCard'

storiesOf('EventCard', module).add('default', () => (
  <EventCard {...someEvent()} />
))
