/** @jsx jsx */
import { jsx } from 'theme-ui'
import { Event } from 'routes'
import { PushLink } from '../components/Navigation'
import { Chip } from '../components/Elements'
import { DateTimeFormat } from '../helpers/date'
import { DateTimeValue } from '../components/DateField'

const EventBubble: React.FC<{
  id: string
  startTime: string
  before?: any
  after?: any
}> = ({ id, startTime, before, after, ...props }) => (
  <PushLink
    {...props}
    route={Event}
    to={`/event/${id}`}
    sx={{
      display: 'inline-block',
      mr: 2,
      textDecoration: 'unset',
      textAlign: 'center',
      color: 'unset'
    }}
  >
    {before}
    <Chip
      key={id}
      sx={{
        variant: 'clickableChip',
        py: 1,
        px: 2,
        fontSize: 3
      }}
    >
      <DateTimeValue
        sx={{ textTransform: 'lowercase' }}
        format={DateTimeFormat.TIME}
        value={startTime}
      />
    </Chip>
    {after}
  </PushLink>
)

export default EventBubble
