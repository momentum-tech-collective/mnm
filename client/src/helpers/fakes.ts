import { times, take } from 'lodash'
import { Field } from 'react-jeff'
import { addHours, startOfDay } from 'date-fns'
import { EventCategory } from 'data/event'

// type inference hint
const literal = <T extends string>(x: T): T => x

export const someField = <T>(value: T) =>
  ({
    value,
    props: {
      value,
      onChange: console.log
    }
  } as Field<T, any>)

export const someUser = (i = 0) => ({
  __typename: literal('ConstituencyType'),
  id: String(i),
  firstName: 'Jeremy',
  lastName: 'Corbyn',
  email: 'theabsolute@parliament.uk',
  commitments: [],
  managedEvents: []
})

export const someConstituency = (i = 0) => ({
  __typename: literal('ConstituencyType'),
  id: String(i),
  name: 'Crawley',
  upcomingEvents: [],
  lastElection: someElection(),
  electionSet: times(1, someElection),
  volunteerNeedBand: i % 6,
  groups: []
})

export const someConstituencyRecommendation = (i = 0) => ({
  __typename: literal('ConstituencyRecommendationType'),
  id: String(i),
  constituency: someConstituency(i),
  events: times(4, someEvent),
  travelToEvents: times(4, someEvent),
  travelToWantedEvents: times(4, someEvent)
})

export const someEvent = (i = 0) => ({
  __typename: literal('EventType'),
  id: String(i),
  properties: {
    __typename: literal('EventProperties'),
    name: 'National Campaign Day 2019',
    startTime: someDateTime(i),
    address: 'Market Square, Crewkerne, UK',
    postcode: 'TA187LE',
    description:
      'Join us for a morning of campaigning against the Tories and their disastrous government. \r\n\r\nWe will be handing out exclusive leaflets, speaking to the public and setting out our key policies going into the next Labour manifesto for National Campaign Day 2019.\r\n\r\nNo need to bring anything, just yourself (and maybe an umbrella!!)',
    subcategory: null,
    category: EventCategory.CAMPAIGNING,
    labourCategory: 'Campaigning',
    constituency: someConstituency(),
    attendance: 5,
    straightLineDistance: (12 * i + 1) % 11,
    amAttending: false
  }
})

export const someElection = (i = 0) => ({
  __typename: literal('ElectionType'),
  id: String(i),
  turnout: 50273,
  labourMarginality: -2457,
  candidateSet: [
    {
      __typename: literal('CandidateType'),
      party: 'Conservative',
      votes: 25426
    },
    {
      __typename: literal('CandidateType'),
      party: 'Labour',
      votes: 22969
    },
    {
      __typename: literal('CandidateType'),
      party: 'Liberal Democrat',
      votes: 1878
    }
  ]
})

const baseDate = new Date('2020-01-01T09:00:00Z')
export const someDateTime = (i = 0) =>
  addHours(startOfDay(baseDate), i * 8).toISOString()

const someValue = <T>(i = 0, opts: T[]) => opts[(i % opts.length) - 1]
const loremText = `
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Nunc sed blandit libero volutpat sed. Neque convallis a cras semper auctor neque vitae tempus. Quis auctor elit sed vulputate mi sit amet mauris. Est ullamcorper eget nulla facilisi. Tristique senectus et netus et malesuada fames ac turpis. Accumsan sit amet nulla facilisi morbi. Proin sagittis nisl rhoncus mattis rhoncus urna neque. Id diam maecenas ultricies mi eget. Lorem ipsum dolor sit amet consectetur adipiscing elit. Feugiat nisl pretium fusce id velit. Blandit massa enim nec dui nunc mattis enim. Vitae et leo duis ut diam quam. Quis ipsum suspendisse ultrices gravida dictum. Ultrices in iaculis nunc sed augue lacus. Sit amet consectetur adipiscing elit pellentesque habitant. Facilisis volutpat est velit egestas dui id ornare. Ut tristique et egestas quis ipsum.
Metus dictum at tempor commodo ullamcorper a lacus vestibulum. Viverra ipsum nunc aliquet bibendum enim. Orci ac auctor augue mauris. Amet est placerat in egestas erat imperdiet sed euismod. Nisi quis eleifend quam adipiscing. Egestas sed sed risus pretium quam vulputate dignissim suspendisse in. Arcu non sodales neque sodales ut. In massa tempor nec feugiat nisl pretium fusce. Aliquet bibendum enim facilisis gravida neque. A diam sollicitudin tempor id eu. Non odio euismod lacinia at quis risus sed vulputate.
Eget arcu dictum varius duis at consectetur. Vivamus arcu felis bibendum ut tristique et. Nunc consequat interdum varius sit amet mattis. Egestas sed tempus urna et pharetra pharetra massa massa ultricies. Id volutpat lacus laoreet non curabitur gravida arcu ac tortor. Posuere urna nec tincidunt praesent semper feugiat nibh sed pulvinar. Nec ultrices dui sapien eget mi proin. Id ornare arcu odio ut sem nulla. Nisl nunc mi ipsum faucibus vitae aliquet nec. Neque vitae tempus quam pellentesque nec. At lectus urna duis convallis convallis. Viverra accumsan in nisl nisi scelerisque eu ultrices vitae. Lectus urna duis convallis convallis tellus id interdum. Ultrices eros in cursus turpis massa. Elit ullamcorper dignissim cras tincidunt lobortis feugiat. Nunc sed blandit libero volutpat. Leo a diam sollicitudin tempor. Integer malesuada nunc vel risus commodo viverra maecenas accumsan lacus. Pellentesque elit eget gravida cum sociis natoque penatibus et.
In nibh mauris cursus mattis molestie a iaculis at. Cum sociis natoque penatibus et magnis dis parturient montes nascetur. Scelerisque mauris pellentesque pulvinar pellentesque habitant morbi. Arcu dictum varius duis at consectetur. Ut tristique et egestas quis ipsum suspendisse ultrices gravida dictum. Purus ut faucibus pulvinar elementum integer. Mauris commodo quis imperdiet massa tincidunt nunc pulvinar sapien et. Cras adipiscing enim eu turpis egestas pretium aenean pharetra. Facilisis gravida neque convallis a cras semper auctor neque. Morbi tempus iaculis urna id volutpat lacus laoreet non. Tristique et egestas quis ipsum suspendisse ultrices. Donec massa sapien faucibus et molestie ac feugiat. Senectus et netus et malesuada fames ac turpis. Et malesuada fames ac turpis egestas integer eget.
Ut tellus elementum sagittis vitae. Bibendum arcu vitae elementum curabitur. Tellus in metus vulputate eu scelerisque felis. Et leo duis ut diam quam nulla. In fermentum et sollicitudin ac. Et malesuada fames ac turpis egestas integer eget. Elit at imperdiet dui accumsan sit amet nulla facilisi. Eu consequat ac felis donec et odio. Viverra adipiscing at in tellus. Arcu odio ut sem nulla pharetra diam. Diam vulputate ut pharetra sit amet aliquam id. Ut pharetra sit amet aliquam id diam. Cras adipiscing enim eu turpis egestas pretium. Faucibus ornare suspendisse sed nisi lacus. Nec ullamcorper sit amet risus nullam eget felis eget nunc. Quis auctor elit sed vulputate. Amet aliquam id diam maecenas ultricies mi.
Tortor id aliquet lectus proin nibh. Massa eget egestas purus viverra accumsan in nisl nisi scelerisque. Congue eu consequat ac felis donec et odio pellentesque diam. Nisi est sit amet facilisis magna etiam tempor orci. Diam in arcu cursus euismod. Mattis pellentesque id nibh tortor id aliquet lectus. Aliquet nec ullamcorper sit amet risus. Mattis rhoncus urna neque viverra. Pretium aenean pharetra magna ac placerat. Vel quam elementum pulvinar etiam non quam. Eu feugiat pretium nibh ipsum consequat nisl vel pretium. Non odio euismod lacinia at quis risus sed vulputate.
Enim blandit volutpat maecenas volutpat. Eget velit aliquet sagittis id consectetur purus ut faucibus pulvinar. Porttitor eget dolor morbi non arcu risus quis varius quam. Metus dictum at tempor commodo ullamcorper. Habitant morbi tristique senectus et netus et malesuada. Elementum pulvinar etiam non quam. Felis imperdiet proin fermentum leo. Ultrices neque ornare aenean euismod elementum nisi. Tristique sollicitudin nibh sit amet. Dolor sit amet consectetur adipiscing elit. Maecenas accumsan lacus vel facilisis volutpat. Elementum facilisis leo vel fringilla est ullamcorper eget nulla facilisi. Vitae tempus quam pellentesque nec. Quisque egestas diam in arcu cursus euismod. Tincidunt lobortis feugiat vivamus at augue. Arcu cursus euismod quis viverra nibh. Lectus quam id leo in vitae turpis. Nunc mattis enim ut tellus elementum sagittis vitae et. At lectus urna duis convallis convallis tellus.
Orci porta non pulvinar neque laoreet suspendisse. Tortor condimentum lacinia quis vel eros donec. Semper feugiat nibh sed pulvinar proin gravida. Risus feugiat in ante metus dictum. Mattis pellentesque id nibh tortor id aliquet lectus proin nibh. Morbi tincidunt augue interdum velit euismod. Quam adipiscing vitae proin sagittis nisl rhoncus mattis. Et sollicitudin ac orci phasellus egestas tellus rutrum tellus pellentesque. Vitae tempus quam pellentesque nec. Amet risus nullam eget felis eget nunc lobortis mattis. Consectetur libero id faucibus nisl tincidunt eget nullam non. Molestie ac feugiat sed lectus vestibulum. Praesent semper feugiat nibh sed pulvinar proin gravida hendrerit. Lacus suspendisse faucibus interdum posuere. Sem integer vitae justo eget magna. Sed risus pretium quam vulputate dignissim suspendisse in. Vitae auctor eu augue ut lectus arcu bibendum. Amet purus gravida quis blandit. Facilisis leo vel fringilla est ullamcorper eget nulla facilisi.
Ornare arcu odio ut sem nulla pharetra diam sit amet. Tincidunt nunc pulvinar sapien et ligula ullamcorper malesuada proin. Pellentesque diam volutpat commodo sed egestas. Semper viverra nam libero justo laoreet sit amet. Vel pharetra vel turpis nunc eget lorem dolor. Viverra aliquet eget sit amet tellus cras adipiscing. Tincidunt vitae semper quis lectus nulla at. Vehicula ipsum a arcu cursus vitae congue mauris rhoncus aenean. Aliquet porttitor lacus luctus accumsan tortor. Ultricies mi eget mauris pharetra. Donec pretium vulputate sapien nec. Vulputate ut pharetra sit amet aliquam. Vel eros donec ac odio tempor. Est ultricies integer quis auctor elit sed vulputate. Nullam eget felis eget nunc lobortis mattis aliquam. Sem fringilla ut morbi tincidunt augue interdum velit. Sapien nec sagittis aliquam malesuada bibendum arcu vitae elementum.
Magna etiam tempor orci eu. Mauris nunc congue nisi vitae suscipit. Velit scelerisque in dictum non consectetur. Libero justo laoreet sit amet cursus sit. Sed id semper risus in hendrerit. Eu non diam phasellus vestibulum lorem sed. Et netus et malesuada fames ac turpis egestas sed. Sed faucibus turpis in eu mi. Tristique nulla aliquet enim tortor at auctor urna nunc id. Volutpat blandit aliquam etiam erat velit scelerisque. Nisi vitae suscipit tellus mauris. In fermentum et sollicitudin ac orci phasellus. Suscipit tellus mauris a diam. Ullamcorper velit sed ullamcorper morbi tincidunt ornare. Ornare arcu odio ut sem nulla pharetra. Tortor at risus viverra adipiscing at in. Consectetur adipiscing elit pellentesque habitant morbi tristique senectus. Ac placerat vestibulum lectus mauris ultrices.
`
  .trim()
  .split('\n')

export const paragraphs = (count = 2) => take(loremText, count).join('\n')
export const sentences = (count = 2) =>
  take(loremText[0].split('.'), count).join('.')
