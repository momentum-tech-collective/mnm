import gql from 'graphql-tag'
import { client } from '../data/graphql'
import { SetIsContactable, SetIsContactableVariables } from './__graphql__/'

const MUTATION_CONTACTABLE = gql`
  mutation SetIsContactable($isContactable: Boolean!) {
    updateUser(isContactable: $isContactable) {
      currentUser {
        id
        isContactable
      }
    }
  }
`

export const updateContactable = () => (
  event: React.ChangeEvent<HTMLInputElement>,
  checked: boolean
) => {
  client.mutate<SetIsContactable, SetIsContactableVariables>({
    mutation: MUTATION_CONTACTABLE,
    variables: { isContactable: checked ? true : false }
  })
}
