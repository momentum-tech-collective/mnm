import { ensureArray } from 'helpers/array'

type Event = React.KeyboardEvent<HTMLInputElement>
type KeyCode = React.KeyboardEvent<HTMLInputElement>['keyCode']

export const onKey = (keys: KeyCode | KeyCode[], cb: (e: Event) => void) => (
  e: Event
) => {
  if (ensureArray(keys).some(key => key === e.keyCode)) cb(e)
}

export const preventDefault = (e: React.SyntheticEvent) => e.preventDefault()
