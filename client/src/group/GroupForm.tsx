/** @jsx jsx */
import { jsx } from 'theme-ui'
import * as React from 'react'
import { Formik, Field, FieldProps } from 'formik'
import * as yup from 'yup'
import AddressInput from 'components/AddressInput'
import ConstituencyInput from 'components/ConstituencyInput'
import { categories, campaigningCategories } from 'data/Group'
import { DateCalendar } from 'components/DateField'
import { FormWrapper, ProgressButton } from 'components/Elements'
import { useAuthenticatedCallback } from 'data/hooks'
import gql from 'graphql-tag'
import {
  Input,
  LargeRadioOption,
  RadioOptionGroup,
  FormSection,
  FormError,
  FORM_GENERAL_ERROR
} from '../components/Form'
import AutocompleteFromQuery from 'components/AutocompleteFromQuery'
import GroupTagInput from './GroupTagInput'
import PlatformInput from './PlatformInput'

export interface CreateGroupFormFields {
  name: string
  description?: string
  joinLink: string
  platform: string
  groupCount?: number
  constituencies: string[]
  tags: string[]
}

const SEEMS_SHORT = 'Seems a bit short...'
const TOO_LONG = 'Try not to blow up the website; shorten the text.'

const validationSchema = yup.object<CreateGroupFormFields>().shape({
  name: yup
    .string()
    .label('Group title')
    .required()
    .min(6, SEEMS_SHORT)
    .max(80, TOO_LONG),
  groupCount: yup.number(),
  joinLink: yup
    .string()
    .required()
    .url('Paste the full link here'),
  platform: yup.string().required(),
  description: yup
    .string()
    .label('Description')
    .required("Let activists know what this group's about.")
    .min(20, SEEMS_SHORT)
    .max(10000, TOO_LONG),
  constituencies: yup.array().of(yup.string()),
  tags: yup.array().of(yup.string())
})

export const GroupForm: React.FC<{
  initialValues: CreateGroupFormFields
  onSubmit: (field: CreateGroupFormFields) => void
}> = ({ initialValues, onSubmit }) => {
  const [callbackAfterAuthentication, authModal] = useAuthenticatedCallback()

  return (
    <React.Fragment>
      {authModal}

      <Formik<CreateGroupFormFields>
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={async (variables, { setSubmitting, setFieldError }) => {
          setSubmitting(true)

          await callbackAfterAuthentication({
            mode: 'register',
            onAuthenticated: async () => {
              try {
                await onSubmit(variables)
                setSubmitting(false)
              } catch (e) {
                console.error(e)
                setFieldError(FORM_GENERAL_ERROR, e.toString())
              } finally {
                setSubmitting(false)
              }
            }
          })
        }}
      >
        {({
          values,
          errors,
          touched,
          handleSubmit,
          setFieldValue,
          isSubmitting,
          isValid
        }) => (
          <FormWrapper onSubmit={handleSubmit}>
            <FormSection label="Describe the day">
              <Input
                name="name"
                fullWidth
                label="Group Name"
                helperText="What is this communication group?"
                autoComplete="off"
              />

              <Input
                name="description"
                label="Description"
                helperText="Describe what this group is for, what kind of people are in it and what happens there."
                multiline
                required
                fullWidth
                rows="2"
              />

              <Input
                name="joinLink"
                label="Web link"
                helperText="Post the join link here so that people can get involved!"
                fullWidth
              />

              <Field
                name="constituency"
                render={({ field, form }: any) => (
                  <ConstituencyInput
                    {...field}
                    label="Constituency"
                    onChange={change => setFieldValue('constituency', change)}
                    fullWidth
                    required
                    helperText="What constituencies is this whatsapp group relevant to?"
                  />
                )}
              />

              <Field
                name="groupTags"
                render={({ field, form }: any) => (
                  <GroupTagInput
                    {...field}
                    label="Tags"
                    onChange={change => setFieldValue('groupTags', change)}
                    fullWidth
                    required
                    // helperText="What constituencies is this whatsapp group relevant to?"
                  />
                )}
              />

              <Field
                name="platform"
                render={({ field, form }: any) => (
                  <PlatformInput
                    {...field}
                    label="Platform"
                    onChange={change => setFieldValue('platform', change)}
                    fullWidth
                    required
                    helperText="What social media platform is this chat running on?"
                  />
                )}
              />

              <Input
                name="groupCount"
                label="Group count"
                type="number"
                helperText="If you know it, roughly how many people are in this group?"
                fullWidth
              />
            </FormSection>

            <FormSection>
              <FormError sx={{ mb: 2 }} />

              <ProgressButton
                variant="contained"
                type="submit"
                size="large"
                color="primary"
                loading={isSubmitting}
                disabled={!isValid}
              >
                Add Group
              </ProgressButton>
            </FormSection>
          </FormWrapper>
        )}
      </Formik>
    </React.Fragment>
  )
}
