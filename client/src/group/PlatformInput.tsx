import * as React from 'react'
import gql from 'graphql-tag'
import { AutocompleteField } from './AutocompleteField'
import AutocompleteFromQuery from 'components/AutocompleteFromQuery'

const QUERY = gql`
  query GroupPlatformList {
    communicationPlatforms {
      id
      icon
      label: name
      value: id
      description
    }
  }
`

const PlatformInput: React.FC<
  Omit<GetComponentProps<typeof AutocompleteField>, 'suggestions'>
> = props => (
  <AutocompleteFromQuery
    query={QUERY}
    getList={d => d.communicationPlatforms}
    {...props}
  />
)

export default React.memo(PlatformInput)
