import * as React from 'react'
import gql from 'graphql-tag'
import { AutocompleteField } from './AutocompleteField'
import AutocompleteFromQuery from 'components/AutocompleteFromQuery'

const QUERY = gql`
  query GroupTagList {
    groupTags {
      id
      icon
      label: name
      value: id
      description
    }
  }
`

const GroupTagInput: React.FC<
  Omit<GetComponentProps<typeof AutocompleteField>, 'suggestions'>
> = props => (
  <AutocompleteFromQuery
    query={QUERY}
    getList={d => d.groupTags}
    multiple
    {...props}
  />
)

export default React.memo(GroupTagInput)
