export const LOCALSTORAGE_NAMESPACE = 'mycampaignmap-'
export const JWT_STORAGE_KEY = `${LOCALSTORAGE_NAMESPACE}AUTH_TOKEN`
export const REACT_APP_ROOT = 'REACT-APP-ROOT'
export const PORTAL_ROOT = 'react-portal-root'
export const PLATFORM = process.env.CORDOVA_APP ? 'cordova' : 'web'
export const FEATURE_FLAGS = process.env.FEATURE_FLAGS
  ? JSON.parse(process.env.FEATURE_FLAGS)
  : {}
