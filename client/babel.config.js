module.exports = {
  presets: [
    '@babel/react',
    [
      '@babel/typescript',
      {
        isTSX: true,
        allExtensions: true
      }
    ]
  ],
  plugins: [
    'macros',
    '@babel/proposal-class-properties',
    '@babel/proposal-object-rest-spread',
    '@babel/syntax-dynamic-import'
  ],
  env: {
    development: {
      plugins: ['react-hot-loader/babel']
    },
    test: {
      plugins: ['@babel/transform-modules-commonjs']
    },
    production: {
      presets: [
        [
          '@babel/env',
          {
            modules: false,
            targets: '> 0.5%, not dead',
            useBuiltIns: 'usage',
            corejs: 3
          }
        ]
      ]
    }
  }
}
