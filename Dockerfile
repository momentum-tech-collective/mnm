FROM python:3.8.18
WORKDIR /usr/src/app
RUN apt-get update && apt-get upgrade -y && apt-get autoremove && apt-get autoclean
# required for geospatial database
RUN apt-get install -y binutils libproj-dev gdal-bin
# required for regular data ingests
RUN apt-get install -y cron
# Install Python and Package Libraries
COPY Pipfile Pipfile.lock /usr/src/app/
RUN pip install pipenv
RUN pipenv install
# Bring over the rest
COPY api /usr/src/app/api
COPY mynearestmarginal /usr/src/app/mynearestmarginal
COPY helpers /usr/src/app/helpers
COPY manage.py /usr/src/app/manage.py
# Sync with docker-compose
EXPOSE 8000
