# -*- coding: utf-8 -*-
from api.settings.base import *
import dj_database_url

print('''
#############################################################
##                                                         ##
##             Running Django in Staging 🔎               ##
##                                                         ##
#############################################################''')
# mycampaignmap frontend
DEBUG = False

# heroku buildpacks:set https://github.com/dschep/heroku-geo-buildpack.git
GDAL_LIBRARY_PATH = os.getenv('GDAL_LIBRARY_PATH')
GEOS_LIBRARY_PATH = os.getenv('GEOS_LIBRARY_PATH')

MIDDLEWARE = ['mynearestmarginal.middleware.query_count_debug_middleware'] + MIDDLEWARE

# Configure Django App for Heroku.
import django_heroku
django_heroku.settings(locals())

# Use Heroku's (or our) DATABASE_URL to configure connection
DATABASES = {}
DATABASES['default'] = database_url
DATABASES['default']['OPTIONS'] = {}

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
        },
    },
    'loggers': {
        'query_count': {
            'handlers': ['console'],
            'level': 'DEBUG'
        }
    }
}
