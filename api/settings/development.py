# -*- coding: utf-8 -*-
from .base import *
import os

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True
HTTPS = False

print('''
#############################################################
##                                                         ##
##             Running Django in DEVELOPMENT 👹            ##
##                                                         ##
#############################################################''')
# mycampaignmap frontend
INSTALLED_APPS += [
    'whitenoise.runserver_nostatic',
]

# Use Heroku's (or our) DATABASE_URL to configure connection
DATABASES = {}
DATABASES['default'] = database_url
DATABASES['default']['OPTIONS'] = {}

from ..loggers import SQLFormatter

MIDDLEWARE = ['mynearestmarginal.middleware.query_count_debug_middleware'] + MIDDLEWARE

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'sql': {
            '()': SQLFormatter,
            'format': '[%(duration).3f] %(statement)s',
        }
    },
    'handlers': {
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
        },
        'sql': {
            'class': 'logging.StreamHandler',
            'formatter': 'sql',
            'level': 'DEBUG',
        },
    },
    'loggers': {
        'query_count': {
            'handlers': ['console'],
            'level': 'DEBUG'
        },
        # 'django.db.backends': {
        #     'handlers': ['sql'],
        #     'level': 'DEBUG',
        #     'propagate': False,
        # },
        # 'django.db.backends.schema': {
        #     'handlers': ['console'],
        #     'level': 'DEBUG',
        #     'propagate': False,
        # },
    }
}

# Because handled
# ALLOWED_HOSTS = [ '*', ]
