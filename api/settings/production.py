# -*- coding: utf-8 -*-
from api.settings.base import *

print('''
#############################################################
##                                                         ##
##             Running Django in Production ☀️             ##
##                                                         ##
#############################################################''')
# mycampaignmap frontend
DEBUG = False

INSTALLED_APPS = ["scout_apm.django"] + INSTALLED_APPS
SCOUT_NAME = "MCM"

# heroku buildpacks:set https://github.com/dschep/heroku-geo-buildpack.git
GDAL_LIBRARY_PATH = os.getenv('GDAL_LIBRARY_PATH')
GEOS_LIBRARY_PATH = os.getenv('GEOS_LIBRARY_PATH')

GRAPHQL_SANDBOX=False
SECURE_SSL_REDIRECT = True
HTTPS=True

# Configure Django App for Heroku.
import django_heroku
django_heroku.settings(locals())

# Use Heroku's (or our) DATABASE_URL to configure connection
DATABASES = {}
DATABASES['default'] = database_url
DATABASES['default']['OPTIONS'] = {}
