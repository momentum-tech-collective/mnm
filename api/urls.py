from django.urls import path, include, re_path
from django.http import HttpResponse
from django.conf.urls import url
from django.contrib import admin
from graphene_django.views import GraphQLView
from django.views.decorators.csrf import csrf_exempt
from mynearestmarginal.auth import social
from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token, verify_jwt_token
from mynearestmarginal.engine import services
from mynearestmarginal import views
from api.views import client
from django.conf import settings
from django.views.decorators.cache import never_cache
import os

urlpatterns = [
    # API
    url(r'^graphql', csrf_exempt(GraphQLView.as_view(graphiql=getattr(settings, 'GRAPHQL_SANDBOX', False)))),
    url(r'^autocomplete/(.*)', services.autocomplete),
    # Admin
    url(r'^admin/', admin.site.urls),
    # GDPR
    url(r'^GDPR/delete_user/$', csrf_exempt(views.delete_user), name='delete_user'),    
    # Auth
    url(r'^accounts/', include('allauth.urls')),
    url(r'^api-auth/', include('rest_framework.urls')),
    url(r'^api-token-auth/', obtain_jwt_token),
    url(r'^api-token-refresh/', refresh_jwt_token),
    url(r'^api-token-verify/', verify_jwt_token),
    url(r'^rest-auth/', include('rest_auth.urls')),
    url(r'^rest-auth/registration/', include('rest_auth.registration.urls')),
    # url(r'^rest-auth/facebook/$', social.FacebookLogin.as_view(), name='fb_login'),
    # url(r'^rest-auth/twitter/$', social.TwitterLogin.as_view(), name='twitter_login'),
    # Serve the app on all remnant routes
    path('djrichtextfield/', include('djrichtextfield.urls'))
]

if not settings.DEBUG:
    urlpatterns += [
        re_path(r'^((?!static).)*$', never_cache(client.index))
    ]
