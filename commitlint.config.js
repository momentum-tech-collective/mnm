// eslint-disable-next-line no-undef
module.exports = {
  extends: ['@commitlint/config-conventional'],
  rules: {
    // See https://app.gitbook.com/@futr-ai/s/playbook/source-code/git-commit-messages for docs
    'subject-case': [2, 'always', ['sentence-case']],
    'type-enum': [
      2,
      'always',
      ['feat', 'fix', 'docs', 'style', 'refactor', 'test', 'chore']
    ]
  }
}
