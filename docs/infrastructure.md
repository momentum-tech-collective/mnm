# MCM Infrastructure

## Stack

A [python](https://www.python.org/) server running the [Django 2.2](https://docs.djangoproject.com/en/2.2/) framework provides a standard [GraphQL](https://graphql.org/) API. GraphQL is served with [Graphene](https://graphene-python.org/) and its adaptors to [Django](https://docs.graphene-python.org/projects/django) and [SQLAlchemy](https://docs.graphene-python.org/projects/sqlalchemy).

The server composes and interacts with people, event and interaction data stored in a [PostgreSQL](https://www.postgresql.org/) relational database, a [NationBuilder](https://nationbuilder.com/) nation's API, [Labour's events API](https://labourorganise.com/api/events/all), and [postcodes.io](https://postcodes.io) for postcode geocoding. We use the Google Maps and Mapbox for additonal geography features.

The frontend is written in [TypeScript](https://www.typescriptlang.org/) and consumes this GraphQL data via [Apollo GraphQL](https://www.apollographql.com/), which also smartly caches data locally to prevent over-fetching. The user interface is composed with [React](https://reactjs.org/).

The platform uses JWT for authentication. Unlike cookie-facilitated server sessions, JWT auth is consistently managed across devices and puts no burden on the server. [(Here's a useful diagram if you need clarity.)](https://miro.medium.com/max/3504/1*d6YcPvq7TeU0DTamj629xw.png)

## Third party dependency config

My Campaign Map makes use of third-party APIs predominantly for geography features.

- You'll need a Mapbox access token (which should be URL-restricted via) to render the maps

- Create a Google Cloud Platform project and provision a key with access to the [Places](https://console.cloud.google.com/google/maps-apis/apis/places-backend.googleapis.com/metrics?project=my-nearest-marginal-dev), [Geocoding](https://console.cloud.google.com/google/maps-apis/apis/geocoding-backend.googleapis.com/metrics?project=my-nearest-marginal-dev), and [Directions Matrix](https://console.cloud.google.com/google/maps-apis/apis/distance-matrix-backend.googleapis.com/metrics) APIs (links to quick-add).

- Finally, you will need a NationBuilder account if you want to synchronise with that system. This is not a crucial aspect of the system, and it will run without.

All of these need to be plugged into an `.env` file at the root of the repository directory, so that docker can ingest them.

### Geometries

Certain geometries need to be uploaded to Mapbox for data visualisation. You can download the shapefiles and upload them to Mapbox at the below addresses. Layers must be named exactly as specified so that they can be accessed by the codebase.

- Constituency boundaries (2018)
  [ id: pcon18cd ] and [ label: pcon18nm ]
  https://geoportal.statistics.gov.uk/datasets/westminster-parliamentary-constituencies-december-2018-gb-bgc/data

  Mapbox layer ID: `constituencies`

- Ward boundaries (2018)
  [ id: wd18cd ]
  https://geoportal.statistics.gov.uk/datasets/wards-december-2018-generalised-clipped-boundaries-gb/data

  Mapbox layer ID: `wards`

- Finally, set the environment variables for identifying the geometry IDs, as the ONS regularly updates geometries and assigns year-specific identifiers.

E.g.

```
CONSTITUENCY_ID_PROPERTY=pcon18cd
WARD_ID_PROPERTY=wd18cd
```

- Ward—Constituency ID lookup table (2018):
  [ WD18CD - PCON18CD ]
  https://geoportal.statistics.gov.uk/datasets/ward-to-westminster-parliamentary-constituency-to-local-authority-district-december-2018-lookup-in-the-united-kingdom/data

### Data sources

Notes on original sourcing of data, stored in [mynearestmarginal/data](./mynearestmarginal/data).

- Constituencies

* GE2017 election results, seat by seat party by party

  https://commonslibrary.parliament.uk/parliament-and-elections/elections-elections/constituency-data-election-results/

* By-elections since 2017, which update the party results:
  https://researchbriefings.parliament.uk/ResearchBriefing/Summary/CBP-8280

* Current MPs and official positions in govt / opposition (as they change so often)
  http://data.parliament.uk/membersdataplatform/services/mnis/members/query/
  House=Commons

  # Get current party

        %7C
        IsEligible=true

  # Get their titles

        /GovernmentPosts%7COppositionPosts/

## Staging & Production

Momentum runs this platform on [Dokku](https://dokku.com/), a self-hosted PAAS implementation similar to [Heroku](https://www.heroku.com/), with support for GitLab push deploys, staging and production builds. The production platform is hosted at [mycampaignmap.com](https://mycampaignmap.com).

Contact [digital@peoplesmomentum.com](mailto:digital@peoplesmomentum.com) for access to the server Dokku is hosted on.

To reproduce this setup with your own Dokku instance:

1. Clone this repo and link it to a Dokku app:

   1. Create an app on Dokku:

      ```
      $ ssh dokku dokku apps:create mcm-staging
      ```

   2. Clone the git repo to your local system.

   3. Add the Dokku app to your git remotes:

      ```
      $ git remote add staging dokku@dokku.peoplesmomentum.com:mcm-staging
      ```

2. Create a PostGIS-enabled PostgreSQL database and link it to the Dokku app:

  1. Install the [Dokku PostgreSQL plugin](https://github.com/dokku/dokku-postgres) if you haven't already:

     ```
     $ ssh dokku dokku plugin:install https://github.com/dokku/dokku-postgres.git postgres
     ```

  2. Create a PostGIS-enabled database (substitute the image version for the [currently recommended version](https://hub.docker.com/r/postgis/postgis)):

     ```
     $ ssh dokku dokku postgres:create mcm-staging --image "postgis/postgis" --image-version "14-3.3"
     ```

  3. Enable the `postgis` and `hstore` extensions:

     ```
     $ ssh dokku dokku postgres:connect mcm-staging

     => CREATE EXTENSION postgis;

     => CREATE EXTENSION hstore;
     ```

     You can verify by running...

     ```
     => SELECT postgis_full_version();
     ```

  4. Link the database to the app:

     ```
     $ ssh dokku dokku postgres:link mcm-staging mcm-staging
     ```

3. Set the remaining environment variables, using [.env.template](./.env.template) as a guide.

   - Set `ssh dokku dokku config:set mcm-staging DISABLE_COLLECTSTATIC=1` so that we can manually do this later, rather than have `heroku/python` jump the gun.

   - Set `ssh dokku dokku config:set mcm-staging GDAL_VERSION=2.4.0` to make the geo buildpack use an old version of GDAL - [newer versions cause Django to read co-ordinates backwards...](https://code.djangoproject.com/ticket/31611)

   - Set `ssh dokku dokku config:set mcm-staging DJANGO_SETTINGS_MODULE=api.settings.{production|staging}` depending on your requirements.

   - Check that `DATABASE_URL` is set (`ssh dokku config:get mcm-staging DATABASE_URL`). Dokku should have done this automatically when you linked the DB in step 2.

   - Set `ssh dokku dokku config:set mcm-staging DOKKU_PROXY_PORT_MAP=http:80:5000` to point incoming traffic to the frontend at port 5000. If you're using the letsencrypt plugin to manage SSL, make sure the port map is configured first.

   - Set `ssh dokku dokku config:set mcm-staging ADMIN_EMAIL={email} ADMIN_PASSWORD={password}` to configure the admin login. It won't compile without these!

   - Set `ssh dokku dokku config:set mcm-staging COVID_GROUPS_URL={url}` to tell MCM where to fetch COVID support group info from. It won't compile without this either!

   - Set `ssh dokku dokku config:set mcm-staging API_HOST={site url}` to tell the frontend where the backend is.

   - Add your Google Maps API key (requires access to the Geocoding, Distance Matrix and Autocomplete APIs):

     ```
     $ ssh dokku dokku config:set mcm-staging GOOGLE_MAPS_API_KEY={api key}
     ```

   - Add your Mapbox API access token, username and style ID:

     ```
     $ ssh dokku dokku config:set mcm-staging MAPBOX_ACCESS_TOKEN={api key} MAPBOX_USERNAME={username} MAPBOX_STYLE_ID={style id}
     ```

   - Ideally publicly accessible tokens, like Mapbox's, should be URL whitelisted to prevent abuse. Mapbox has settings for this.

   - (The thing will run without Nationbuilder deets tbh.)

5. `git push staging master`, sit back and watch #thepolitics unfold.

### Syncing external data

Heroku's Scheduler add-on runs any arbitrary `manage.py` command at regular intervals; typically
either every 10 minutes, every hour, or every day. 

`python manage.py ingest_events` pulls in Labour Party event data.
`python manage.py ingest_groups` pulls in Covid-19 Mutual Aid groups.

### Further reading

We broadly followed [this guide](https://librenepal.com/article/django-and-create-react-app-together-on-heroku/) to achieve the general Heroku setup. [This article](https://medium.com/the-geospatials/deploy-geodjango-application-to-heroku-in-2019-part-3-41ca4535f377) was a solid guide to provisioning PostGIS, the geospatial DB extension.

## Development

This git repo uses [Docker's](https://www.docker.com/) `docker-compose` featureset to spin up a self-contained development environment. This means all contributors share exactly the same environmental setup with each other.

Packages are managed by [yarn](https://yarnpkg.com) (for the typescript frontend) and [pipenv](https://docs.pipenv.org/en/latest/) (for the python backend), which both run within the docker environments.

When attached to the docker container, in order to access the Python virtual env, you should prefix all python commands in the with `yarn`; for example:

```bash
$ yarn python manage.py startapp mynearestmarginal
```

To run commands (like `yarn install` or `pipenv run ...`) from outside the containerised instances, use the handy `yarn shell` tool, pointing to `django` or `react`:

```
$ yarn python manage.py makemigrations

$ yarn shell react yarn add react-spring
```

To talk to the database using psql, use `yarn psql`.

## Cordova

This app also supports being built built as a cordova app for iOS and android.

### Install

```bash
yarn global add cordova
(cd client/cordova && npm i)
```

Follow the cordova platform setup instructions:

- [iOS](https://cordova.apache.org/docs/en/latest/guide/platforms/ios)
- [Android](https://cordova.apache.org/docs/en/latest/guide/platforms/android) (watch out for missing dependencies and make sure you follow the instructions about adding environmental variables to your path closely)

### Run

Do most of your development work in the web environment as cordova doesn’t support livereload natively. We _may_ be able to get it working with webpack dev server’s livereload if needed.

On emulator:

```bash
cd client
yarn cordova:build --watch
yarn cordova:emulator (ios|android)
```

On device:

```bash
cd client
yarn cordova:build --watch
yarn cordova:run (ios|android) --device
```

### Debugging

Both Safari and Chrome support remote-debugging cordova apps. Check the relevant cordova platform docs for more information.
