import pytz
import json
import logging
from django.utils import timezone
from django.db import connection
import scout_apm.api

# TODO: Handle this at django admin form level, rather than globally
def timezone_middleware(get_response):
    def process_request(request):
        timezone.activate(pytz.timezone('Europe/London'))
        return get_response(request)

    return process_request

def query_count_debug_middleware(get_response):
    logger = logging.getLogger('query_count')

    """
    This middleware will log the number of queries run
    and the total time taken for each request (with a
    status code of 200). It does not currently support
    multi-db setups.
    """
    def process_request(request):
        response = get_response(request)

        if request.method == 'POST' and request.path == '/graphql' and response.status_code == 200:
            query = json.loads(request.body.decode('utf8'))
            if 'operationName' in query:
                operation = query['operationName']
            else:
                operation = '<anon>'

            total_time = 0

            for query in connection.queries:
                query_time = query.get('time')
                if query_time is None:
                    # django-debug-toolbar monkeypatches the connection
                    # cursor wrapper and adds extra information in each
                    # item in connection.queries. The query time is stored
                    # under the key "duration" rather than "time" and is
                    # in milliseconds, not seconds.
                    query_time = query.get('duration', 0) / 1000
                total_time += float(query_time)

            logger.info('graphql %s: %s queries run, total %s seconds' % (operation, len(connection.queries), total_time))
        return response
    
    return process_request

def scout_graphql_middleware(get_response):
    logger = logging.getLogger('query_count')

    """
    This middleware will log the number of queries run
    and the total time taken for each request (with a
    status code of 200). It does not currently support
    multi-db setups.
    """
    def process_request(request):
        response = get_response(request)

        if request.method == 'POST' and request.path == '/graphql' and response.status_code == 200:
            query = json.loads(request.body.decode('utf8'))
            if 'operationName' in query:
                operation = query['operationName']
            else:
                operation = '<anon>'

            scout_apm.api.Context.add("operation", operation)

        return response
    
    return process_request
