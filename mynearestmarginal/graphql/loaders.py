from promise.dataloader import Promise, DataLoader
from ..engine.geo import get_relative_travel_info, postcode_geo, bulk_postcode_geo, bulk_coordinate_geo
from .geo import TravelInfoType, GeocodeResult
from .utils import create_graphql_instance
from helpers.geo import create_point
from helpers.cache import cached_fn
from mynearestmarginal.graphql.django import Election, Constituency, Candidate, Event
from dateutil import parser
from datetime import datetime

def TravelInfoLoaderFactory():
    '''
    Get travel information to this destination postcode/address, from an origin.

    TravelInfoLoaderFactory({...})
        .load({ origin, destination, mode })
    TravelInfoLoaderFactory({...})
        .load_many([{ origin, destination, mode }, { origin, destination, mode }])
    '''

    def get_cache_key(key):
        try:
            return f"{key.get('origin')}::{key.get('destination')}::{key.get('mode')}"
        except:
            return key

    def initialise_graphql_type(row):
        if row is None:
            return row
        return create_graphql_instance(
            TravelInfoType,
            **row
        )

    def batch_load_fields(keys):
        origins = list(set([k.get('origin') for k in keys]))
        outcomes = []

        for origin in origins:
            relevant_keys = [k for k in keys if k.get('origin') == origin]
            destinations = list(set([k.get('destination') for k in relevant_keys]))
            mode = list(set([k.get('mode') for k in relevant_keys]))
            mode = mode[0] if len(mode) > 0 else 'transit'

            outcomes += get_relative_travel_info(
                origin=origin,
                destinations=destinations,
                mode=mode
            )

        if len(outcomes) == 0:
            return Promise.resolve([None for key in keys])

        key_map = [
            next((
                initialise_graphql_type(outcome.get('payload'))
                for outcome in outcomes
                if outcome.get('mode') == key.get('mode')
                and outcome.get('origin') == key.get('origin')
                and outcome.get('destination') == key.get('destination')
            ), None)
            for key in keys
        ]

        return Promise.resolve(key_map)

    return DataLoader(batch_load_fields, get_cache_key=get_cache_key)

def GeoFromPostcodesLoaderFactory():
    '''
    Get geographic information for this postcode.

    GeoLoaderFactory({...})
        .load("LE115AG")
    GeoLoaderFactory({...})
        .load_many(["LE115AG", "WDD351"])
    '''

    def initialise_graphql_type(row):
        if row is None:
            return row
        return create_graphql_instance(
            GeocodeResult,
            **row
        )

    def batch_load_fields(postcodes):
        postcodes = [p.replace(" ", "") for p in postcodes]
        if len(postcodes) == 1:
            geo = postcode_geo(postcodes[0])
            return Promise.resolve([geo])

        geos = bulk_postcode_geo(postcodes)

        if geos is None or len(geos) == 0:
            return Promise.resolve([None for key in postcodes])

        key_map = [
            next((
                initialise_graphql_type(geo.get('result')) if geo.get('result') else None
                for geo in geos
                if geo['query'].replace(" ", "") == postcode
            ), None)
            for postcode in postcodes
        ]

        return Promise.resolve(key_map)

    return DataLoader(batch_load_fields)

def GeoFromCoordsLoaderFactory():
    '''
    Get geographic information for this coordinate.
    GeoLoaderFactory({...})
        .load({ latitude: y, longitude: x})
    GeoLoaderFactory({...})
        .load_many([{ latitude: y, longitude: y}, { latitude: y, longitude: x}])
    '''

    def get_cache_key(key):
        try:
            return f"{key.get('latitude')}::{key.get('longitude')}"
        except:
            return key

    def initialise_graphql_type(row):
        if row is None:
            return row
        return create_graphql_instance(
            GeocodeResult,
            **row
        )

    def batch_load_fields(coordinates):
        geos = bulk_coordinate_geo(coordinates)

        if geos is None or len(geos) == 0:
            return Promise.resolve([None for key in coordinates])

        key_map = [
            next((
                initialise_graphql_type(geo.get('result')) if geo.get('result') else None
                for geo in geos
                if geo['query'].get('latitude') == coord.get('latitude')
                and geo['query'].get('longitude') == coord.get('longitude')
            ), None)
            for coord in coordinates
        ]

        return Promise.resolve(key_map)

    return DataLoader(batch_load_fields, get_cache_key=get_cache_key)

def ConstituencyElectionLoaderFactory():
    '''
    ConstituencyElectionLoaderFactory({...})
        .load({ constituency: W07900008, date: 'latest' })
    ConstituencyElectionLoaderFactory({...})
        .load_many([{ constituency: W07900008, date: 'latest' }, { constituency: W07900008, date: '2017-06-08' }])
    '''

    def get_cache_key(key):
        try:
            return f"{key.get('constituency')}::{key.get('date')}"
        except:
            return key

    def batch_load_fields(keys):
        ids = set([k.get('constituency') for k in keys])
        if len(ids) > 50:
            elections = Election.objects.all()
        else:
            elections = Election.objects.filter(constituency_id__in=[key.get('constituency') for key in keys])

        def find_event(constituency, date):
            if date == 'latest':
                c_elections = list(filter(
                    lambda d: d.constituency_id == constituency,
                    elections
                ))
                return sorted(c_elections, key=lambda d: d.date, reverse=True)[0]
            else:
                date = parser.parse(date)
                return list(filter(
                    lambda d: (
                        d.constituency_id == constituency
                        and
                        d.date.date() == date.date()
                    ),
                    elections
                ))[0]

        key_map = [
            find_event(key.get('constituency'), key.get('date')) for key in keys
        ]

        return Promise.resolve(key_map)

    return DataLoader(batch_load_fields, get_cache_key=get_cache_key)

def ConstituencyElectionCandidateLoaderFactory():
    def batch_load_fields(keys):
        ids = set(keys)
        if len(ids) > 50:
            data = Candidate.objects.all()
        else:
            data = Candidate.objects.filter(election_id__in=ids)

        key_map = [
            list(filter(
                lambda d: d.election_id == key,
                data
            ))
            for key in keys
        ]

        return Promise.resolve(key_map)

    return DataLoader(batch_load_fields)

def ConstituencyLoaderFactory():
    def batch_load_fields(keys):
        ids = set(keys)
        if len(ids) == 650:
            data = Constituency.objects.all()
        else:
            data = Constituency.objects.filter(id__in=ids)

        key_map = [
            list(filter(lambda d: d.id == key, data))[0]
            for key in keys
        ]

        return Promise.resolve(key_map)

    return DataLoader(batch_load_fields)

def TravelToEventLoaderLoaderFactory():
    def get_cache_key(key):
        return f"{key.get('target')}::{key.get('postcode')}"

    def batch_load_fields(keys):
        fetch_map = {key['postcode']: set() for key in keys}
        postcodes = [key['postcode'] for key in keys]

        origin_map = {
            res.get('query'): create_point(
                latitude=res.get('result').get('latitude'),
                longitude=res.get('result').get('longitude')
            )
            for res in bulk_postcode_geo(postcodes)
        }

        for key in keys:
            fetch_map[key['postcode']].add(key['target'])

        results = {
            key['postcode']: {
                e.targetEvent_id: e for e
                in Event.objects.filter(
                    published=True,
                    targetEvent_id__in=target_event_ids,
                    category='TravelTogether'
                )
            }
            for postcode, target_event_ids
            in fetch_map.items()
        }

        return Promise.resolve([
            results.get(key['postcode']).get(key['target'])
            for key in keys
        ])

    return DataLoader(batch_load_fields, get_cache_key=get_cache_key)

def ConstituencyElectionsLoaderFactory():
    def batch_load_fields(keys):
        data = [Election.get_by_constituency(key) for key in keys]

        key_map = [
            list(sorted(filter(lambda d: d.constituency_id == key, data), key=lambda d: d.date))
            for key in keys
        ]

        return Promise.resolve(key_map)

    return DataLoader(batch_load_fields)

def EventLoaderFactory():
    def batch_load_fields(keys):
        data = Event.objects.filter(id__in=keys)

        key_map = [
            list(filter(lambda d: d.id == key, data))[0]
            for key in keys
        ]

        return Promise.resolve(key_map)

    return DataLoader(batch_load_fields)

def ConstituencyEventLoaderFactory(*args, **kwargs):
    def batch_load_fields(keys):
        data = Event.objects.filter(*args, constituency_id__in=keys, published=True, **kwargs)

        key_map = [
            list(filter(lambda d: d.constituency_id == key, data))
            for key in keys
        ]

        return Promise.resolve(key_map)

    return DataLoader(batch_load_fields)
