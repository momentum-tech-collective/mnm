import graphene
from . import geo, recommendations, django, auth, interactions

class Query(
    graphene.ObjectType,
    geo.Queries,
    recommendations.Queries,
    django.Queries,
    auth.Queries,
    interactions.Queries,
):
    # This class will inherit from multiple Queries
    # as we begin to add more apps to our project
    pass

class Mutation(
    graphene.ObjectType,
    django.Mutations,
    auth.Mutations,
    interactions.Mutations,
):
    # This class will inherit from multiple Queries
    # as we begin to add more apps to our project
    pass

schema = graphene.Schema(query=Query, mutation=Mutation)

from mynearestmarginal.graphql.context import DataLoadersMiddleware

def error_handler(ex):
    print("original exception", ex, ex.traceback if hasattr(ex, 'traceback') else '')
    raise Exception("System error") # some error message to show to users

class ErrorMiddleware:
    def resolve(self, next, root, info, **args):
        res = next(root, info, **args)

        return res.then(None, error_handler)

def query (query=None, variables={}, **kwargs):
    res = schema.execute(
        query,
        context=lambda: none,
        middleware=[DataLoadersMiddleware(),ErrorMiddleware()],
        variables=variables
    )

    return res
