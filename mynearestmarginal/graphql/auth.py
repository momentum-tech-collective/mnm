import graphene
from django import forms
from ..models import User, RequestUpdates
from .django import UserType
import graphql_jwt
from graphql_jwt.decorators import login_required
from graphene_django.rest_framework.mutation import SerializerMutation
from graphene_django.forms.mutation import DjangoModelFormMutation

class RequestUpdatesMutation(graphene.Mutation):
    class Arguments:
        email = graphene.String(required=True)
        postcode = graphene.String(required=True)
        is_contactable = graphene.Boolean(required=True)

    def mutate(self, info, **kwargs):
        capture = RequestUpdates.objects.create(**kwargs)
        return RequestUpdatesMutation(success=True)

    success = graphene.Field(graphene.Boolean)

class UserSignupMutation(graphene.Mutation):
    class Arguments:
        email = graphene.String(required=True)
        phone = graphene.String(required=True)
        postcode = graphene.String(required=True)
        first_name = graphene.String(required=True)
        last_name = graphene.String(required=True)
        is_contactable = graphene.Boolean(required=True)
        is_public = graphene.Boolean(required=True)
        tags = graphene.List(graphene.String, required=True)
        password = graphene.String(required=True)

    def mutate(self, info, **kwargs):
        user = User.objects.create_user(**kwargs)
        try:
            RequestUpdates.objects.get(email=kwargs['email']).delete()
        except:
            pass

        return UserSignupMutation(current_user=user)

    current_user = graphene.Field(UserType)

class UserUpdateMutation(graphene.Mutation):
    class Arguments:
        password = graphene.String()
        postcode = graphene.String()
        is_contactable = graphene.Boolean()
        is_public = graphene.Boolean()

    @login_required
    def mutate(self, info, **kwargs):
        user = info.context.user

        password = kwargs.pop('password', None)

        if password:
            info.context.user.set_password(password)

        for (key, value) in kwargs.items():
            setattr(info.context.user, key, value)

        info.context.user.save()

        return UserUpdateMutation(current_user=info.context.user)

    current_user = graphene.Field(UserType)


from rest_auth.serializers import LoginSerializer, JWTSerializer, UserDetailsSerializer, PasswordResetSerializer, PasswordChangeSerializer
# from rest_auth.registration.serializers import RegisterSerializer

# class LoginMutation(SerializerMutation):
#     class Meta:
#         serializer_class = LoginSerializer

# class TokenMutation(SerializerMutation):
#     class Meta:
#         serializer_class = JWTSerializer

class PasswordResetMutation(SerializerMutation):
    class Meta:
        serializer_class = PasswordResetSerializer

class PasswordChangeMutation(SerializerMutation):
    class Meta:
        serializer_class = PasswordChangeSerializer

# class RegisterMutation(SerializerMutation):
#     class Meta:
#         serializer_class = RegisterSerializer

###

class Queries():
    current_user = graphene.Field(UserType)

    @login_required
    def resolve_current_user(self, info):
        return info.context.user

class Mutations():
    # signup = graphene.Field(required=True)
    # login = LoginMutation.Field()
    # jwt = TokenMutation.Field()
    resetPassword = PasswordResetMutation.Field()
    changePassword = PasswordChangeMutation.Field()
    # register = RegisterMutation.Field()

    token_auth = graphql_jwt.ObtainJSONWebToken.Field()

    verify_token = graphql_jwt.Verify.Field()

    refresh_token = graphql_jwt.Refresh.Field()

    create_user = UserSignupMutation.Field()
    request_updates = RequestUpdatesMutation.Field()

    update_user = UserUpdateMutation.Field()
