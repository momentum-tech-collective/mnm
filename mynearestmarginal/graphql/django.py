import graphene
from graphene_django.types import DjangoObjectType
from django import forms
from django.db.models import Q
import operator
from graphene_django.forms.mutation import DjangoModelFormMutation
from .utils import DjangoFilterField
from .geo import TravelInfoType, GeocodeResult
import graphql_geojson
from helpers.utils import get, get_path
from helpers.cache import cached_fn
from ..models import *
from graphql_jwt.decorators import login_required
from django.utils import timezone
from datetime import datetime  
from datetime import timedelta  
from ..engine import politics, recommendations
from graphene.types.generic import GenericScalar
from helpers.geo import create_point
import urllib.parse

def straight_line_distance(coordinates, info, postcode):
  def calculate_distance(origin_geo):
    origin_point = create_point(latitude=origin_geo['latitude'], longitude=origin_geo['longitude'])
    distance = coordinates.distance(origin_point) * 100
    return distance
  return info.context.loaders.get('geo_from_postcode').load(postcode).then(calculate_distance)

class LivestreamType(DjangoObjectType):
  class Meta:
    model = Livestream
    fields = '__all__'
    filter_fields = ['id', 'name']

class CategoryType(DjangoObjectType):
  class Meta:
    model = Category
    fields = '__all__'

class SubcategoryType(DjangoObjectType):
  class Meta:
    model = Subcategory
    fields = '__all__'

class EventType(graphql_geojson.GeoJSONType):
  class Meta:
    model = Event
    geojson_field = 'coordinates'
    fields = '__all__'
    convert_choices_to_enum = False

  straight_line_distance = graphene.Float(required=True, postcode=graphene.String(required=True))

  def resolve_straight_line_distance(self, info, postcode):
    return straight_line_distance(self.coordinates, info, postcode)

  attendance = graphene.Int(required=True)

  def resolve_attendance(self, info):
    return self.cached_attendance_count()

  am_attending = graphene.Boolean()

  def resolve_am_attending (self, info):
    if not info.context.user.is_authenticated:
      return None
    # Temporarily disabled for performance until dataloaders on this
    # return len(info.context.user.commitments.filter(id=self.id)) > 0
    return False

  is_full = graphene.Boolean()

  def resolve_is_full(self, info):
    if self.max_capacity is None:
      return False
    return self.attendees.count() >= self.max_capacity

  geo = graphene.Field(GeocodeResult)

  def resolve_geo(self, info):
    if get(self, 'geo'):
      return get(self, 'geo')
    else:
      data = info.context.loaders.get('geo_from_postcode').load(self.postcode)
      if data is None:
        data = info.context.loaders.get('geo_from_coords').load({
          "longitude": self.coordinates.x,
          "latitude": self.coordinates.y,
        })
      return data

  def resolve_constituency(self, info):
    if self.constituency_id is None:
      return None

    return info.context.loaders.get('constituency_by_id').load(self.constituency_id)

  def resolve_targetEvent(self, info):
    if self.targetEvent_id is None:
      return None

    return info.context.loaders.get('event_by_id').load(self.targetEvent_id)

  travel_to_event = graphene.Field(lambda: EventType, postcode=graphene.String(required=True))

  # TODO: Make graphene decorators for these usecases, when we can figure out how

  start_time = graphene.String(required=True, format=graphene.String())

  def resolve_start_time(self, info, **kwargs):
    format = kwargs.get('format')
    return self.start_time.strftime(format) if format else self.start_time

  end_time = graphene.String(required=False, format=graphene.String())

  def resolve_end_time(self, info, **kwargs):
    format = kwargs.get('format')
    return self.end_time.strftime(format) if format else self.end_time

  def resolve_travel_to_event(self, info, postcode):
    return info.context.loaders.get('event_travel_to_event').load({
      'target': self.id,
      'postcode': postcode
    })

class EventForm(forms.ModelForm):
  class Meta:
    model = Event
    fields = ('name','address', 'postcode', 'category', 'subcategory', 'description', 'start_time', 'end_time', 'contact_number', 'constituency',)

class EventMutation(DjangoModelFormMutation):
  event = graphene.Field(EventType)

  class Meta:
    form_class = EventForm

class UserType(DjangoObjectType):
  class Meta:
    model = User
    filter_fields = ['id', 'email']
    exclude = ['password']

  @classmethod
  def get_queryset(cls, queryset, info):
    return [info.context.user]

  commitmentCount = graphene.Int()

  def resolve_commitmentCount(self, info):
    return self.commitments.count()

  upcomingCommitmentCount = graphene.Int()

  def resolve_upcomingCommitmentCount(self, info):
    return self.commitments.filter(
    Q(start_time__gte=datetime.now()) | Q(end_time__gte=datetime.now())
  ).count()

  nationbuilder_account = GenericScalar()

  @login_required
  def resolve_nationbuilder_account(self, info):
    # Highly sensitive really
    if self.id != info.context.user.id:
      return
    return self.nationbuilder_account()

  upcoming_commitments = graphene.List(graphene.NonNull(EventType))

  def resolve_upcoming_commitments(self, info):
    return self.commitments.filter(
    Q(start_time__gte=datetime.now()) | Q(end_time__gte=datetime.now())
  )

class CommunicationPlatformType(DjangoObjectType):
  class Meta:
    model = CommunicationPlatform
    fields = '__all__'
    filter_fields = ['name', 'id']
    exclude = []

class GroupTagType(DjangoObjectType):
  class Meta:
    model = GroupTag
    fields = '__all__'
    filter_fields = ['name', 'id']
    exclude = []

class CommunicationGroupType(graphql_geojson.GeoJSONType):
  class Meta:
    model = CommunicationGroup
    geojson_field = 'coordinates'
    fields = '__all__'

  encoded_join_link = graphene.String(required=True)
  def resolve_encoded_join_link(self, info):
    return urllib.parse.quote(self.join_link)

  straight_line_distance = graphene.Float(required=True, postcode=graphene.String(required=True))
  def resolve_straight_line_distance(self, info, postcode):
    return straight_line_distance(self.coordinates, info, postcode)

  geo = graphene.Field(GeocodeResult)

  def resolve_geo(self, info):
    if get(self, 'geo'):
      return get(self, 'geo')
    else:
      data = info.context.loaders.get('geo_from_coords').load({
        "longitude": self.coordinates.x,
        "latitude": self.coordinates.y,
      })
    return data

class CommitmentType(DjangoObjectType):
  class Meta:
    model = Commitment
    filter_fields = ['user__id', 'event__id']
    fields = '__all__'

class ElectionType(DjangoObjectType):
  class Meta:
    model = Election
    filter_fields = ['name', 'id', 'date']
    fields = '__all__'

  def resolve_candidate_set(self, info):
    return info.context.loaders.get('candidates_by_election_id').load(self.id)

  def resolve_constituency(self, info):
    return info.context.loaders.get('constituency_by_id').load(self.constituency_id)

class CandidateType(DjangoObjectType):
  class Meta:
    model = Candidate
    filter_fields = ['name', 'id', 'party']
    fields = '__all__'

class ConstituencyType(DjangoObjectType):
  class Meta:
    model = Constituency
    filter_fields = ['name', 'id']
    exclude = ['priority']

  election_set = graphene.Field(graphene.List(graphene.NonNull(ElectionType), required=True), description="List of this seat's elections, sorted by date (earliest first)")

  def resolve_election_set(self, info):
    return info.context.loaders.get('election_set_by_constituency_id').load(self.id)

  volunteer_need_band = graphene.Field(graphene.Int, required=True)

  def resolve_volunteer_need_band(self, info):
    if self.priority is not None:
      return self.priority
    else:
      return politics.constituency_priority(self)

  last_election = graphene.Field(ElectionType)

  def resolve_last_election(self, info):
    return info.context.loaders.get('constituency_election').load({
      "constituency": self.id,
      "date": "latest",
    })

  election = graphene.Field(ElectionType, date=graphene.String(required=True))

  def resolve_election(self, info, date):
    return info.context.loaders.get('constituency_election').load({
      "constituency": self.id,
      "date": date,
    })

  event_set = graphene.List(graphene.NonNull(EventType), required=True)

  campaign_centres = graphene.List(
    graphene.NonNull(EventType),
    required=True
  )

  def resolve_campaign_centres(self, info):
    def filter_events(events):
      return [
        e for e
        in events
        if (
          e.subcategory.name == "Campaign Center"
          or
          e.subcategory.name == "Campaign Centre"
          or
          e.labour_category == "Polling Day Campaign Center"
          or
          e.labour_category == "Polling Day Campaign Centre"
        )
      ]

    return info.context.loaders.get('events_today_by_constituency_id').load(self.id).then(filter_events)

  upcoming_events = graphene.List(graphene.NonNull(EventType), required=True)

  def resolve_upcoming_events(self, info):
    def filter_events(events):
      return [
        e for e
        in events
        if (
          politics.constituency_priority(self) > 0
          or e.category != 'Campaigning'
        ) and e.category != 'TravelTogether'
      ]

    return info.context.loaders.get('upcoming_events_by_constituency_id').load(self.id).then(filter_events)


class LocationType(graphql_geojson.GeoJSONType):
  class Meta:
    model = Location
    geojson_field = 'coordinates'
    filter_fields = ['name', 'id', 'constituency__id']
    fields = '__all__'

from django.db.models import Q

MAX_COUNT = 100

class Queries():
  events_by_category = graphene.List(graphene.NonNull(EventType),
              category=graphene.String(required=True),
              date_from=graphene.String(default_value=(datetime.now() - timedelta(hours=1)).isoformat()),
              date_to=graphene.String(default_value=(datetime.now() + timedelta(days=2)).isoformat()),
              constituency_id=graphene.String(),
              count=graphene.Int(default_value = 30),
              required=True)

  def resolve_events_by_category(self, info, category = None, count = 30, **kwargs):

    events = Event.objects.filter(
        published=True,
        start_time__gte = kwargs.get('date_from'),
        start_time__lte=kwargs.get('date_to')
    )
    
    category = ''.join(category.split()).upper()

    events = events.filter(
      Q(category__name__iexact=category) |
      Q(subcategory__name__iexact=category)
    )

    if kwargs.get('constituency_id'):
      events = events.filter(
        constituency_id=kwargs.get('constituency_id')
      )

    count = count if count < MAX_COUNT else MAX_COUNT

    return events[:count]

  event = graphene.Field(EventType, id=graphene.ID(required=True))

  def resolve_event(self, info, id):
    return Event.objects.get(pk=id)

  categories = graphene.List(graphene.NonNull(CategoryType), required=True)

  def resolve_categories(self, info):
    return Category.objects.all()

  constituencies = DjangoFilterField(ConstituencyType)

  marginal_constituencies = graphene.List(ConstituencyType, required=True)

  def resolve_marginal_constituencies(self, info):
    return politics.marginal_constituencies()

  constituency = graphene.Field(ConstituencyType, id=graphene.String(required=True))

  def resolve_constituency(self, info, id):
    return Constituency.objects.get(pk=id)

  elections = DjangoFilterField(ElectionType)

  election = graphene.Field(ElectionType, id=graphene.ID(required=True))

  def resolve_election(self, info, id):
    return Election.objects.get(pk=id)

  candidates = DjangoFilterField(CandidateType)

  candidate = graphene.Field(CandidateType, id=graphene.ID(required=True))

  def resolve_candidate(self, info, id):
    return Candidate.objects.get(pk=id)

  locations = DjangoFilterField(LocationType)

  location = graphene.Field(LocationType, id=graphene.ID(required=True))

  def resolve_location(self, info, id):
    return Location.objects.get(pk=id)

  communication_platforms = DjangoFilterField(CommunicationPlatformType)

  group_tags = DjangoFilterField(GroupTagType)

  livestreams = graphene.List(LivestreamType, required=True)

  def resolve_livestreams(self, info):
    return Livestream.objects.filter(end_time__gte=timezone.now()).order_by('start_time')



class Mutations():
  # create_event = EventMutation.Field()
  pass
