import graphene
from .types import DateInt
from ..engine import labour, nationbuilder
from mynearestmarginal import engine
from mynearestmarginal.models import CommunicationGroup
from .utils import resolve_attr
from django import forms
from graphene_django.forms.mutation import DjangoModelFormMutation
from .django import UserType, EventType, CommunicationGroupType
from .geo import Coordinates
from ..models import Commitment, Event, Category, Subcategory
from graphql_jwt.decorators import login_required
from django.contrib.gis.geos import Point
import datetime

import pprint
import logging
pp = pprint.PrettyPrinter()

class CommunicationGroupForm(forms.ModelForm):
    class Meta:
        model = CommunicationGroup
        fields = (
            'name',
            'description',
            'join_link',
            'platform',
            'group_count',
            'constituencies',
            'tags',
        )

class CommunicationGroupMutation(DjangoModelFormMutation):
    communicationGroup = graphene.Field(CommunicationGroupType)

    class Meta:
        form_class = CommunicationGroupForm

class CommitmentMutation(graphene.Mutation):
    class Arguments:
        event_id = graphene.ID(required=True)

    current_user = graphene.Field(UserType) # to show list of commitments
    event = graphene.Field(EventType)

    def resolve_event(self, info):
        return Event.objects.get(id=self.event)

    @login_required
    def mutate(self, info, event_id):
        try:
            Commitment.objects.create(event_id=event_id, user=info.context.user)
            # TODO: Nationbuilder link
        except:
            pass
        return CommitmentMutation(
            current_user=info.context.user,
            event=event_id
        )

class UncommitmentMutation(graphene.Mutation):
    class Arguments:
        event_id = graphene.ID(required=True)

    current_user = graphene.Field(UserType) # to show list of commitments
    event = graphene.Field(EventType)

    def resolve_event(self, info):
        return Event.objects.get(id=self.event)

    @login_required
    def mutate(self, info, event_id):
        Commitment.objects.get(event_id=event_id, user=info.context.user).delete()
        # TODO: Nationbuilder link
        return CommitmentMutation(
            current_user=info.context.user,
            event=event_id
        )

class CreateEventMutation(graphene.Mutation):
    class Arguments:
        name = graphene.String(required=True)
        description = graphene.String(required=True)
        start_time = graphene.Int(required=True)
        constituency = graphene.String(required=False)
        target_event = graphene.String(required=False)
        address = graphene.String(required=True) 
        postcode = graphene.String(required=True)
        whatsapp_link = graphene.String(required=False)
        level = graphene.String(required=True)
        contact_number = graphene.String(required=False)
        contact_email = graphene.String(required=False)
        cat_or_subcat_id = graphene.String(required=True)
        other_category_description = graphene.String(required=False)

    event = graphene.Field(EventType)
    current_user = graphene.Field(UserType)

    # @login_required
    def mutate(self, info, **kwargs):
        # TODO: A more elegant custom Date type that vibes between Django and Graphene
        kwargs['start_time'] = datetime.datetime.fromtimestamp(kwargs['start_time'])

        geo = engine.geo.postcode_geo(kwargs.get('postcode'))
        kwargs['coordinates'] = engine.geo.point_from_geo(geo)

        c_id = kwargs.pop('constituency', None)
        target_id = kwargs.pop('target_event', None)

        # Even though we have categories and subcategories,
        # the user sees them as one flat level
        level = kwargs.pop('level', 'subcategory')
        cat_or_subcat_id = kwargs.pop('cat_or_subcat_id', None)            
        event = Event(**kwargs)
        if level == 'subcategory':
            event.subcategory = Subcategory.objects.get(pk=int(cat_or_subcat_id))
            event.category = event.subcategory.parent
        else:
            event.category = Category.objects.get(pk=int(cat_or_subcat_id))
        event.save()
        
        return CreateEventMutation(event=event)

class Queries():
    pass

class Mutations():
    commit = CommitmentMutation.Field()
    uncommit = UncommitmentMutation.Field()
    create_event = CreateEventMutation.Field()
    # create_group = CommunicationGroupMutation.Field()
