import graphene
from .django import EventType, ConstituencyType, CommunicationGroupType
from .geo import TravelInfoType, DistanceType
from helpers.utils import get
from ..engine.recommendations import event_recommendations_by_postcode, constituency_recommendations_by_postcode, polling_day_constituency_recommendations_by_postcode, group_recommendations_by_postcode, recommended_constituency_travel_to_events, events_near_postcode
from graphene.types.generic import GenericScalar
from datetime import datetime


class ConstituencyRecommendationType(graphene.ObjectType):
    id = graphene.ID(required=True)
    constituency = graphene.Field(ConstituencyType, required=True)
    distance = graphene.NonNull(DistanceType)
    events = graphene.List(graphene.NonNull(EventType), required=True, item_count=graphene.Int(default_value=10000))

    def resolve_events(self, info, item_count):
        return get(self, 'events')[:item_count]

class ConstituencyTravelRecommendationType(graphene.ObjectType):
    travel_to_events = graphene.List(
        graphene.NonNull(EventType), required=True)
    travel_to_wanted_events = graphene.List(
        graphene.NonNull(EventType), required=True)


class Queries():
    groups_for_postcode = graphene.List(graphene.NonNull(CommunicationGroupType),
                                        required=True,
                                        postcode=graphene.String(required=True),
                                        item_count=graphene.Int(default_value=10000))

    def resolve_groups_for_postcode(self, info, postcode, item_count):
        return group_recommendations_by_postcode(postcode)[:item_count]

    events_for_postcode = graphene.List(graphene.NonNull(EventType),
                                        required=True,
                                        postcode=graphene.String(
                                            required=True),
                                        date_start=graphene.String(
                                            required=True),
                                        date_end=graphene.String(
                                            required=True),
                                        search_radius=graphene.Float(),
                                        item_count=graphene.Int(default_value=10000))

    def resolve_events_for_postcode(self, info, postcode, date_start, date_end, item_count, **kwargs):
        return event_recommendations_by_postcode(postcode, date_start, date_end, **kwargs)[:item_count]

    events_near_postcode = graphene.List(graphene.NonNull(EventType),
                                         required=True,
                                         postcode=graphene.String(
        required=True),
        date_start=graphene.String(
        required=True),
        date_end=graphene.String(
        required=True),
        search_radius=graphene.Float())

    def resolve_events_near_postcode(self, info, postcode, date_start, date_end, **kwargs):
        return events_near_postcode(postcode, date_start, date_end, **kwargs)

    constituencies_for_postcode = graphene.List(graphene.NonNull(ConstituencyRecommendationType),
                                                required=True,
                                                search_radius=graphene.Float(),
                                                postcode=graphene.String(required=True))

    def resolve_constituencies_for_postcode(self, info, **kwargs):
        return constituency_recommendations_by_postcode(**kwargs)

    polling_day_constituencies_for_postcode = graphene.List(graphene.NonNull(ConstituencyRecommendationType),
                                                required=True,
                                                search_radius=graphene.Float(),
                                                postcode=graphene.String(required=True))

    def resolve_polling_day_constituencies_for_postcode(self, info, **kwargs):
        return polling_day_constituency_recommendations_by_postcode(**kwargs)

    constituency_travel_to_events = graphene.Field(
        ConstituencyTravelRecommendationType,
        required=True,
        constituency_id=graphene.String(required=True),
        postcode=graphene.String(required=True)
    )

    def resolve_constituency_travel_to_events(self, info, **kwargs):
        return recommended_constituency_travel_to_events(**kwargs)
