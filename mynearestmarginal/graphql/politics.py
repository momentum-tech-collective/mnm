import graphene
from ..engine import politics

class Candidate(graphene.ObjectType):
    id = graphene.ID()
    local_id = graphene.ID()
    fullName = graphene.String()
    party = graphene.String()
    order = graphene.Int()
    isElectionCandidate = graphene.Boolean()
    numberOfVotes = graphene.Int()
    voteChangePercentage = graphene.Float()

class Election(graphene.ObjectType):
    id = graphene.Int()
    name = graphene.String()
    global_id = graphene.Int()
    electionType = graphene.String()
    electorate = graphene.Int()
    turnout = graphene.Int()
    resultOfElection = graphene.String()
    majority = graphene.Int()

    date = graphene.String()

    def resolve_date(self, info):
        data = politics.get_election_metadata(self.get('global_id'))
        return data.get('date')

    candidates = graphene.List(Candidate)

    def resolve_candidates(self, info):
        data = [
            politics.get_election_candidate(self.get('id'), id)
            for id in self.get('candidate_local_ids')
        ]
        return data