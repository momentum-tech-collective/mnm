from mynearestmarginal.graphql import loaders
from datetime import datetime
from django.db.models import Q

class DataLoadersMiddleware:
	def resolve(self, next, root, info, **kwargs):
		if not hasattr(info, 'context'):
			info.context = lambda: none
		if not hasattr(info.context, 'loaders'):
			# TODO: Postcode bulk loader
			# TODO: Labour event bulk loader
			setattr(info.context, 'loaders', {
				'travel_info': loaders.TravelInfoLoaderFactory(),
				'geo_from_postcode': loaders.GeoFromPostcodesLoaderFactory(),
				'geo_from_coords': loaders.GeoFromCoordsLoaderFactory(),
				'constituency_by_id': loaders.ConstituencyLoaderFactory(),
				'event_by_id': loaders.EventLoaderFactory(),
				'events_by_constituency_id': loaders.ConstituencyEventLoaderFactory(),
				'upcoming_events_by_constituency_id': loaders.ConstituencyEventLoaderFactory(Q(
					start_time__gte=datetime.now()
				)),
				'events_today_by_constituency_id': loaders.ConstituencyEventLoaderFactory(Q(
					# Starts today
					Q(start_time__date=datetime.date(datetime.now())) &
					(
						# Ends after the current time
						Q(end_time__gte=datetime.now()) 
						|
						# Or, if end time not known, just show it regardless
						Q(end_time__isnull=True)
					)
				)),
				'election_set_by_constituency_id': loaders.ConstituencyElectionsLoaderFactory(),
				'constituency_election': loaders.ConstituencyElectionLoaderFactory(),
				'candidates_by_election_id': loaders.ConstituencyElectionCandidateLoaderFactory(),
				'event_travel_to_event': loaders.TravelToEventLoaderLoaderFactory(),
			})
		return next(root, info, **kwargs)
