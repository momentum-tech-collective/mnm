from graphene import ObjectType
from graphene.types.scalars import Scalar
from graphql.language import ast
import numpy as np

class DateInt(Scalar):
    '''
    The `Date` scalar type parses Date strings as specified by
    [iso8601](https://en.wikipedia.org/wiki/ISO_8601) and
    coerces them to numpy.datetime64
    serialisable as a unix timestap (Int)
    '''

    @staticmethod
    def serialize(input):
        if isinstance(input, str):
            input = DateInt.parse_value(input)
        return int(input.view('<i8')) * 1000

    @classmethod
    def parse_literal(cls, node):
        if isinstance(node, ast.StringValue):
            return cls.parse_value(node.value)

    @staticmethod
    def parse_value(str):
        return np.datetime64(str)