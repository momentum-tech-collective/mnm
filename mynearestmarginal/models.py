import json
import os
from itertools import islice

import requests
from django.contrib.gis.db import models
from django.core import serializers
from django.db.models.signals import post_save
from helpers.cache import cache_resolve_many, cache_resolve
from django.dispatch import receiver
from django.conf import settings

from .engine import nationbuilder
from .engine.geo import TransportModes
from .util import tzformat
from helpers.utils import get
from helpers.cache import cached_fn
from django.core.mail import send_mail
from django.contrib.auth.models import PermissionsMixin
from django.contrib.auth.base_user import AbstractBaseUser
from django.utils.translation import ugettext_lazy as _
from allauth.account.signals import user_signed_up
import datetime
from django.utils import timezone
import uuid

from djrichtextfield.models import RichTextField

class DatedModel(models.Model):
    created_date = models.DateTimeField(null=True, blank=True, auto_now_add=True)
    modified_date = models.DateTimeField(null=True, blank=True, auto_now=True)
    class Meta:
        abstract = True

class UUIDModel(models.Model):
	id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)

	class Meta:
		abstract = True


class Party(UUIDModel):
	name = models.CharField(unique=True, null=False, blank=False, max_length=200)
	short_name = models.CharField(unique=True, null=False, blank=False, max_length=30)
	primary_color_hex = models.CharField(null=False, blank=False, max_length=6)

	def __str__(self):
		return self.name


class LocalAuthority(models.Model):
	class Meta:
		verbose_name_plural = "local authorities"
	code = models.CharField(unique=True, null=False, blank=False, max_length=16)
	name = models.CharField(unique=True, null=False, blank=False, max_length=200)
	centroid = models.PointField(null=True, blank=False, srid=4326, geography=True)

	def __str__(self):
		return self.name


class Constituency(models.Model):
	class Meta:
		verbose_name_plural = "constituencies"

	@staticmethod
	@cached_fn(cache_type="memory", key=__package__ + "constituency.cachedMap")
	def cache():
		return {c.id: c for c in Constituency.objects.all()}

	@staticmethod
	def targets_for(id):
		return list(Constituency.get(id).target_constituencies.all())

	@staticmethod
	def polling_day_targets_for(id):
		return list(Constituency.get(id).target_constituencies.filter(
			polling_day_attendance__lte=2
		))

	@staticmethod
	def get(constituency_id):
		if constituency_id is None:
			return None

		return Constituency.cache()[constituency_id]

	id = models.CharField(primary_key=True, max_length=30, unique=True, null=False, blank=False)
	parl_id = models.IntegerField(unique=True, null=False, blank=False)
	name = models.CharField(unique=True, null=False, blank=False, max_length=200)
	priority = models.IntegerField(blank=True, null=True, choices = [(0, '0 - Ignore'), (1, '1'), (2, '2'), (3, '3'), (4, '4'), (5, '5 - Top priority')])
	attendance = models.IntegerField(blank=False, null=False, default=2, choices = [(0, '0 - Few Attendees'), (1, '1'), (2, '2'), (3, '3'), (4, '4'), (5, '5 - Over Capacity')])
	polling_day_attendance = models.IntegerField(blank=False, null=False, default=2, choices = [(1, '1 - Prioritise if possible'), (2, '2 - Normal targeting priority'), (3, '3 - Over Capacity; divert away')])
	target_constituencies = models.ManyToManyField(to='self', symmetrical=False, blank=True)
	centroid = models.PointField(null=True, blank=False, srid=4326, geography=True)

	def __str__(self):
		return self.name

	@staticmethod
	def as_serializable_dict(instances):
		serialized_json = serializers.serialize('json', instances, fields=('name', 'priority', ))
		return [instance['fields'] for instance in json.loads(serialized_json)]


class Election(UUIDModel):
	@staticmethod
	@cached_fn(cache_type="memory", key=__package__ + "Election.cachedMapByConstituencyDate")
	def cache_by_constituency_and_date():
		res = {}

		for e in Election.objects.all():
			if e.constituency_id not in res:
				res[e.constituency_id] = {}

			res[e.constituency_id][e.date.timestamp()] = e

		return res

	@staticmethod
	@cached_fn(cache_type="memory", key=__package__ + "Election.cachedMap")
	def cache():
		return {e.id: e for e in Election.objects.all()}

	@staticmethod
	def get_by_constituency(id):
		c = Election.cache_by_constituency_and_date()[id]
		if c is None:
			return []

		return c.values()

	@staticmethod
	def get_by_constituency_and_date(constituency_id, date):
		c = Election.cache_by_constituency_and_date().get(constituency_id)
		if c is None:
			return None

		return c.get(date.timestamp())

	date = models.DateTimeField(null=False, blank=False)
	constituency = models.ForeignKey(Constituency, related_name='elections', blank=False, null=False,
									 on_delete=models.DO_NOTHING)
	name = models.CharField(null=False, blank=False, max_length=200)
	turnout = models.IntegerField(null=False, blank=False)
	electorate = models.IntegerField(null=False, blank=False)
	majority = models.IntegerField(null=False, blank=False)
	winning_candidate = models.CharField(null=False, blank=False, max_length=200)
	winning_party = models.CharField(null=False, blank=False, max_length=200)
	labour_marginality = models.IntegerField(null=False, blank=False)
	result_statement = models.CharField(null=False, blank=False, max_length=200, help_text='E.g. "Lab hold"')

	def __str__(self):
		return self.name

	class Meta:
		get_latest_by = ['date']


class Candidate(UUIDModel):
	name = models.CharField(null=False, blank=False, max_length=200)
	party = models.CharField(null=False, blank=False, max_length=200)
	votes = models.IntegerField(default=0, null=False, blank=False)
	swing = models.FloatField(default=0, null=False, blank=False)
	share = models.FloatField(default=0, null=False, blank=False)
	elected = models.BooleanField(default=False, null=False, blank=False)
	election = models.ForeignKey(Election, blank=False, null=False, on_delete=models.DO_NOTHING)

	def __str__(self):
		return f'{self.name} ({self.party}) {self.election}'

class GroupTag(UUIDModel):
	'''
	These are admin-definable tags that can be used to categorise
	communication groups.
	'''
	icon = models.CharField(null=True, blank=True, max_length=200, default='✅', help_text='Emoji or image URL')
	name = models.CharField(null=False, blank=False, max_length=200)
	description = models.CharField(null=True, blank=True, max_length=400)

	def __str__(self):
		return self.name

class CommunicationPlatform(UUIDModel):
	''' e.g.
	('WhatsappChat', 'Whatsapp Chat Group'),
	('WhatsappBroadcast', 'Whatsapp Broadcast Group'),
	('FacebookGroup', 'FacebookGroup'),
	('EmailBroadcast', 'EmailBroadcast'),
	'''
	icon = models.CharField(null=True, blank=True, max_length=200, default='💬', help_text='Emoji or image URL')
	name = models.CharField(null=False, blank=False, max_length=200)
	description = models.CharField(null=True, blank=True, max_length=400)

	def __str__(self):
		return self.name

class CommunicationGroup(UUIDModel, DatedModel):
	'''
	These are recommendable communication groups that can be shown to users.
	'''
	name = models.CharField(null=False, blank=False, max_length=200)
	description = models.CharField(null=True, blank=True, max_length=400)
	join_link = models.URLField(null=False, blank=False, max_length=1000)
	published = models.BooleanField(null=True, blank=True, default=False)
	official = models.BooleanField(null=True, blank=True, default=False, help_text='Mark official if this is the legit real mccoy whatsapp group for the area. Only system admins like yourself can do this, not public users.')
	group_count = models.IntegerField(null=True, blank=True, help_text='If you know it, a rough idea of how many people are in this group already')
	coordinates = models.PointField(null=True, blank=True, srid=4326, geography=True)
	postcode = models.CharField(null=True, blank=False, max_length=30)

	constituencies = models.ManyToManyField(Constituency)
	local_authority = models.ForeignKey(LocalAuthority, null=True, blank=True, on_delete=models.SET_NULL)
	visitors = models.ManyToManyField('User')
	platform = models.ForeignKey(CommunicationPlatform, on_delete=models.DO_NOTHING)
	tags = models.ManyToManyField(GroupTag)

	def __str__(self):
		return self.name

class Livestream(UUIDModel):
	name = models.CharField(null=False, blank=False, max_length=200)
	link = models.CharField(null=False, blank=False, max_length=200)
	description = models.TextField(null=False, blank=False, max_length=512)
	start_time = models.DateTimeField(null=False, blank=False)
	end_time = models.DateTimeField(null=False, blank=True)
	contact_number = models.CharField(max_length=5000, null=True, blank=True)
	contact_email = models.CharField(max_length=5000, null=True, blank=True)
	created_time = models.DateTimeField(null=False, blank=True, auto_now_add=True)
	published = models.BooleanField(null=False, blank=False, default=False)
	organisation = models.TextField(null=True, blank=True)
	manager = models.ForeignKey('User', related_name='managed_livestreams', null=True, blank=True,
								on_delete=models.DO_NOTHING)
	def __str__(self):
		date = tzformat(self.start_time, "%B %d, %Y, %I:%M %p")
		return f'{self.name} ({date})'


class BaseCategoryType(models.Model):
	name = models.CharField(max_length=30)
	emoji = models.CharField(max_length=2)
	description = models.CharField(max_length=200)
	userVisible = models.BooleanField(default=False, help_text="Visible to users")
	allowUserCreate = models.BooleanField(default=False, help_text="Available for user-created events")

	def __str__(self):
		return self.name

	class Meta:
		abstract = True		


class Category(BaseCategoryType):
	class Meta:
		verbose_name_plural = "Categories"


class Subcategory(BaseCategoryType):
	parent = models.ForeignKey(Category, related_name='subcategories', on_delete=models.CASCADE)

	class Meta:
		verbose_name_plural = "Subcategories"


class Event(UUIDModel):
	@staticmethod
	@cached_fn(cache_type="memory", key=__package__ + "Event.cachedMap")
	def attendance_cache():
		return {e.id: e.attendees.count() for e in Event.objects.all()}

	def cached_attendance_count(self):
		# TODO: do this efficiently
		return 0
		return Event.attendance_cache().get(self.id) or 0

	labour_id = models.IntegerField(unique=True, null=True, blank=True)
	labour_priority = models.IntegerField(null=True, blank=True)
	nationbuilder_id = models.IntegerField(unique=True, null=True, blank=True)
	name = models.CharField(null=False, blank=False, max_length=200)
	address = models.CharField(null=False, blank=False, max_length=500)
	postcode = models.CharField(null=False, blank=False, max_length=30)
	coordinates = models.PointField(null=False, blank=False, srid=4326, geography=True)
	category = models.ForeignKey(Category, help_text='Used for filtering and prioritising recommendations. Inferred from Labour data.', on_delete=models.DO_NOTHING)
	subcategory = models.ForeignKey(Subcategory, null=True, blank=True, help_text="Will affect prioritisation and visual style.", on_delete=models.DO_NOTHING)
	other_category_description = models.CharField(null=True, blank=True, max_length=200)
	labour_category = models.CharField(null=True, blank=True, max_length=200)
	whatsapp_link = models.CharField(null=True, blank=True, max_length=200)
	description = models.TextField(null=False, blank=False)
	start_time = models.DateTimeField(null=False, blank=False)
	end_time = models.DateTimeField(null=True, blank=True)
	contact_number = models.CharField(max_length=5000, null=True, blank=True)
	contact_email = models.CharField(max_length=5000, null=True, blank=True)
	constituency = models.ForeignKey(Constituency, related_name='events', blank=True, null=True,
									 on_delete=models.DO_NOTHING)
	targetEvent = models.ForeignKey('self', blank=True, null=True, on_delete=models.DO_NOTHING)
	attendees = models.ManyToManyField('User', through='Commitment', blank=True)
	max_capacity = models.IntegerField(null=True, blank=True,
									   help_text="Used for Momentum's own Training events. Will limit the number of attendees who can RSVP.")
	created_time = models.DateTimeField(null=False, blank=True, auto_now_add=True)
	published = models.BooleanField(null=False, blank=False, default=False)
	manager = models.ForeignKey('User', related_name='managed_events', null=True, blank=True,
								on_delete=models.DO_NOTHING)

	@staticmethod
	def as_serializable_dict(instances):
		fields = [
					'name',
					'address',
					'postcode',
					'coordinates',
					'category',
					'subcategory',
					'other_category_description',
					'whatsapp_link',
					'description',
					'start_time',
					'end_time',
					'contact_number',
					'contact_email',
					'constituency',
					'targetEvent',
					'attendees',
					'max_capacity',
					'created_time',
					'published',
					'manager',
				]
		serialized_json = serializers.serialize('json', instances, fields=fields)
		return [instance['fields'] for instance in json.loads(serialized_json)]

	def __str__(self):
		date = tzformat(self.start_time, "%B %d, %Y, %I:%M %p")
		return f'{self.name} ({date}) in {str(self.constituency)}'


class Commitment(UUIDModel):
	class Meta:
		db_table = 'mynearestmarginal_commitments'
		unique_together = [['user', 'event']]

	user = models.ForeignKey('User', on_delete=models.DO_NOTHING)
	event = models.ForeignKey(Event, on_delete=models.DO_NOTHING)
	created_time = models.DateTimeField(null=False, blank=True, auto_now_add=True)

	def __str__(self):
		return self.user.full_name() + ' campaigning on ' + tzformat(self.datetime, "%B %d, %Y, %I:%M %p")


class Location(UUIDModel):
	name = models.CharField(null=False, blank=False, max_length=200)
	contact_phone = models.CharField(max_length=200)
	contact_name = models.CharField(max_length=200)
	address = models.CharField(null=False, blank=False, max_length=500)
	constituency = models.ForeignKey(Constituency, blank=False, null=False, on_delete=models.DO_NOTHING)
	coordinates = models.PointField(null=False, blank=False, srid=4326, geography=True)
	postcode = models.CharField(max_length=10, blank=False, null=False)

	def __str__(self):
		return self.name + ' - in ' + str(self.constituency)


from django.contrib.auth.base_user import BaseUserManager


class UserManager(BaseUserManager):
	use_in_migrations = True

	def _create_user(self, email, password, **extra_fields):
		"""
		Creates and saves a User with the given email and password.
		"""
		if not email:
			raise ValueError('The given email must be set')
		email = self.normalize_email(email)
		user = self.model(email=email, **extra_fields)
		user.set_password(password)
		user.save(using=self._db)
		return user

	def create_user(self, email, password, tags=[], **registration_fields):
		registration_fields.setdefault('is_superuser', False)
		user = self._create_user(email, password, **registration_fields)
		user.sync_nationbuilder_contact(tags=tags)
		return user

	def create_superuser(self, email, password, **extra_fields):
		extra_fields.setdefault('is_superuser', True)

		if extra_fields.get('is_superuser') is not True:
			raise ValueError('Superuser must have is_superuser=True.')

		return self._create_user(email, password, **extra_fields)


class User(UUIDModel, AbstractBaseUser, PermissionsMixin):
	class Meta:
		verbose_name = _('user')
		verbose_name_plural = _('users')

	email = models.EmailField(_('email address'), blank=True, unique=True)
	phone = models.CharField(_('mobile phone'), max_length=30, null=False, blank=False, unique=False)
	first_name = models.CharField(_('first name'), max_length=30, blank=True)
	last_name = models.CharField(_('last name'), max_length=150, blank=True)
	is_staff = models.BooleanField(
		_('staff status'),
		default=False,
		help_text=_('Designates whether the user can log into this admin site.'),
	)
	is_active = models.BooleanField(
		_('active'),
		default=True,
		help_text=_(
			'Designates whether this user should be treated as active. '
			'Unselect this instead of deleting accounts.'
		),
	)
	date_joined = models.DateTimeField(_('date joined'), default=timezone.now)
	photo_url = models.URLField(null=True, blank=True)

	objects = UserManager()

	USERNAME_FIELD = 'email'
	REQUIRED_FIELDS = ['first_name', 'last_name']

	postcode = models.CharField(max_length=10)
	nationbuilder_id = models.IntegerField(unique=True, null=True)
	commitments = models.ManyToManyField(Event, through=Commitment, blank=True)
	preferred_transport = models.CharField(
		blank=True,
		null=False,
		max_length=20,
		choices=(
			(TransportModes.driving, TransportModes.driving),
			(TransportModes.walking, TransportModes.walking),
			(TransportModes.bicycling, TransportModes.bicycling),
			(TransportModes.transit, TransportModes.transit)
		),
		default=TransportModes.transit,
	)

	is_public = models.BooleanField(default=False, blank=True, null=True)
	is_contactable = models.BooleanField(default=False, blank=True, null=True)

	is_sendgrid_allowed = models.BooleanField(default=False, blank=True, null=True,
		help_text='By default users are excluded from email functionality, whilst we test this in production.')

	def sync_nationbuilder_contact(self, **kwargs):
		# TODO: Fix this
		if self.is_contactable:
			nb_person = nationbuilder.create_or_get_person(
				email=self.email,
				phone=self.phone,
				first_name=self.first_name,
				last_name=self.last_name,
				postcode=self.postcode,
				**kwargs
			)
			if nb_person is not None:
				self.nationbuilder_id = get(nb_person, 'id')
				self.save()

	def full_name(self):
		'''
		Returns the first_name plus the last_name, with a space in between.
		'''
		full_name = '%s %s' % (self.first_name, self.last_name)
		return full_name.strip()

	def short_name(self):
		'''
		Returns the short name for the user.
		'''
		return self.first_name

	def email_user(self, subject, message, from_email=None, **kwargs):
		'''
		Sends an email to this User.
		'''
		send_mail(subject, message, from_email, [self.email], **kwargs)

	def nationbuilder_account(self):
		return nationbuilder.get_person(self.nationbuilder_id)

	def __str__(self):
		return self.full_name() + ' <' + str(self.email) + '>'

	@staticmethod
	def as_serizable_dict(instances):
		fields = [
					'email',
					'phone',
					'first_name',
					'last_name',
					'date_joined',
					'photo_url',
					'postcode',
					'commitments',
					'preferred_transport',
				]
		serialized_json = serializers.serialize('json', instances, fields=fields)
		return [instance['fields'] for instance in json.loads(serialized_json)]

class RequestUpdates(UUIDModel):
	class Meta:
		verbose_name = _('contact_capture')
		verbose_name_plural = _('contact_captures')

	email = models.EmailField(_('email address'), blank=False, unique=True)
	postcode = models.CharField(_('postcode'), max_length=10, blank=True, unique=False)
	is_contactable = models.BooleanField(default=False, blank=True, null=True)


@receiver(user_signed_up)
def populate_profile(sociallogin, user, **kwargs):
	if sociallogin.account.provider == 'facebook':
		user_data = user.socialaccount_set.filter(provider='facebook')[0].extra_data
		picture_url = "http://graph.facebook.com/" + sociallogin.account.uid + "/picture?type=large"
		email = user_data['email']
		first_name = user_data['first_name']
		last_name = user_data['last_name']

	if sociallogin.account.provider == 'twitter':
		user_data = user.socialaccount_set.filter(provider='twitter')[0].extra_data
		picture_url = user_data['profile_image_url']
		picture_url = picture_url.rsplit("_", 1)[0] + "." + picture_url.rsplit(".", 1)[1]
		email = user_data['email']
		first_name = user_data['name'].split()[0]
		last_name = user_data['name'].split()[1]

	user.photo_url = picture_url
	user.email = email
	user.first_name = first_name
	user.last_name = last_name
	user.save()


class ScheduleEmailJobManager(models.Manager):
	def get_queryset(self):
		now = datetime.datetime.now(timezone.utc)
		return super().get_queryset().filter(delivery_datetime__lte=now).order_by('delivery_datetime')


class ScheduledEmail(UUIDModel, DatedModel):
	PENDING = 'pending'
	IN_PROGRESS = 'in progress'
	COMPLETED = 'completed'
	FAILED = 'failed'
	STATES = (
		(PENDING, 'pending'),
		(IN_PROGRESS, 'in progress'),
		(COMPLETED, 'completed'),
		(FAILED, 'failed')
	)

	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		self._template_choices = self.get_available_templates_from_sendgrid()
		self._meta.get_field('sendgrid_template_id').choices = self._template_choices
		self.template_choices = dict(self._template_choices)

	delivery_datetime = models.DateTimeField(blank=True, null=True, default=None)
	status = models.CharField(max_length=24, choices=STATES, default=PENDING)

	objects = models.Manager()
	ready_to_deliver_objects = ScheduleEmailJobManager()

	# Content
	subject_line = models.CharField(max_length=150, blank=False, null=False, default='My Campaign Map update: 📍 your top priorities today')
	intro = RichTextField(blank=False, null=False, default='''
		<p>Hi $first_name 👋</p>
		<p>Keep up the hard work! With only <b>$days_left days to go until polling day</b> and everything to play for, every voter we can connect with is a victory.</p>
		<p>Here's where you're needed the most, based on your postcode, $postcode.</p>
	''', help_text="Available variables are the user's $first_name, $last_name, $postcode and $days_left (until polling day).")
	sendgrid_template_id = models.CharField(max_length=255, choices=[('', 'No Templates Available')])
	query = models.TextField(
		help_text='Query data to populate the email template. Available variables are $postcode, $today, $tomorrow and $dayafter',
		default=
        '''
        fragment EmailEventCard on EventType {
            id
            properties {
                startTime(format:"%a %d, %I:%M%P")
                name
                constituency {
                    id
                    name
                }
            }
        }

        query EmailQuery($postcode: String!) {
            constituencies: constituenciesForPostcode(postcode: $postcode) {
                id
                constituency {
                    id
                    name
                }
                events(itemCount: 3) {
                    ...EmailEventCard
                }
            }
            groups: groupsForPostcode(postcode: $postcode, itemCount: 3) {
				name
				description
				encodedJoinLink
				tags {
					name
				}
				platform {
					icon
					name
				}
            }
        }
        '''
	)

	class Meta:
		ordering = ['-delivery_datetime']

	def create_email_jobs(self):
		# N.B. Trying to create a duplicate email job i.e. one that has the same user and schedule
		# will cause an IntegrityError. this is intentional - we don't want users receiving duplicate emails
		batch_size = 1000
		yield_email_jobs = (EmailJob(user=user, schedule=self) for user in User.objects.filter(is_contactable=True, is_sendgrid_allowed=True))
		while True:
			batch = list(islice(yield_email_jobs, batch_size))
			if not batch:
				break
			else:
				EmailJob.objects.bulk_create(batch, batch_size)

	@staticmethod
	def get_available_templates_from_sendgrid():
		headers = {"Authorization": f"Bearer {settings.SENDGRID_API_KEY}"}
		response = requests.get('https://api.sendgrid.com/v3/templates?generations=dynamic', headers=headers)
		try:
			response.raise_for_status()
		except Exception:
			return [('-', 'Could not connect to SendGrid')]
		else:
			try:
				templates = json.loads(response.content)['templates']
			except KeyError:
				return [('-', 'No Templates Available')]
			else:
				return [(t['id'], t['name']) for t in templates]

	def __str__(self):
		return f'{self.delivery_datetime} | {self.template_choices.get(self.sendgrid_template_id)} | status: {self.status}'


class EmailJob(UUIDModel, DatedModel):
	PENDING = 'pending'
	IN_PROGRESS = 'in progress'
	COMPLETED = 'completed'
	FAILED = 'failed'
	STATES = (
		(PENDING, 'pending'),
		(IN_PROGRESS, 'in progress'),
		(COMPLETED, 'completed'),
		(FAILED, 'failed')
	)
	user = models.ForeignKey(User, on_delete=models.CASCADE)
	schedule = models.ForeignKey(ScheduledEmail, related_name='emails', on_delete=models.CASCADE)
	status = models.CharField(max_length=24, choices=STATES, default=PENDING)

	class Meta:
		unique_together = (('user', 'schedule'),)

	def __str__(self):
		return f'{self.pk} | status: {self.status}'
