from django.utils.timezone import get_current_timezone

def tzformat(datetime, format_str):
    return datetime.astimezone(get_current_timezone()).strftime(format_str)
