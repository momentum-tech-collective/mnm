from django.test import TestCase
from .cron.events import sync_labour_events
from .engine.politics import get_constituency_metadata
from .models import Event, Constituency

class IngestionTests(TestCase):

    def test_constituency_sync(self):
        """
        Labour sync should insert events where they aren't already in the DB.
        """
        data = get_constituency_metadata()

        # After ingesting, there should be Labour events
        self.assertIs(
            Constituency.objects.count() == 650,
            True
        )

    def test_labour_sync(self):
        """
        Labour sync should insert events where they aren't already in the DB.
        """
        sync_labour_events()

        # After ingesting, there should be Labour events
        self.assertIs(
            Event.objects.filter(labour_id__isnull=False).count() > 0,
            True
        )
