# Generated by Django 2.2.7 on 2020-04-02 17:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mynearestmarginal', '0035_sample_livestream_data'),
    ]

    operations = [
        migrations.AlterField(
            model_name='livestream',
            name='description',
            field=models.TextField(max_length=512),
        ),
    ]
