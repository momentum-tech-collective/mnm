from django.db import migrations
from mynearestmarginal.data import csv
from helpers.utils import get_path

def load_comms_group_options(apps, schema_editor):
    CommunicationPlatform = apps.get_model('mynearestmarginal', 'CommunicationPlatform')
    GroupTag = apps.get_model('mynearestmarginal', 'GroupTag')
    
    initial_platforms = [
      {
        "name": "Whatsapp Chat",
        "icon": "/static/VC_Hand_Whatsapp.svg",
        "description": "Open whatsapp group for people to coordinate their campaigning"
      },
      {
        "name": "Whatsapp Broadcast",
        "icon": "/static/VC_Hand_Whatsapp.svg",
        "description": "Listen-on whatsapp group for organisers to share info"
      },
      {
        "name": "Facebook Group",
        "icon": "/static/VC_Hand_FB.svg",
        "description": "Group for activists to coordinate their campaigning"
      }
    ]

    initial_tags = [
      {
        "name": "Official Labour",
        "icon": "✅"
      },
      {
        "name": "Official Momentum",
        "icon": "✅"
      }
    ]

    for platform in initial_platforms:
        CommunicationPlatform.objects.create(**platform)

    for tag in initial_tags:
        GroupTag.objects.create(**tag)


class Migration(migrations.Migration):
    dependencies = [
        ('mynearestmarginal', '0012_add_comms_groups_20191120_1332'),
    ]

    operations = [
        migrations.RunPython(load_comms_group_options),
    ]
