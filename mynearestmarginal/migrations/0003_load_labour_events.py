# Generated by Django 2.2.4 on 2019-08-23 12:08

from django.db import migrations
from helpers.utils import get_path
from ..cron.events import sync_labour_events

def load_labour_events(apps, schema_editor):
    Event = apps.get_model('mynearestmarginal', 'Event')
    #sync_labour_events(Event)


class Migration(migrations.Migration):
    dependencies = [
        ('mynearestmarginal', '0002_ingest_constituencies_data'),
    ]

    operations = [
        migrations.RunPython(load_labour_events),
    ]
