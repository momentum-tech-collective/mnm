# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations
from datetime import datetime  
import os
from mynearestmarginal.models import User


def create_superuser(apps, schema_editor):
    superuser = User()
    superuser.is_active = True
    superuser.is_superuser = True
    superuser.is_staff = True
    superuser.email = os.getenv('ADMIN_EMAIL')
    superuser.set_password(os.getenv('ADMIN_PASSWORD'))
    superuser.firstName = 'Jeremy'
    superuser.lastName = 'Corbyn'
    superuser.last_login = datetime.now()
    superuser.save()


class Migration(migrations.Migration):

    dependencies = [
        ('mynearestmarginal', '0003_load_labour_events'),
    ]

    operations = [
        migrations.RunPython(create_superuser)
    ]