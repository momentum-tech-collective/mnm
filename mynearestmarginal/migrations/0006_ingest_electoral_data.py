# Generated by Django 2.2.4 on 2019-08-23 12:08
from dateutil import parser
from django.db import migrations
from helpers.utils import get_path
from mynearestmarginal.data import csv
import json

labour_party = ['Labour', 'Labour and Co-operative']

def calculate_marginality(results):
    winner = results[0]
    overview = results[0]
    if winner.get('party') in labour_party:
        return int(winner.get('majority'))
    else:
        try:
            labour_candidate = list(filter(lambda e: e.get('party') in labour_party, results))[0]
            return int(labour_candidate.get('votes')) - int(winner.get('votes'))
        except:
            return -int(overview.get('turnout'))

def load_elections(Election, Candidate, results):
    election_names = set([e.get('election_name') for e in results])

    for election in election_names:
        constituencies = set([
            e.get('constituency_pk') for e in results
            if election == e.get('election_name')
        ])

        # print(f'{election} - {len(constituencies)} constituencies')

        for constituency in constituencies:
            constituency_results = list(filter(
                lambda e: (
                    constituency == e.get('constituency_pk')
                    and
                    election == e.get('election_name')
                ),
                results
            ))

            constituency_results = sorted(
                constituency_results,
                key=lambda e: int(e.get('votes')) if e.get('votes') is not None else 0,
                reverse=True
            )

            overview = constituency_results[0]

            e = Election.objects.create(**{
                "constituency_id": overview.get('constituency_pk'),
                "turnout": overview.get('turnout'),
                "electorate": overview.get('electorate'),
                "majority": overview.get('majority'),
                "name": overview.get('election_name'),
                "date": parser.parse(overview.get('date')),
                "result_statement": overview.get('result_statement'),
                "winning_candidate": overview.get('name'),
                "winning_party": overview.get('party'),
                "labour_marginality": calculate_marginality(constituency_results)
            })

            for r in constituency_results:
                Candidate.objects.create(**{
                    "election": e,
                    "name": r.get('name'),
                    "party": r.get('party'),
                    "votes": r.get('votes'),
                    "swing": r.get('swing'),
                    "share": r.get('share'),
                    "elected": r.get('elected') == 'TRUE'
                })

def load_electoral_data(apps, schema_editor):
    Party = apps.get_model('mynearestmarginal', 'Party')
    parties = csv.ingest('parties')
    Party.objects.bulk_create([Party(**{
        "name": d.get('name'),
        "short_name": d.get('short_name'),
        "primary_color_hex": d.get('hex')
    }) for d in parties])

    Election = apps.get_model('mynearestmarginal', 'Election')
    Candidate = apps.get_model('mynearestmarginal', 'Candidate')
    election_results = csv.ingest('ge2017')
    election_results += csv.ingest('byelections')
    elections = load_elections(Election, Candidate, election_results)

def delete_data(apps, schema_editor):
    apps.get_model('mynearestmarginal', 'Party').objects.all().delete()
    apps.get_model('mynearestmarginal', 'Election').objects.all().delete()
    apps.get_model('mynearestmarginal', 'Candidate').objects.all().delete()

class Migration(migrations.Migration):
    dependencies = [
        ('mynearestmarginal', '0005_add_electoral_models'),
    ]

    operations = [
        migrations.RunPython(load_electoral_data, delete_data),
    ]
    