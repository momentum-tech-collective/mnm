from mynearestmarginal.data import csv
from mynearestmarginal.engine import covid
from mynearestmarginal.cron import groups
from helpers.geo import create_point
from django.utils import dateparse
import googlemaps
import os

# Utilities used in migrations
# Kept in non-generated file so generated files can be re-generated easily

def load_local_authorities(apps, schema_editor):
  LocalAuthority = apps.get_model('mynearestmarginal', 'LocalAuthority')
  data = csv.ingest('local_authorities')
  for c in data:
    la = LocalAuthority()
    la.code = c['lad19cd']
    la.name = c['lad19nm']
    la.centroid = create_point(latitude=c['lat'], longitude=c['long'])
    la.save()


def delete_local_authorities(apps, schema_editor):
  LocalAuthority = apps.get_model('mynearestmarginal', 'LocalAuthority')
  LocalAuthority.objects.all().delete()


def new_platform_types(apps, schema_editor):
  CommunicationPlatform = apps.get_model('mynearestmarginal', 'CommunicationPlatform')
  data = csv.ingest('new_platform_types')
  for c in data:
    if (CommunicationPlatform.objects.filter(name=c['name'])).count() == 0:
      platform = CommunicationPlatform()
      platform.icon = c['icon']
      platform.name = c['name']
      platform.description = c['description']
      platform.save()


def delete_new_platform_types(apps, schema_editor):
  CommunicationPlatform = apps.get_model('mynearestmarginal', 'CommunicationPlatform')
  data = csv.ingest('new_platform_types')
  for c in data:
    CommunicationPlatform.objects.filter(name=c['name']).delete()


def new_group_tags(apps, schema_editor):
  GroupTag = apps.get_model('mynearestmarginal', 'GroupTag')
  if not GroupTag.objects.filter(name='Mutual Aid').exists():
    tag = GroupTag()
    tag.icon = '🖐️'
    tag.name = 'Mutual Aid'
    tag.description = 'Covid-19 Mutual Aid'
    tag.save()


def delete_new_group_tags(apps, schema_editor):
  GroupTag = apps.get_model('mynearestmarginal', 'GroupTag')
  GroupTag.objects.filter(name='Mutual Aid').delete()


def load_mutual_aid(apps, schema_editor):
  CommunicationPlatform = apps.get_model('mynearestmarginal', 'CommunicationPlatform')
  CommunicationGroup = apps.get_model('mynearestmarginal', 'CommunicationGroup')
  LocalAuthority = apps.get_model('mynearestmarginal', 'LocalAuthority')
  GroupTag = apps.get_model('mynearestmarginal', 'GroupTag')
  data = csv.ingest('CovidMap')
  print(list(GroupTag.objects.all()))
  mutual_aid = GroupTag.objects.get(name='Mutual Aid')
  matched_la = 0
  for c in data:
    group = CommunicationGroup()
    group.name = c['Name']
    group.join_link = covid.clean_link(c['URL'])
    group.official = False
    group.published = True
    group.platform = covid.determine_type(group.join_link, CommunicationPlatform)
    group.tags.add(mutual_aid)
    qs = LocalAuthority.objects.filter(name=c['Location'])
    found = qs.exists()
    if found:
      matched_la += 1
      group.local_authority = qs[0]
    if c['Lat'] != 'null' and c['Lon'] != 'null':
      group.coordinates = create_point(latitude=c['Lat'], longitude=c['Lon'])
    elif found and qs[0].centroid:
      group.coordinates = qs[0].centroid
    group.save()
  print(f'{matched_la} matched')


def delete_groups_from_file(apps, filename):
  CommunicationGroup = apps.get_model('mynearestmarginal', 'CommunicationGroup')
  data = csv.ingest(filename)
  for c in data:
    CommunicationGroup.objects.filter(name=c['Name']).delete()

def delete_mutual_aid(apps, schema_editor):
  delete_groups_from_file(apps, 'CovidMap')


ACORN_CITIES_BATCH_1 = ['Brighton', 'Bristol', 'Nottingham', 'Manchester', 
                        'Sheffield', 'Leeds', 'Lancaster']

def add_acorn_groups(apps, schema_editor):
  CommunicationPlatform = apps.get_model('mynearestmarginal', 'CommunicationPlatform')
  GroupTag = apps.get_model('mynearestmarginal', 'GroupTag')
  CommunicationGroup = apps.get_model('mynearestmarginal', 'CommunicationGroup')
  LocalAuthority = apps.get_model('mynearestmarginal', 'LocalAuthority')
  if not GroupTag.objects.filter(name='ACORN').exists():
    tag = GroupTag()
    tag.icon = '🖐️'
    tag.name = 'ACORN'
    tag.description = 'ACORN the Union'
    tag.save()
  tag = GroupTag.objects.get(name='ACORN')
  platform = CommunicationPlatform.objects.get(name='Web')
  for city in ACORN_CITIES_BATCH_1:
    group = CommunicationGroup()
    group.name = f'ACORN {city}'
    group.join_link = 'https://share.hsforms.com/1bmwJAoqrSmq4rjm9UAGT7w2tu82'
    group.official = True
    group.published = True
    group.platform = platform
    group.tags.add(tag)
    qs = LocalAuthority.objects.filter(name=city)
    if qs.exists():
      group.local_authority = qs[0]
      group.coordinates = qs[0].centroid
    group.save()


def delete_acorn_groups(apps, schema_editor):
  GroupTag = apps.get_model('mynearestmarginal', 'GroupTag')
  CommunicationGroup = apps.get_model('mynearestmarginal', 'CommunicationGroup')
  CommunicationGroup.objects.filter(tags__name='ACORN').delete()
  GroupTag.objects.filter(name='ACORN').delete()


def fix_rejected_groups(apps, schema_editor):
  CommunicationGroup = apps.get_model('mynearestmarginal', 'CommunicationGroup')
  CommunicationPlatform = apps.get_model('mynearestmarginal', 'CommunicationPlatform')
  GroupTag = apps.get_model('mynearestmarginal', 'GroupTag')
  data = csv.ingest('rejected_groups')
  mutual_aid = GroupTag.objects.get(name='Mutual Aid')
  gmaps = googlemaps.Client(key=os.getenv('GOOGLE_MAPS_API_KEY'))
  for c in data:
    group = CommunicationGroup()
    group.name = c['Name']
    group.join_link = covid.clean_link(c['URL'])
    group.official = False
    group.published = True
    group.platform = covid.determine_type(group.join_link, CommunicationPlatform)
    group.tags.add(mutual_aid)
    geocode_result = gmaps.geocode(f'{c["Location"]}, United Kingdom')
    if len(geocode_result) > 0:
      loc = geocode_result[0]['geometry']['location']
      group.coordinates = create_point(latitude=loc['lat'], longitude=loc['lng'])
    group.save()


def delete_rejected_groups(apps, schema_editor):
  delete_groups_from_file(apps, 'rejected_groups')


def groups_clean(CommunicationGroup):
  data = csv.ingest('duplicate_groups')
  prev = {}
  deleted = 0
  for c in data:
    if 'join_link' in prev and prev['join_link'] == c['join_link']:
      qs = CommunicationGroup.objects.filter(id=prev['id'])
      if qs.exists():
        qs.delete()
        deleted += 1
    prev = c
  print(f'{deleted} deleted')
  

def groups_clean_and_import(apps, schema_editor):
  CommunicationGroup = apps.get_model('mynearestmarginal', 'CommunicationGroup')
  CommunicationPlatform = apps.get_model('mynearestmarginal', 'CommunicationPlatform')
  GroupTag = apps.get_model('mynearestmarginal', 'GroupTag')
  groups_clean(CommunicationGroup)
  groups.sync_covid_groups(GroupTag=GroupTag, CommunicationGroup=CommunicationGroup, CommunicationPlatform=CommunicationPlatform)


def fake_unmigration(apps, schema_editor):
  print('Warning: this is a fake unmigration. Used for testing only')
  pass


def add_sample_livestreams(apps, schema_editor):
  Livestream = apps.get_model('mynearestmarginal', 'Livestream')
  data = csv.ingest('sample_livestreams')
  for c in data:
    l = Livestream(
      name=c['name'],
      description=c['description'],
      link=c['link'], 
      start_time=c['start_time'], 
      end_time=c['end_time'], 
      published=True)
    l.save()


def remove_all_livestreams(apps, schema_editor):
  Livestream = apps.get_model('mynearestmarginal', 'Livestream')
  Livestream.objects.all().delete()


def add_real_livestreams(apps, schema_editor):
  Livestream = apps.get_model('mynearestmarginal', 'Livestream')
  Livestream.objects.all().delete()
  data = csv.ingest('initial-livestreams')
  for c in data:
    l = Livestream(
      name=c['Name'],
      description=f'{c["Description"]} Hosted by {c["Organisation"]}.',
      link=c['Link'], 
      start_time=dateparse.parse_datetime(c['StartTime']), 
      end_time=dateparse.parse_datetime(c['EndTime']),
      published=True)
    l.save()

# Second pass after adding the org field
def add_real_livestreams_with_org(apps, schema_editor):
  Livestream = apps.get_model('mynearestmarginal', 'Livestream')
  Livestream.objects.all().delete()
  data = csv.ingest('initial-livestreams')
  for c in data:
    l = Livestream(
      name=c['Name'],
      description=c["Description"],
      organisation=c["Organisation"],
      link=c['Link'], 
      start_time=dateparse.parse_datetime(c['StartTime']), 
      end_time=dateparse.parse_datetime(c['EndTime']),
      published=True)
    l.save()

# Replace campaigning_category and training_category with subcategory
def replace_cats_and_subs(apps, schema_editor):
  Event = apps.get_model('mynearestmarginal', 'Event')
  Category = apps.get_model('mynearestmarginal', 'Category')
  Subcategory = apps.get_model('mynearestmarginal', 'Subcategory')

  samples = Event.objects.all().order_by('old_category').distinct('old_category')
  for sample in samples:
    name = sample.old_category
    category = Category.objects.create(name=name, emoji='✊', description=name)
    Event.objects.filter(old_category=name).update(category=category)

  samples = Event.objects.exclude(campaigning_category__isnull=True).order_by('campaigning_category').distinct('campaigning_category')
  # If there's no matching events, don't continue (else get() will throw)
  if len(samples) > 0:
    campaigning = Category.objects.get(name='Campaigning')
    for sample in samples:
      name = sample.campaigning_category
      subcat = Subcategory.objects.create(name=name, emoji='✊', description=name, parent=campaigning)
      Event.objects.filter(campaigning_category=name).update(subcategory=subcat)
  
  samples = Event.objects.exclude(training_category__isnull=True).order_by('training_category').distinct('training_category')
  if len(samples) > 0:
    training = Category.objects.get(name='Training')
    for sample in samples:
      name = sample.training_category
      subcat = Subcategory.objects.create(name=name, emoji='💭', description=name, parent=training)
      Event.objects.filter(training_category=name).update(subcategory=subcat)
