import os
import csv

def csv_file_to_dict(f, skipinitialspace=True, delimiter=',', quotechar='"'):
	return [
		{str(k): str(v) for k, v in row.items()}
		for row in csv.DictReader(f, skipinitialspace=skipinitialspace, delimiter=delimiter, quotechar=quotechar)
	]

def ingest(filename: str):
    with open(os.path.join(os.path.dirname(__file__), f'./{filename}.csv')) as f:
        data = csv_file_to_dict(f)
        return data