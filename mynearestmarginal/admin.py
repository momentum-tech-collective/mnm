import csv
from mynearestmarginal.data.csv import csv_file_to_dict
from django.urls import path
from django.shortcuts import redirect, render
from django import forms
from io import TextIOWrapper
from datetime import datetime
from django.contrib import admin
from rangefilter.filter import DateRangeFilter
from django.db.models import Q, Count, Subquery, OuterRef, IntegerField
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.forms import ReadOnlyPasswordHashField
from django.contrib.auth import get_user_model
from .models import *
from .management.filters import InputFilter, FilterFactory, LabourFilter
from helpers.geo import create_point
from .engine.geo import postcode_geo
from mynearestmarginal.engine.politics import marginality_to_band

def platform(cgroup):
	return f'{cgroup.platform.name}'

def source(cgroup):
	return f'{cgroup.tags.all()[0].name}'

@admin.register(CommunicationGroup)
class CommunicationGroupAdmin(admin.ModelAdmin):
	exclude = ['visitors', 'group_count', 'constituencies']
	ordering = ['name']
	autocomplete_fields = ['local_authority']
	list_display =  ('name', 'published', source, platform, 'join_link')
	source.admin_order_field = 'tags__name'
	platform.admin_order_field = 'platform__name'
	list_filter = (
		FilterFactory('name', lambda bit: Q(name__icontains=bit)),
		'published',
		FilterFactory('source', lambda bit: Q(tags__name__icontains=bit)),
		FilterFactory('platform', lambda bit: Q(platform__name__icontains=bit))
	)

	def get_form(self, request, obj=None, **kwargs):
		form = super(CommunicationGroupAdmin, self).get_form(request, obj, **kwargs)
		form.base_fields['official'].initial = True
		return form

	def save_model(self, request, obj, form, change):
		geo = postcode_geo(obj.postcode)
		obj.coordinates = create_point(latitude=geo['latitude'], longitude=geo['longitude'])
		super().save_model(request, obj, form, change)

admin.site.register(CommunicationPlatform)

admin.site.register(GroupTag)

@admin.register(Livestream)
class LivestreamAdmin(admin.ModelAdmin):
	readonly_fields = ('manager',)
	ordering = ('-start_time', '-created_time')
	def save_model(self, request, obj, form, change):
		if not obj.manager:
			obj.manager = request.user
		super().save_model(request, obj, form, change)

ADD_USER_FIELDS = ('email', 'first_name', 'last_name', 'phone')
class UserCreationForm(forms.ModelForm):
    password1 = forms.CharField(label='Password', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Password confirmation', widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ADD_USER_FIELDS

    def clean_password2(self):
        # Check that the two password entries match
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Passwords don't match")
        return password2

    def save(self, commit=True):
        # Save the provided password in hashed format
        user = super().save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user

USER_FIELDS = ('email', 'password', 'phone', 'first_name', 'last_name', 
		'is_staff', 'is_superuser', 'groups', 'user_permissions', 'is_active',
		'postcode', 'nationbuilder_id', 'is_public', 'date_joined', 'photo_url',
		'is_contactable', 'preferred_transport', 'is_sendgrid_allowed')

class UserChangeForm(forms.ModelForm):
    password = ReadOnlyPasswordHashField(label= ("Password"),
        help_text= ("Raw passwords are not stored, so there is no way to see "
                    "this user's password, but you can change the password "
                    "using <a href=\'../password/\'>this form</a>."))

    class Meta:
        model = User
        fields = USER_FIELDS

    def clean_password(self):
        # Regardless of what the user provides, return the initial value.
        # This is done here, rather than on the field, because the
        # field does not have access to the initial value
        return self.initial["password"]

@admin.register(User)
class UserAdmin(BaseUserAdmin):
	form = UserChangeForm
	add_form = UserCreationForm

	list_display = ('email', 'full_name', 'phone', 'postcode', 'RSVPs', 'managed_event_count')
	ordering = ('last_name', 'first_name')
	fieldsets = ((None, {'fields': USER_FIELDS}),)
	add_fieldsets = ((None, {'fields': ADD_USER_FIELDS + ('password1', 'password2')}),)

	def get_queryset(self, request):
		qs = super().get_queryset(request)
		return qs.annotate(RSVPs=Count('commitments'), managed_event_count=Count('managed_events'))

	def RSVPs(self, obj):
		return obj.RSVPs
	RSVPs.admin_order_field = 'RSVPs'

	def managed_event_count(self, obj):
		return obj.managed_event_count
	managed_event_count.admin_order_field = 'managed_event_count'

	list_filter = (
		'is_superuser',
		'is_staff',
		FilterFactory('name', lambda bit: Q(
			(
				Q(first_name__icontains=bit) |
				Q(last_name__icontains=bit)
			)
		)),
		FilterFactory('email', lambda bit: Q(email__icontains=bit)),
		FilterFactory('postcode', lambda bit: Q(postcode__icontains=bit))
	)

	search_fields = ['first_name', 'last_name', 'email', 'phone',]


class CsvImportForm(forms.Form):
	csv_file = forms.FileField()

@admin.register(Constituency)
class ConstituencyAdmin(admin.ModelAdmin):
	list_per_page = 650
	list_display = ('id', 'name', 'priority', 'attendance', 'polling_day_attendance', 'marginality_band', 'marginality_2017', 'event_count', 'RSVPs',)
	search_fields = ('name', 'target_constituencies__name')
	ordering = ['name']

	def get_queryset(self, request):
		qs = super(admin.ModelAdmin, self).get_queryset(request)
		GE2017 = datetime(2017, 6, 8)

		return qs.annotate(
			marginality_2017=Subquery(
				Election.objects.filter(
					date=GE2017,
					constituency=OuterRef('pk')
				).values('labour_marginality')[:1]
			),
			event_count=Count('events'),
			RSVPs=Count('events__attendees')
		)

	def marginality_2017(self, obj):
		return obj.marginality_2017
	marginality_2017.admin_order_field = 'marginality_2017'

	def marginality_band(self, obj):
		return marginality_to_band(obj.marginality_2017)

	def event_count(self, obj):
		return obj.event_count
	event_count.admin_order_field = 'event_count'

	def RSVPs(self, obj):
		return obj.RSVPs
	RSVPs.admin_order_field = 'RSVPs'

	## CSV import 

	change_list_template = "constituency_changelist.html"

	def get_urls(self):
		urls = super().get_urls()
		my_urls = [
			path('import-csv/', self.import_csv),
		]
		return my_urls + urls

	def import_csv(self, request):
		if request.method == "POST":
			csv_file = TextIOWrapper(request.FILES['csv_file'].file, encoding=request.encoding)
			rows = csv_file_to_dict(csv_file)
			line_count = 0
			for row in rows:
				c = Constituency.objects.get(pk=row['id'])

				if len(row['priority']):
					c.priority = int(row['priority'])

				if len(row['attendance']):
					c.attendance = int(row['attendance'])

				if len(row['polling_day_attendance']):
					c.polling_day_attendance = int(row['polling_day_attendance'])

				c.save()

			self.message_user(request, "Your csv file has been imported")
			return redirect("..")
		form = CsvImportForm()
		payload = {"form": form}
		return render(
			request, "csv_form.html", payload
		)

@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
	list_display = ('name', 'emoji', 'userVisible', 'allowUserCreate')

@admin.register(Subcategory)
class SubcategoryAdmin(admin.ModelAdmin):
	list_display = ('name', 'emoji', 'parent', 'userVisible', 'allowUserCreate')

@admin.register(Event)
class EventAdmin(admin.ModelAdmin):
	search_fields = ('id', 'name', 'postcode', 'address', 'category', 'subcategory',)

	list_display = (
		'name',
		'start_time',
		'address',
		'category',
		'subcategory',
		'published',
		'contact_number',
		'contact_email',
	)

	ordering = ('start_time','name','published',)

	list_select_related = ('manager', 'constituency')

	def RSVPs(self, obj):
		return obj.RSVPs
	RSVPs.admin_order_field = 'RSVPs'

	def get_queryset(self, request):
		qs = super(admin.ModelAdmin, self).get_queryset(request)
		return qs.annotate(
			RSVPs=Count('attendees')
		)

	list_filter = (
		FilterFactory('name', lambda bit: Q(name__icontains=bit)),
		('start_time', DateRangeFilter),
		'published',
		LabourFilter,
	)

	## Change form

	exclude = ('labour_priority', 'constituency', 'coordinates',
	           'labour_category', 'nationbuilder_id', 'labour_id', 
						 'other_category_description', 'targetEvent')
	autocomplete_fields = ('manager', 'constituency', 'targetEvent',)

	def save_model(self, request, obj, form, change):
		geo = postcode_geo(obj.postcode)
		obj.coordinates = create_point(latitude=geo['latitude'], longitude=geo['longitude'])
		super().save_model(request, obj, form, change)


@admin.register(LocalAuthority)
class LocalAuthorityAdmin(admin.ModelAdmin):
	ordering = ["name"]
	search_fields = ['name']


@admin.register(ScheduledEmail)
class ScheduledEmailAdmin(admin.ModelAdmin):
	readonly_fields=('created_date', 'modified_date',)
	ordering = ('delivery_datetime',)

	def get_readonly_fields(self, request, obj=None):
		if obj:
			return ["delivery_datetime"]
		else:
			return []


@admin.register(EmailJob)
class EmailJobAdmin(admin.ModelAdmin):
	readonly_fields=('created_date', 'modified_date',)
	pass

