from allauth.socialaccount.providers.facebook.views import FacebookOAuth2Adapter
from allauth.socialaccount.providers.twitter.views import TwitterOAuthAdapter
from rest_auth.registration.views import SocialLoginView
from rest_auth.social_serializers import TwitterLoginSerializer


class FacebookLogin(SocialLoginView):
    adapter_class = FacebookOAuth2Adapter


class TwitterLogin(SocialLoginView):
    serializer_class = TwitterLoginSerializer
    adapter_class = TwitterOAuthAdapter


### This runs when users register via Instagram etc.

from allauth.socialaccount.adapter import DefaultSocialAccountAdapter

class SocialSignupUserDataAdapter(DefaultSocialAccountAdapter):
    pass
    # def populate_user(self, request, sociallogin, data):
    #     sociallogin.account.user.email = data.get('email')
    #     sociallogin.account.user.first_name = data.get('first_name')         
    #     sociallogin.account.user.last_name = data.get('last_name')    
    #     sociallogin.account.user.save()