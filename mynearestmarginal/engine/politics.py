import requests
from helpers.utils import get_path, trace
from helpers.cache import cached_fn
import re
import json
from datetime import datetime
from ..models import Constituency, Election

'''
We can get all election results at constituency level, across mutliple years, from
http://lda.data.parliament.uk/electionresults.json?_view=Elections&_pageSize=10&_page=0

http://explore.data.parliament.uk/?learnmore=Elections has the filter information.

Can get candidates etc from this tree:
- http: //explore.data.parliament.uk/?learnmore=Election%20Results

Election results API with candidates and turnout and such:
- constituency results:
    DOCS: http://eldaddp.azurewebsites.net/electionresults.html?_page=0&_view=all&constituencyId=143474&_properties=constituency.label#w39988142aab9b2
    API: http://eldaddp.azurewebsites.net/electionresults.json?_page=0&_view=all&constituencyId=143474&_properties=constituency.label
- result candidates:
    DOCS: http://eldaddp.azurewebsites.net/resources/382388/candidates/3
    API: http://eldaddp.azurewebsites.net/resources/382388/candidates/3.json?_view=all&_properties=party,fullName,voteChangePercentage,numberOfVotes,order
'''

def parl_id(url: str) -> int:
    return re.search(r"([0-9]+)$", url, re.MULTILINE).group(0)

# (parl_id from above used to get all elections for each constituency below )

def get_constituency_election_results():
    res = requests.get('http://eldaddp.azurewebsites.net/electionresults.json?_page=0&_view=all&_properties=constituency.label&_pageSize=10000')
    data = res.json()
    elections = data.get('result').get('items')
    elections = [parse_election(c) for c in elections]
    return elections

###

# @cached_fn(lambda *args, **kwargs: 'candidates' + json.dumps(args) + json.dumps(kwargs))
def get_election_candidate(local_election_id: int, local_candidate_id: int):
    res = requests.get(f'http://eldaddp.azurewebsites.net/resources/{local_election_id}/candidates/{local_candidate_id}.json?_view=all&_properties=party,fullName,voteChangePercentage,numberOfVotes,order')
    data = res.json()
    candidate = data.get('result').get('primaryTopic')
    candidate = {
        **candidate,
        "party": candidate.get("party").get("_value"), # this seat
        "id": candidate.get("_about"), # this seat
        "local_id": parl_id(candidate.get("_about")), # this seat
        "full_name": candidate.get("fullName").get('_value')
    }
    return candidate

def parse_election(c):
    return {
        **c,
        "id": parl_id(c.get("_about")), # this seat
        "global_id": parl_id(c.get("election").get("_about")), # the umbrella election
        "name": c.get("election").get("label").get("_value"), # the umbrella election
        "candidate_local_ids": [parl_id(c) for c in c.get("candidate")],
        "constituency": {
            "parl_id": parl_id(c.get("constituency").get("_about")),
            "name": c.get('constituency').get('label').get('_value')
        },
    }

# @cached_fn(lambda *args, **kwargs: 'elections' + json.dumps(args) + json.dumps(kwargs))
def get_constituency_election_results(id: int):
    res = requests.get(f'http://eldaddp.azurewebsites.net/electionresults.json?_page=0&_view=all&constituencyId={id}&_properties=constituency.label')
    data = res.json()
    elections = data.get('result').get('items')
    elections = [parse_election(c) for c in elections]
    return elections

# @cached_fn(lambda *args, **kwargs: 'elections_meta' + json.dumps(args) + json.dumps(kwargs))
def get_election_metadata(id: int):
    res = requests.get(f'http://lda.data.parliament.uk/elections/{id}.json')
    data = res.json()
    election = data.get('result').get('primaryTopic')
    return {
        **election,
        "date": election.get("date").get("_value"),
        "name": election.get("label").get("_value")
    }

def constituency_marginality_band(constituency):
    constituency_results_2017 = Election.get_by_constituency_and_date(constituency.id, date=datetime(2017, 6, 8))
    labour_vote_margin_2017 = int(constituency_results_2017.labour_marginality)

    ## Defensive
    return marginality_to_band(labour_vote_margin_2017)

def marginality_to_band(labour_vote_margin_2017):
    if labour_vote_margin_2017 < 0:
        if labour_vote_margin_2017 > -400:
            return 5
        if labour_vote_margin_2017 > -1200:
            return 4
        if labour_vote_margin_2017 > -2200:
            return 3
        if labour_vote_margin_2017 > -4000:
            return 2
        if labour_vote_margin_2017 > -8000:
            return 1
    else:
        # Offensive
        if labour_vote_margin_2017 < 400:
            return 5
        if labour_vote_margin_2017 < 1200:
            return 4
        if labour_vote_margin_2017 < 2200:
            return 3
        if labour_vote_margin_2017 < 4000:
            return 2
        if labour_vote_margin_2017 < 6000:
            return 1

    return 0

def constituency_priority(constituency, hardcoded_target_ids = []):
    if constituency is None:
        return 0

    if constituency.id in [c.id for c in hardcoded_target_ids]:
        return 10

    # de-emphasise the better-attended constituencies compared with others in the same
    # priority band
    attendance_modifier = -0.1 * constituency.attendance

    if constituency.priority is None:
        return constituency_marginality_band(constituency) + attendance_modifier
    else:
        return constituency.priority + attendance_modifier

@cached_fn(cache_type="memory", key=__package__ + 'marginal_constituencies')
def marginal_constituencies():
    return [c for c in Constituency.objects.all() if constituency_priority(c) > 0]


###


def polling_day_constituency_priority(constituency, hardcoded_target_ids = []):
    if constituency is None:
        return 0

    if constituency.id in [c.id for c in hardcoded_target_ids]:
        return 10

    # de-emphasise the better-attended constituencies compared with others in the same
    # priority band
    attendance_modifier = -0.1 * constituency.attendance

    if constituency.priority is None:
        return constituency_marginality_band(constituency) + attendance_modifier
    else:
        return constituency.priority + attendance_modifier
