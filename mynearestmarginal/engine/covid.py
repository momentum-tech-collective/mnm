import os
import requests
import urllib.parse
from helpers.cache import cached_fn
from mynearestmarginal.models import CommunicationPlatform

def clean_link(url):
  parsed = urllib.parse.urlparse(url)
  if parsed.netloc.find("facebook.com") >= 0:
    parsed = parsed._replace(scheme="https", netloc="www.facebook.com")
    if parsed.path.find("/groups/") >= 0:
      parsed = parsed._replace(query="")
  elif parsed.netloc.find("whatsapp.com") >= 0:
    parsed = parsed._replace(scheme="https", query="")
  newurl = urllib.parse.urlunparse(parsed)
  return newurl

def determine_type(url, CommunicationPlatform=CommunicationPlatform):
  parsed = urllib.parse.urlparse(url)
  if parsed.netloc == 'www.facebook.com':
    return CommunicationPlatform.objects.get(name='Facebook Group')
  elif parsed.netloc == 'chat.whatsapp.com':
    return CommunicationPlatform.objects.get(name='Whatsapp Chat')
  elif parsed.netloc.find('nextdoor') >= 0:
    return CommunicationPlatform.objects.get(name='Nextdoor')
  elif parsed.netloc.find('twitter.com') >= 0:
    return CommunicationPlatform.objects.get(name='Twitter')
  else:
    return CommunicationPlatform.objects.get(name='Web')

#@cached_fn('COVID_GROUPS_CACHE_KEY', 60 * 5)
def get_groups():
  response = requests.get(os.getenv('COVID_GROUPS_URL'))
  return response.json()