import googlemaps
import requests
from helpers.utils import get, get_path, batch_and_aggregate
from helpers.geo import create_point
from helpers.cache import cached_fn
from django.core.cache import cache
import os

def postcode_geo(postcode: str):
    response = requests.get(f'https://api.postcodes.io/postcodes/{postcode}')
    data = response.json()
    status = get(data, 'status')
    result = get(data, 'result')

    if status is not 200 or result is None:
        raise Exception(f'Failed to geocode postcode: {postcode}.')

    return result

@batch_and_aggregate(100)
def bulk_postcode_geo(postcodes):
    response = requests.post(f'https://api.postcodes.io/postcodes', json={
        "postcodes": postcodes
    },)
    data = response.json()
    status = get(data, 'status')
    result = get(data, 'result')

    if status is not 200 or result is None:
        raise Exception(f'Failed to bulk geocode postcodes: {postcodes}.')

    return result


@batch_and_aggregate(25)
def bulk_coordinate_geo(coordinates):
    for i, coords in enumerate(coordinates):
        coordinates[i]["limit"] = 1

    payload = {
        "geolocations": coordinates
    }

    response = requests.post(f'https://api.postcodes.io/postcodes', data=payload)
    data = response.json()
    status = get(data, 'status')
    result = get(data, 'result')

    if status is not 200 or result is None:
        raise Exception(f'Failed to bulk geocode coordinates: {payload}')

    return result

def coordinates_geo(latitude: float, longitude: float):
    response = requests.get(
        f'https://api.postcodes.io/postcodes?lon={longitude}&lat={latitude}')
    data = response.json()
    status = get(data, 'status')
    result = get(data, 'result')

    if status is not 200 or result is None or len(result) < 1:
        raise Exception(
            f'Failed to get postcode for coordinates: lon={longitude}&lat={latitude}.')

    return result[0]

def point_from_geo(geo):
    return create_point(latitude=get_path(geo, 'latitude'), longitude=get_path(geo, 'longitude'))

def constituency_id_from_geo(geo):
    return get_path(geo, 'codes', 'parliamentary_constituency')

def local_auth_from_geo(geo):
    return get_path(geo, 'codes', 'admin_district')

def constituency_id_by_postcode(postcode: str) -> str:
    geo = postcode_geo(postcode)
    return constituency_id_from_geo(geo)

def local_auth_by_postcode(postcode: str) -> str:
    geo = postcode_geo(postcode)
    return local_auth_from_geo(geo)

class TransportModes():
    driving = "driving"
    walking = "walking"
    bicycling = "bicycling"
    transit = "transit"

def fetch_distance_matrix(origins, destinations, mode):
    def encode_location(loc):
        if isinstance(loc, dict):
            return ",".join(map(str, [loc.get('latitude'), loc.get('longitude')]))
        else:
            return loc

    res = requests.get("https://maps.googleapis.com/maps/api/distancematrix/json", params={
        "units": "metric",
        "origins": "|".join(map(encode_location, origins)),
        "destinations": "|".join(map(encode_location, destinations)),
        "mode": mode,
        "key": os.getenv('GOOGLE_MAPS_API_KEY')
    })

    data = res.json()

    if data is None or len(data['rows']) == 0:
        print('ERROR fetching travel distances', data)
        return []

    return [
        {
            "origin": origin,
            "destination": destination,
            "mode": mode,
            "payload": data['rows'][0]['elements'][destinations.index(destination)]
        } for origin in origins for destination in destinations
    ]

def travel_info_key(origin, destination, mode):
    return f'travel_info:{origin}:{destination}:{mode}'

def cached_distances(origin, destinations, mode):
    return [
        v for [d, v] in list(cache.get_many([travel_info_key(origin, destination, mode) for destination in destinations]).items())
    ]

def get_relative_travel_info(origin, destinations=[], mode=TransportModes.transit):
    ## Search for cached travel info to reduce billing

    origin_coordinates, *dest_coordinates = get_approximate_postcode_locations([origin] + destinations)
    unique_dest_coordinates = list({ create_point(**d): d for d in dest_coordinates }.values())

    cache_hits = cached_distances(origin_coordinates, unique_dest_coordinates, mode)

    result = { create_point(**hit['destination']): hit for hit in cache_hits }

    # Unique missing destinations (important to unique this to cut down on distance matrix api calls)
    missing_destinations = [
        d for d in unique_dest_coordinates
        if create_point(**d) not in result
    ]

    if len(missing_destinations) > 0:
        # Fetch the missing travel infos
        new_travel_info = fetch_distance_matrix(origins=[origin_coordinates], destinations=missing_destinations, mode=mode)

        # Cache the new data
        cache.set_many({
            travel_info_key(d.get('origin'), d.get('destination'), d.get('mode')): d
            for d in new_travel_info
        }, None)

        # Add to the results map
        for d in new_travel_info:
            result[create_point(**d['destination'])] = d

    return [result[create_point(**d['destination'])] for d in dest_coordinates]

def get_approximate_postcode_locations(postcodes):
    '''
    Increase frequency of distance matrix cache hits by lowering precision of locations
    '''

    def approximate_location(coordinate):
        # 0.01 degrees distance on both long and lat == about a 20 minute walk in the uk
        return {
            "latitude": round(get_path(coordinate, 'result', 'latitude'), 2),
            "longitude": round(get_path(coordinate, 'result', 'longitude'), 2)
        }

    return map(approximate_location, bulk_postcode_geo(postcodes))


def postcode_components(g):
    return [t for t in g.get('address_components') if 'postal_code' in t.get('types')]

def geo_by_address(address: str):
    params = {
        "key": os.getenv('GOOGLE_MAPS_API_KEY'),
        "components": 'country:' + os.getenv('CCTLD'),
        "address": address
    }
    res = requests.get(f'https://maps.googleapis.com/maps/api/geocode/json?', params=params)
    data = res.json()
    postcoded_geos = [g for g in data.get('results') if len(postcode_components(g))]
    if len(postcoded_geos) == 0:
        return {}
    geo = postcoded_geos[0]
    return {
        'address': geo.get('formatted_address'),
        'postcode': postcode_components(geo)[0].get('short_name'),
        'latitude': float(geo.get('geometry').get('location').get('lat')),
        'longitude': float(geo.get('geometry').get('location').get('lng'))
    }

'''
output {
    "status" : "OK",
    "destination_addresses" : [ "New York, NY, USA" ],
    "origin_addresses" : [ "Washington, DC, USA" ],
    "rows" : [
        {
            "elements" : [
                {
                    "distance" : {
                        "text" : "225 mi",
                        "value" : 361715
                    },
                    "duration" : {
                        "text" : "3 hours 49 mins",
                        "value" : 13725
                    },
                    "status" : "OK"
                }
            ]
        }
    ]
}
'''
