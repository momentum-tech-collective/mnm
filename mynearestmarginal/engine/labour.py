"""
We ingest Labour Data and optionally port it to NationBuilder,
when it's necessary to record RSVPs.

The data is synced every hour and whilst the format isn't quite as we'd like, it otherwise up to date:

* All upcoming events
    https://events.labour.org.uk/api/events/all
    https://labourorganise.com/api/events/all

* Radius search from postcode centroid
    https://events.labour.org.uk/api/events/search/{postcode}
    https://labourorganise.com/api/events/search?date=&show=filters&embed=&keyword=&postcode=W13AJ&category=&distance=50&constituency=true

* Individual event
     https://events.labour.org.uk/api/events/get/{id}
    
There's an issue with the single CLP events endpoint, so you might want to filter on the radius within the CLP then subsequently filter on the in_constituency flag.
"""
import requests
from helpers.utils import get
from helpers.cache import cached_fn
from collections import Sequence


@cached_fn('LABOUR_EVENTS_CACHE_KEY', 60 * 5)
def get_events():
    # should_filter = false
    # filter_fields = [
    #     'date'
    #     'keyword'
    #     'postcode'
    #     'category'
    #     'distance'
    #     'constituency'
    # ]

    # for field in filter_fields:
    #     if field in kwargs:
    #         should_filter = true

    # if should_filter:
    response = requests.get("https://labourorganise.com/api/events/all")
    data = response.json()
    status = get(data, 'success')
    results = get(data, 'results')

    if status is False or not isinstance(results, Sequence):
        raise Exception('Failed to load events from the Labour API')

    return results


def get_event_by_id(id: str):
    response = requests.get(
        f"https://events.labour.org.uk/api/events/get/{id}")
    data = response.json()
    status = get(data, 'success')
    result = get(data, 'result')

    if status is False or not isinstance(result, Sequence):
        raise Exception(f'Failed to load event ID {id} from the Labour API')

    return result[0]
