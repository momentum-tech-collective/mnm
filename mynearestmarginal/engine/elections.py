import requests
from helpers.utils import get
import numpy as np


def get_election_date(election: str) -> np.datetime65:
    return np.datetime64(election['id'].replace('parl.'))


def get_constituency_results(gss: str):
    res = requests.get(
        f'https://candidates.democracyclub.org.uk/api/v0.9/posts/WMC:{gss}/')
    constituency = res.json()

    # Get last election ID
    last_election_date = sort([
        get_election_date(election)
        for election in constituency['elections']
        if "parl." in election['id']
    ])[0]

    # Filter candidates by last election ID
    candidates = [
        candidate
        for candidate in constituency['memberships']
        if last_election_date == get_election_date(candidate['election'])
    ]

    return {
        "name": constituency['label'],
        "gss_id": constituency['id'].replace('WMC:', ''),
        "last_election_date": last_election_date,
        "last_election_candidates": candidates
    }
