import datetime
import json
import logging

from django.core import serializers

from .geo import get_relative_travel_info, postcode_geo, constituency_id_by_postcode, local_auth_by_postcode
from ..models import Constituency, Commitment, Event, CommunicationGroup
from .politics import constituency_priority, marginal_constituencies, polling_day_constituency_priority
from django.contrib.gis.measure import Distance
from django.contrib.gis.db.models.functions import Distance as DistanceExpr
from helpers.geo import create_point
import numpy as np
from django.utils import timezone
from datetime import timedelta, date, time, datetime
from functools import reduce
from datetime import timedelta

DISTANCE_MATRIX_MAX_ELEMENTS = 100

def all_events_near_postcode(
	postcode: str,
	date_start: str,
	date_end: str,
	search_radius: float = 15
):
	geo = postcode_geo(postcode)
	origin = create_point(latitude=geo['latitude'], longitude=geo['longitude'])

	# Filter events
	candidates = Event.objects.filter(
		# within a sensible radius, so we can do rich analysis
		coordinates__distance_lte=(
			create_point(latitude=geo['latitude'], longitude=geo['longitude']),
			Distance(km=search_radius)
		),
		# in the requested date range
		start_time__range=[
			date_start,
			date_end
		],
		published=True
	).annotate(
		distance=DistanceExpr('coordinates', origin)
	).order_by('start_time')

	return candidates


def events_near_postcode(
	postcode: str,
	date_start: str,
	date_end: str,
	search_radius: float = 40
):
   # cap search radius to 100km to avoid hurting the database
	if (search_radius > 100):
		search_radius = 100
	candidates = all_events_near_postcode(
		postcode, date_start, date_end, search_radius)

	def rank_event(event):
		return event.distance

	sorted_candidates = sorted(candidates, key=rank_event)

	return sorted_candidates[:200]


def event_recommendations_by_postcode(
	postcode: str,
	date_start: str,
	date_end: str,
	search_radius: float = 40
):
	geo = postcode_geo(postcode)
	start_constituency = constituency_from_geo(geo)

	def rank_event(event):
		return (
			0,
			-5,
			distance_band(event.distance, Distance(km=5)).km / 15,
		)

	def filter_event(event):
		return True

	candidates = all_events_near_postcode(
		postcode, date_start, date_end, search_radius)

	accepted_candidates = list(filter(filter_event, candidates))
	sorted_candidates = sorted(accepted_candidates, key=rank_event)

	return sorted_candidates[:25]


def constituency_recommendations_by_postcode(
	postcode: str,
	search_radius: float = 100
):
	# Get nearby constituencies by searching events in range
	geo = postcode_geo(postcode)
	start_constituency = constituency_from_geo(geo)
	hardcoded_targets = Constituency.targets_for(start_constituency.id)
	origin = create_point(latitude=geo['latitude'], longitude=geo['longitude'])
	
	if len(hardcoded_targets) > 0:
		def get_distance(t):
			events = Event.objects.filter(constituency__id=t.id) \
				.annotate(distance=DistanceExpr('coordinates', origin))[:5]

			return median([e.distance for e in events])

		return [
			{
				'id': t.id,
				'constituency': t,
				'distance': get_distance(t),
				'events': Event.objects.filter(
					labour_id__isnull=False,
					constituency__id=t.id,
					# in the requested date range
					start_time__gte=timezone.now() + timedelta(hours=4),
					category=Category.objects.get(name='Campaigning'),
					subcategory=Subcategory.objects.get('Canvassing')
				).order_by('start_time')
			}
			for t in hardcoded_targets
		]

	events = Event.objects.filter(
		labour_id__isnull=False,
		constituency__isnull=False,
		coordinates__distance_lte=(
			origin,
			Distance(km=search_radius)
		),
		# in the requested date range
		start_time__gte=timezone.now() + timedelta(hours=4),
		subcategory__name='Canvassing'
	).annotate(distance=DistanceExpr('coordinates', origin))

	# Get candidate constituencies
	target_map = {
		event.constituency_id: {
			'id': event.constituency_id,
			'constituency': Constituency.get(event.constituency_id),
			'events': []
		}
		for event
		in events
	}

	# Flatten out the constituency events
	for event in events:
		target_map[event.constituency_id].get('events').append(event)

	# Collate constituency-level data used for ranking
	for target in target_map.values():
		constituency = target['constituency']
		events = target['events']

		target['distance'] = median([event.distance for event in events])
		target['constituency_priority'] = constituency_priority(constituency, hardcoded_targets)

	target_constituencies = [
		t for t in target_map.values()
		if t['constituency_priority'] > 0
	]

	ranked_constituencies = sorted(target_constituencies, key=lambda target: (
		# Favour priority (highest priority first)
		-target['constituency_priority'],

		# Then favour distance with wide bands (closest first)
		distance_band(target['distance'], Distance(km=30)),

		# Then distribute recommendations randomly but consistently by hashing the start of the postcode?
		# hash(postcode[:3]),
	))

	# Trim out non-campaigning events
	for constituency in ranked_constituencies:
		constituency['events'] = sort_by_start_time([
			e for e in constituency['events']
			if e.category == 'Campaigning'
		])[:7]

	return sorted(ranked_constituencies[:3], key=lambda target: target['constituency_priority'])


def constituency_from_geo(geo):
	return Constituency.get(geo.get('codes').get('parliamentary_constituency'))


def recommended_travel_to_events(constituency_id, postcode):
	try:
		geo = postcode_geo(postcode)
	except:
		return []

	origin = create_point(latitude=geo['latitude'], longitude=geo['longitude'])
	constituency = Constituency.get(constituency_id)

	# Get all the upcoming campaign events
	campaigning_events = (
		[] if constituency_priority(constituency) < 4 or constituency.attendance >= 4
		else Event.objects.filter(
			constituency_id=constituency_id,
			category='Campaigning',
			published=True,
			start_time__range=[
				date.today(),
				date.today() + timedelta(weeks=2)
			],
		)
	)

	# Get all the travel to events in range
	travel_to_event_map = {
		event.targetEvent_id: event
		for event in Event.objects.filter(
			targetEvent_id__in=[e.id for e in campaigning_events],
			coordinates__distance_lte=(
				origin,
				Distance(km=15)
			),
			published=True,
			category="TravelTogether"
		)
	}

	def rank_travel_to_event(event):
		return (
			# Prefer weekend events
			0 if event.start_time.weekday() >= 5 else 1,

			# Prefer earlier events
			event.start_time
		)

	# Annotate the constituencies with events that have a 'travel together'
	# within range, along with those where we dont have one.
	travel_to_ranked = sorted([
		travel_to_event_map[e.id]
		for e in one_for_each(constituency['events'], key=lambda x: x.start_time.date())
		if e.id in travel_to_event_map
	], key=rank_travel_to_event)[:5]

	travel_to_days = {e.start_time.date() for e in travel_to_ranked}

	travel_to_wanted_ranked = sorted([
		e
		for e in one_for_each(constituency['events'], key=lambda x: x.start_time.date())
		if e.id not in travel_to_event_map
		and constituency_priority(Constituency.get(constituency_id))
		and e.start_time.date() not in travel_to_days
	], key=rank_travel_to_event)[:4]

	return {
		'travel_to_events': sort_by_start_time(travel_to_ranked),
		'travel_to_wanted_events': sort_by_start_time(travel_to_wanted_ranked)
	}


def recommended_constituency_travel_to_events(constituency_id, postcode):
	try:
		geo = postcode_geo(postcode)
	except:
		return {
			'travel_to_events': [],
			'travel_to_wanted_events': []
		}

	origin = create_point(latitude=geo['latitude'], longitude=geo['longitude'])

	# Get all the upcoming campaign events
	campaigning_events = [
		e for e
		in Event.objects.filter(
			constituency_id=constituency_id,
			category='Campaigning',
			published=True,
			start_time__range=[
				datetime.now(),
				date.today() + timedelta(weeks=3)
			],
		)
		if 'phone' not in e.name.lower()
	]

	# Get all the travel to events in range
	travel_to_event_map = {
		event.targetEvent_id: event
		for event in Event.objects.filter(
			targetEvent_id__in=[e.id for e in campaigning_events],
			coordinates__distance_lte=(
				origin,
				Distance(km=15)
			),
			published=True,
			category="TravelTogether"
		)
	}

	def rank_travel_to_event(event):
		return (
			# Prefer weekend events
			0 if event.start_time.weekday() >= 5 else 1,

			# Prefer earlier events
			event.start_time
		)

	# Annotate the constituencies with events that have a 'travel together'
	# within range, along with those where we dont have one.
	travel_to_ranked = sorted(
		travel_to_event_map.values(), key=rank_travel_to_event)[:5]

	travel_to_days = {e.start_time.date() for e in travel_to_ranked}

	travel_to_wanted_ranked = sorted([
		e for e in one_for_each(campaigning_events, key=lambda x: x.start_time.date())
		if e.id not in travel_to_event_map
		and e.start_time.date() not in travel_to_days
	], key=rank_travel_to_event)[:12]

	return {
		'travel_to_events': sort_by_start_time(travel_to_ranked),
		'travel_to_wanted_events': sort_by_start_time(travel_to_wanted_ranked)
	}


def sort_by_start_time(events):
	return sorted(events, key=lambda e: e.start_time)


def one_for_each(items, key):
	grouped = {key(item): item for item in items}
	return grouped.values()


def distance_band(distance, band_size):
	return Distance(km=nearest(distance.km, band_size.km))


def nearest(x, base):
	return base * round(x / base)


def band_size(values, band_count):
	if len(values) == 0:
		return 0

	return max((1, (max(values) - min(values)))) / band_count


def median(arr):
	return sorted(arr)[int(len(arr) / 2)]


def constituency_recommendations_as_serializable_dict(constituency_recommendations):
	recommendations = []
	for recommendation in constituency_recommendations:
		recommendation['events'] = Event.as_serializable_dict(recommendation['events'])
		recommendation['constituency'] = Constituency.as_serializable_dict([recommendation['constituency'], ])
		recommendations.append(recommendation)
	return recommendations


def all_groups_near_postcode(postcode: str, search_radius: float = 15):
	geo = postcode_geo(postcode)
	origin = create_point(latitude=geo['latitude'], longitude=geo['longitude'])

	candidates = CommunicationGroup.objects.filter(
		coordinates__distance_lte=(
			create_point(latitude=geo['latitude'], longitude=geo['longitude']),
			Distance(km=search_radius)
		),
		published=True
	).annotate(
		distance=DistanceExpr('coordinates', origin)
	)

	return candidates


def group_recommendations_by_postcode(postcode: str, search_radius: float = 40):
	candidates = all_groups_near_postcode(postcode, search_radius)
	sorted_candidates = sorted(candidates, key=lambda group: group.distance)
	return sorted_candidates[:25]

def polling_day_constituency_recommendations_by_postcode(
	postcode: str,
	search_radius: float = 100
):
	# User location
	geo = postcode_geo(postcode)
	origin = create_point(latitude=geo['latitude'], longitude=geo['longitude'])
	start_constituency = constituency_from_geo(geo)
	setattr(start_constituency, 'distance', Distance(km=0))

	# Bin for populated recommendations
	target_map = {}
	# Bin for constituencies
	recommended_constituencies = []
	#
	marginals =  [
		c for c
		in Constituency.objects.all().annotate(distance=DistanceExpr('centroid', origin))
		if polling_day_constituency_priority(c) > 0
	]

	# Add your own one if possible
	user_is_in_a_marginal = get_index(
		marginals,
		lambda c: c.id == start_constituency.id
	) > -1

	if user_is_in_a_marginal:
		recommended_constituencies += [start_constituency]

	# If political have explicitly said targets for constituency
	hardcoded_targets = Constituency.targets_for(start_constituency.id)
	recommended_constituencies += hardcoded_targets
	recommended_constituencies = Constituency.objects\
		.filter(id__in=[c.id for c in recommended_constituencies])\
		.annotate(distance=DistanceExpr('centroid', origin))
	
	if len(recommended_constituencies) > 0:
		more_target_map = {
			c.id: {
				'id': c.id,
				'constituency': c,
				'distance': c.distance,
				'events': []
			}
			for c
			in recommended_constituencies
		}
		target_map = {
			**target_map,
			**more_target_map
		}

	# Get candidate constituencies
	more_target_map = {
		m.id: {
			'id': m.id,
			'constituency': m,
			'events': []
		}
		for m
		in marginals
	}

	target_map = {
		**target_map,
		**more_target_map
	}

	if user_is_in_a_marginal:
		target_map[start_constituency.id] = {
			'id': start_constituency.id,
			'constituency': start_constituency,
			'events': []
		}

	# Collate constituency-level data used for ranking
	for target in target_map.values():
		constituency = target['constituency']
		target['distance'] = target['constituency'].distance
		target['constituency_priority'] = polling_day_constituency_priority(constituency, hardcoded_targets)

	target_constituencies = [
		t for t in target_map.values()
		if t['constituency_priority'] > 0
	]

	ranked_constituencies = sorted(target_constituencies, key=lambda target: (
		# Favour priority (highest priority first)
		-target['constituency_priority'],

		# Then favour distance with wide bands (closest first)
		distance_band(target['distance'], Distance(km=30))
	))

	# Ensure the user's own marginal constituency comes first
	if user_is_in_a_marginal:
		user_constituency_index = get_index(
			ranked_constituencies,
			lambda c: c['id'] == start_constituency.id
		)

		if user_constituency_index > -1:
			ranked_constituencies.insert(0, ranked_constituencies.pop(user_constituency_index))

	# Add a minimum number of constituencies that are not oversaturated
	# and keep adding more until this is the case
	required_count = max(4, len(hardcoded_targets))
	final_recommendations = []
	for c in ranked_constituencies:
		final_recommendations.append(c)
		if c['constituency'].polling_day_attendance == 3:
			required_count += 1
		if required_count == len(final_recommendations):
			break

	return sorted(final_recommendations, key=lambda target: target['constituency_priority'])

def get_index(l, find):
	index = -1

	for i, c in enumerate(l):
		if find(c):
			index = i
			break
	else:
		index = -1

	return index
