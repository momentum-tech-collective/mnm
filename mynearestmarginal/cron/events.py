import numpy as np
import datetime
from pytz import timezone
from helpers.utils import get
from helpers.geo import create_point
from ..engine.geo import bulk_postcode_geo, constituency_id_from_geo, postcode_geo, coordinates_geo, point_from_geo

date_now = np.datetime64(datetime.datetime.now())

'''
These scripts regularly ingest events data into Django
to standardise objects for querying on date/location/etc.

Ingesting on a schedule helps us to avoid over-loading 3rd party services
and allows us to aggregate events information.

By centralising in Django, we avoid spamming NationBuilder with ghost events that exist in theory,
but for which no RSVPs are known — a useful distinction as NationBuilder is primarily used by
Momentum for communications oriented around people, not around events per se.

By contrast, MyNearestMarginal is an events-oriented platform.
'''

from ..engine import labour
from ..models import Event

def sync_labour_events(Event = Event):
    # Find labour events in the API that have locations
    events = labour.get_events()
    events_labour = [
        e for e in events
        if (
            e is not None
            and type(e['postcode']) is not None
            and len(e['postcode']) >= 4
        )
    ]

    existing_events = Event.objects.all()
    
    existing_events_by_labour_id = {ev_d.labour_id:ev_d for ev_d in existing_events}
    labour_events_id_set = {get(ev_d, 'event_id') for ev_d in events_labour}

    events_to_upsert = [
        event for event
        in [event_from_labour_event(e) for e in events_labour]
        if get(event, 'postcode') != None
        and get(event, 'coordinates') != None
        and get(event, 'address') != None
    ]
    # Attach constituency IDs to these new events
    postcode_geos = bulk_postcode_geo([e['postcode'] for e in events_to_upsert])

    for i, event in enumerate(events_to_upsert):
        matching_geos = list(filter(lambda geo: geo['query'] == events_to_upsert[i]['postcode'], postcode_geos))
        events_to_upsert[i]['constituency_id'] = constituency_id_from_geo(matching_geos[0]['result'])

    # Ingest

    events_to_insert = [
        event for event
        in events_to_upsert
        if get(event, 'labour_id') not in existing_events_by_labour_id
    ]

    events_to_update = [
        event for event
        in events_to_upsert
        if get(event, 'labour_id') in existing_events_by_labour_id
    ]

    event_ids_to_unpublish = [
        event.id for event
        in existing_events
        if event.labour_id is not None
        and get(event, 'labour_id') not in labour_events_id_set
        # Don’t unpublish events that have only disappeared from the API response because they are in the past
        and get(event, 'start_time').timestamp() > datetime.datetime.now().timestamp()
    ]

    Event.objects.bulk_create([Event(**e) for e in events_to_insert if e['constituency_id'] is not None])
    Event.objects.filter(id__in=event_ids_to_unpublish).update(published=False)

    for event_data in events_to_update:
        # Don't ever overwrite these fields
        event_data.pop('constituency_id')
        event_data.pop('published')

        existing_event = existing_events_by_labour_id[event_data['labour_id']]

        if is_update_to_model(event_data, existing_event):
            Event.objects.filter(id=existing_event.id).update(**event_data)

def is_update_to_model(e, model):
    for key, val in e.items():
        if getattr(model, key) != val:
            return True

    return False

def event_from_labour_event(e):
    event = {
        "name": get(e, 'title'),
        "category": Category.objects.get(name=infer_category(e)),
        "labour_category": get(e, 'category_name'),
        "labour_priority": get(e, "priority"),
        "labour_id": get(e, 'event_id'),
        "address": get(e, 'location') or '',
        "postcode": get(e, 'postcode'),
        "coordinates": point_from_geo(e), # based on Labour providing the lat/lng
        "description": get(e, 'description'),
        "start_time": convert_date_time(get(e, 'start_time')),
        "end_time": convert_date_time(get(e, 'end_time')),
        "contact_number": get(e, "contact_number"),
        "published": False # Start hiding Labour events from the map
    }
    subcat_name = infer_subcategory(e)
    if subcat_name and Subcategory.objects.filter(name=subcat_name).count() > 0:
        event["subcategory"] = Subcategory.objects.get(name=subcat_name)


    if (event["postcode"] is None or len(event["postcode"]) < 1) and e["latitude"] and e["longitude"]:
        geo = coordinates_geo(latitude=get(e, 'latitude'), longitude=get(e, 'longitude'))
        event["postcode"] = get(geo, 'postcode')

    elif event["postcode"] and (e["latitude"] is None or e["longitude"] is None):
        geo = postcode_geo(get(event, 'postcode'))
        event["coordinates"] = point_from_geo(e)

    event["postcode"] = event["postcode"].replace(" ", "").upper()

    return event

def convert_date_time(datetime_str):
    '''
    Convert naive datetime string returned by labour api to a timzeone-aware iso datetime string
    '''

    tz = timezone("Europe/London")
    dt = datetime.datetime.strptime(datetime_str, "%Y-%m-%d %H:%M:%S")

    return tz.localize(dt)

def infer_category(event):
    category = get(event, 'category_name')

    if category in ["Polling Day Campaign Centre", "Campaigning"]:
        return "Campaigning"

    if category in ["Conference", "Members' Meeting", "Rally or Speech", "Surgery"]:
        return "Meeting"

    if category in ["Social Event"]:
        return "Socialising"

    if category in ["Training (in person)", "Training (online)"]:
        return "Training"

    return "Campaigning"

def infer_subcategory(event):
    if infer_category(event) != 'Campaigning':
        return None

    name = get(event, 'name', default='').lower()
    labour_category = get(event, 'category_name')
    
    if 'stall' in name or labour_category == 'Street Stall':
        return 'Visibility'
    
    if labour_category == 'Polling Day Campaign Centre':
        return 'Campaign Centre'

    if 'dialogue' in name or 'phone' in name or labour_category == 'Phonebanking':
        return 'Dialogue'

    if 'leafleting' in name or labour_category == 'Leafletting':
        return 'Leafletting'

    return 'Canvassing'

