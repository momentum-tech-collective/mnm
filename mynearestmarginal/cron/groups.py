import csv
import os
from django.db import transaction
from ..engine import covid
from helpers.utils import get
from helpers.geo import create_point
from ..models import CommunicationGroup, CommunicationPlatform, GroupTag

def sync_covid_groups(
    GroupTag=GroupTag,
    CommunicationGroup=CommunicationGroup,
    CommunicationPlatform=CommunicationPlatform):
  
  mutual_aid = GroupTag.objects.get(name='Mutual Aid')

  scraped_groups = covid.get_groups()
  existing_groups = CommunicationGroup.objects.filter(tags__name='Mutual Aid')
  existing_groups_by_key = {key(group):group for group in existing_groups}

  groups_to_upsert = [
    clean_link(group) for group
    in scraped_groups
    if get(group, 'location_coord') != None
  ]
  scraped_groups_key_set = {key(group) for group in groups_to_upsert}

  groups_to_insert = [
    group for group 
    in groups_to_upsert
    if key(group) not in existing_groups_by_key
  ]

  groups_to_update = [
    group for group 
    in groups_to_upsert
    if key(group) in existing_groups_by_key
  ]

  group_ids_to_delete = [
    group.id for group
    in existing_groups
    if key(group) not in scraped_groups_key_set
  ]

  print((f"Mutual aid scraper: {len(existing_groups)} already in DB, "
         f"{len(scraped_groups)} scraped from Covid, "
         f"{len(groups_to_upsert)} to upsert, "
         f"{len(groups_to_insert)} to insert, "
         f"{len(groups_to_update)} to check for updates, "
         f"{len(group_ids_to_delete)} to delete"))

  CommunicationGroup.objects.filter(id__in=group_ids_to_delete).delete()

  updated = 0
  with transaction.atomic():
    for scraped in groups_to_update:
      existing = existing_groups_by_key[key(scraped)]
      if update_from_scraped(scraped, existing, CommunicationPlatform):
        updated += 1

  with transaction.atomic():
    for scraped in groups_to_insert:
      create_from_scraped(scraped, mutual_aid, CommunicationGroup, CommunicationPlatform)

  print(f'Mutual aid scraper: {updated} updated records modified.')
  print(f'Mutual aid scraper: {CommunicationGroup.objects.filter(tags__name="Mutual Aid").count()} records in DB')


def clean_link(scraped):
  scraped["join_link"] = covid.clean_link(scraped["link_facebook"])
  return scraped


def key(group):
  return f'{get(group, "join_link")} {get(group, "name").upper()}'


def get_coordinates(scraped):
  coord = scraped["location_coord"]
  return create_point(latitude=coord["lat"], longitude=coord["lng"])


def create_from_scraped(scraped, mutual_aid, CommunicationGroup, CommunicationPlatform):
  group = CommunicationGroup()
  group.name = scraped['name']
  group.join_link = scraped['join_link']
  group.official = False
  group.published = True
  group.platform = covid.determine_type(group.join_link, CommunicationPlatform)
  group.tags.add(mutual_aid)
  group.coordinates = get_coordinates(scraped)
  group.save()


def update_from_scraped(scraped, existing, CommunicationPlatform):
  coordinates = get_coordinates(scraped)
  if scraped['name'] != existing.name or coordinates != existing.coordinates:
    existing.name = scraped['name']
    existing.coordinates = coordinates
    existing.save()
    return True
  else:
    return False
