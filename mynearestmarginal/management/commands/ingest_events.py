from django.core.management.base import BaseCommand
from ...cron.events import sync_labour_events

class Command(BaseCommand):
    help = 'Ingests events from Labour. Expected to be run by an external cron job on a schedule.'

    def handle(self, *args, **kwargs):
        sync_labour_events()
