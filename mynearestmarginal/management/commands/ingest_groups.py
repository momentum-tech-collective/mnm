from django.core.management.base import BaseCommand
from ...cron.groups import sync_covid_groups

class Command(BaseCommand):
    help = 'Ingests groups from Covid. Expected to be run by an external cron job on a schedule.'

    def handle(self, *args, **kwargs):
        sync_covid_groups()
