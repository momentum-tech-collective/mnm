from django.core.management.base import BaseCommand
from django.db.models import Q

from ...models import ScheduledEmail, EmailJob

import logging
from .logging import handler

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
logger.addHandler(handler)


class Command(BaseCommand):
    help = 'Create EmailJobs to be processed by a worker'

    def handle(self, *args, **kwargs):
        logger.info('Creating EmailJobs for Scheduled Emails that are ready to be delivered')
        for scheduled_email in ScheduledEmail.ready_to_deliver_objects.all():
            if scheduled_email.status == ScheduledEmail.PENDING:
                try:
                    logger.info(f'Trying to create EmailJobs for {scheduled_email.id}')
                    scheduled_email.create_email_jobs()
                except Exception as e:
                    logger.error(f'Failed creating EmailJobs for {scheduled_email.id}')
                    print(f'\t{e}')
                    scheduled_email.status = scheduled_email.FAILED
                else:
                    logger.info(f'Marking ScheduledEmail {scheduled_email.id} as IN PROGRESS')
                    scheduled_email.status = scheduled_email.IN_PROGRESS


            elif scheduled_email.status == ScheduledEmail.IN_PROGRESS:
                # Have all the email jobs completed?
                if not EmailJob.objects\
                    .filter(schedule=scheduled_email) \
                    .filter(Q(status=EmailJob.IN_PROGRESS) | Q(status=EmailJob.PENDING))\
                    .exists():
                    # All the email jobs have completed, have any failed?
                    if EmailJob.objects\
                        .filter(
                            schedule=scheduled_email,
                            status=EmailJob.FAILED
                        ).exists():
                        logger.error(f'Marking ScheduledEmail {scheduled_email.id} as FAILED')
                        scheduled_email.status = ScheduledEmail.FAILED
                    else:
                        logger.info(f'Marking ScheduledEmail {scheduled_email.id} as Completed')
                        scheduled_email.status = ScheduledEmail.COMPLETED

            scheduled_email.save()
