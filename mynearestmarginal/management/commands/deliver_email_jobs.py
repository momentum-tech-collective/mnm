import concurrent
import logging
import os
import sys
import textwrap
from concurrent.futures.thread import ThreadPoolExecutor
from datetime import datetime, timedelta
from django.conf import settings
from django.core.management.base import BaseCommand
from django.db import transaction
from sendgrid import SendGridAPIClient, Mail, MailSettings, SandBoxMode
from ...models import User, EmailJob, Event, ScheduledEmail
from .logging import handler
from ...graphql.schema import query
from mynearestmarginal.graphql.context import DataLoadersMiddleware
from graphql_jwt.middleware import JSONWebTokenMiddleware
from string import Template
from django import db
import time

MAX_WORKERS = settings.MAX_WORKERS
EMAIL_BATCH_SIZE = settings.EMAIL_BATCH_SIZE

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
logger.addHandler(handler)


class Command(BaseCommand):
    help = 'Send email recommendations to all contactable users'

    def handle(self, *args, **kwargs):
        count = EmailJob.objects.filter(status=EmailJob.PENDING).count()
        db.connections.close_all()

        if count > 0:
            with concurrent.futures.ProcessPoolExecutor(max_workers=MAX_WORKERS) as executor:
                logger.info(f'Beginning EmailJob delivery with %s workers' % MAX_WORKERS)

                for _ in executor.map(EmailJobHandler.submit, range(0, count)):
                    pass

                logger.info(f'Processed batch of {count} emails')
        sys.exit(0)

class EmailJobHandler:
    # per-worker postcode area caching
    shared = None

    @staticmethod
    def submit(_):
        if EmailJobHandler.shared is None:
            EmailJobHandler.shared = EmailJobHandler()

        return EmailJobHandler.shared.deliver_email()

    def __init__(self):
        self.cache = dict()

    @transaction.atomic
    def deliver_email(self):
        # This get's the next 'pending' EmailJob and locks it so that
        # no other deliver_email process can access it.
        GET_AND_LOCK_EMAIL_JOB = '''
            UPDATE mynearestmarginal_emailjob
            SET status = 'completed'
            WHERE id = (
                SELECT id FROM mynearestmarginal_emailjob
                WHERE status = 'pending'
                FOR UPDATE SKIP LOCKED
                LIMIT 1
                )
            RETURNING *;'''

        try:
            email_job = EmailJob.objects.raw(textwrap.dedent(GET_AND_LOCK_EMAIL_JOB))[0]
        except IndexError:
            return
        else:
            try:
                response = self.send_sengrid_mail(email_job)
            except Exception as e:
                print('Email delivery failed', e)
                email_job.status = email_job.FAILED
            else:
                email_job.status = email_job.COMPLETED
            finally:
                email_job.save()

    def send_sengrid_mail(self, email_job):
        # This is an expensive function call:
        message = Mail(
            to_emails=email_job.user.email,
            from_email=settings.DEFAULT_FROM_EMAIL
        )

        mail_settings = MailSettings()

        if settings.SENDGRID_SANDBOX_MODE:
            mail_settings.sandbox_mode = SandBoxMode(True)

        message.dynamic_template_data = self.create_sendgrid_email_context(email_job.user, email_job.schedule)
        message.template_id = email_job.schedule.sendgrid_template_id
        send_grid = SendGridAPIClient(settings.SENDGRID_API_KEY)

        message.mail_settings = mail_settings
        return send_grid.send(message)

    def create_sendgrid_email_context(self, user: User, schedule: ScheduledEmail):
        today = datetime.now()
        tomorrow = today + timedelta(days=1)
        day_after = tomorrow + timedelta(days=1)
        polling_day = datetime(2019, 12, 12)
        days_left = polling_day.date() - today.date()

        data = self.query_sendgrid_email_context(
            schedule,
            postcode=user.postcode,
            today=today.strftime('%Y-%m-%d'),
            tomorrow=tomorrow.strftime('%Y-%m-%d'),
            day_after=day_after.strftime('%Y-%m-%d')
        )

        variables = {
            "subject": schedule.subject_line,
            "days_left": days_left.days,
            'first_name': user.first_name,
            'last_name': user.last_name,
            'postcode': user.postcode,
            'mycampaignmap_url': settings.SITE_URL,
        }

        variables['intro'] = Template(schedule.intro).substitute(**variables)

        context = {
            **variables,
            **data
        }

        return context

    def get_cache_key(self, postcode, today, tomorrow, day_after):
        return '|'.join([postcode[:4], today, tomorrow, day_after])

    def query_sendgrid_email_context(self, schedule, **kwargs):
        cache_key = self.get_cache_key(**kwargs)

        # we’re ok with this not being atomic, as the ocassional unnecessary cache miss is preferable to
        # frequent locking
        if cache_key in self.cache:
            return self.cache.get(cache_key)

        res = query(
            schedule.query,    
            variables=kwargs
        )

        if res.errors:
            print(res.errors)
            raise Exception(res.errors)

        self.cache[cache_key] = res.data
        return res.data
