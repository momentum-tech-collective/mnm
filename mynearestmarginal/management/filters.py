from django.contrib import admin
from django.db.models import Q
import os
from django.template.loader import get_template

class InputFilter(admin.SimpleListFilter):
  template = 'input_filter.html'

  def lookups(self, request, model_admin):
    # Dummy, required to show the filter.
    return ((),)

  def choices(self, changelist):
    # Grab only the "all" option.
    all_choice = next(super().choices(changelist))
    all_choice['query_parts'] = (
      (k, v)
      for k, v in changelist.get_filters_params().items()
      if k != self.parameter_name
    )
    yield all_choice
      

def FilterFactory(param: str, q):
  class SomeFilter(InputFilter):
    parameter_name = param
    title = param
    def queryset(self, request, queryset):
      term = self.value()
      if term is None:
        return 
      for bit in term.split():
        return queryset.filter(q(bit))
  return SomeFilter


class LabourFilter(admin.SimpleListFilter):
  title = 'whether from Labour'
  parameter_name = 'is_labour'

  def lookups(self, request, model_admin):
    return [(None, 'Not Labour'),
            ('labour', 'Labour'),
            ('all', 'All')]

  def choices(self, cl):
    for lookup, title in self.lookup_choices:
        yield {
            'selected': self.value() == lookup,
            'query_string': cl.get_query_string({
                self.parameter_name: lookup,
            }, []),
            'display': title,
        }
  
  def queryset(self, request, queryset):
    if self.value() == 'labour':
      return queryset.exclude(labour_id__exact=None)
    elif self.value() == 'all':
      return queryset
    else:
      return queryset.filter(labour_id__exact=None)

