from django.http.response import HttpResponse, HttpResponseForbidden, HttpResponseBadRequest, HttpResponseNotFound
from django.conf import settings
from .models import User


def delete_user(request):
    if request.method == 'POST' and request.POST['secret'] == settings.GDPR_TOOL_SECRET:
        if 'email' in request.POST or 'nb_id' in request.POST:
            if 'nb_id' in request.POST:
                if isinstance(request.POST['nb_id'],int) is False:
                    try:
                        num = int(request.POST['nb_id'])
                    except:
                        return HttpResponseBadRequest(content='nb_id must be an integer or string that can be converted to an integer')
                    
                user = User.objects.filter(nationbuilder_id=int(request.POST['nb_id'])).first()
                if user is None:
                    return HttpResponseNotFound(content='No record with that nb_id ' + request.POST['nb_id'])
                if user.is_superuser:
                    return HttpResponseForbidden(content='Can not delete superuser using GDPR tool. Log into the MCM admin panel to delete a superuser.')
                
                User.objects.filter(
                    nationbuilder_id=request.POST['nb_id']).delete()
                return HttpResponse(content='Deleted member with nb_id ' + str(request.POST['nb_id']))

            if 'email' in request.POST:
                user = User.objects.filter(email=request.POST['email']).first()
                if user is None:
                    return HttpResponseNotFound(content='No record with email address ' + request.POST['email'])
                if user.is_superuser:
                    return HttpResponseForbidden(content='Can not delete superuser using GDPR tool. Log into the MCM admin panel to delete a superuser.')

                User.objects.filter(
                    email=request.POST['email']).delete()
                return HttpResponse(content='Deleted member with email address ' + request.POST['email'])
        else:
            return HttpResponseBadRequest(content='No nb_id or email provided.')
    else:
        return HttpResponseForbidden(content='Invalid Credentials')

