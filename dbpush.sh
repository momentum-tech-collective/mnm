#!/bin/bash

APP=mcm-staging

export PGPORT=5462
export PGHOST=localhost
export PGUSER=postgres
export PGPASSWORD=postgres

DB=postgres

HEROKU=heroku 

$HEROKU pg:backups:capture --app $APP
$HEROKU pg:reset DATABASE_URL --app $APP --confirm $APP
$HEROKU pg:push $DB DATABASE_URL --app $APP
